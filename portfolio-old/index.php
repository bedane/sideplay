<!doctype html>
  <head>
    <title>SidePlay Game Demos</title>
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styles.css">

    <link rel="javascript" type="text/css" href="jquery.js">
    <link rel="javascript" type="text/css" href="bootstrap.min.js">
    <link rel="javascript" type="text/css" href="html5.js">
    <link rel="javascript" type="text/css" href="respond.js">
  </head>
  <body>
            <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NLS84R"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NLS84R');</script>
    <!-- End Google Tag Manager -->
    <header>
      <div class="container">
        <img src="img/logo.png" alt="Sideplay Logo"/>
      </div>
    </header>
    <section class="container">
      <!-- ***** -->
      <section class="row">
        <h2>Licensed Content</h2>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=mousetrap" title="Mouse Trap"><img src="./img/mousetrap/start.png" alt="Mouse Trap"/></a>
          <footer>
            <a href="./playGame.php?gameName=mousetrap"><h4>Mouse Trap</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=dealornodeal" title="Deal Or No Deal"><img src="./img/dealornodeal/start.jpg" alt="Deal or No Deal"/></a>
          <footer>
            <a href="./playGame.php?gameName=dealornodeal"><h4> Deal or No Deal</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=cluedo" title="Cluedo"><img src="./img/cluedo/start.png" alt="Cluedo"/></a>
          <footer>
            <a href="./playGame.php?gameName=cluedo"><h4>Cluedo</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=passthepigs" title="Pass The Pigs"><img src="./img/passthepigs/start.png" alt="Pass The Pigs"/></a>
          <footer>
            <a href="./playGame.php?gameName=passthepigs"><h4>Pass The Pigs</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=strikeitrich" title="Strike It Rich"><img src="./img/strikeitrich/start.png" alt="Strike It Rich"/></a>
          <footer>
            <a href="./playGame.php?gameName=strikeitrich"><h4>Strike It Rich</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=pacman" title="Pacman"><img src="./img/pacman/start.png" alt="Pacman"/></a>
          <footer>
            <a href="./playGame.php?gameName=pacman"><h4>Pacman</h4></a>
          </footer>
        </article>
      </section>
      <!-- ***** -->
      <section class="row">
        <h2>Board & Dice</h2>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=classicsnakes" title="Classic Snakes & Ladders"><img src="./img/classicsnakes/start.jpg" alt="Classic Snakes & Ladders"/></a>
          <footer>
            <a href="./playGame.php?gameName=classicsnakes"><h4>Classic Snakes & Ladders</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=monstermoney" title="Monster Money"><img src="./img/monstermoney/start.png" alt="Monster Money"/></a>
          <footer>
            <a href="./playGame.php?gameName=monstermoney"><h4>Monster Money</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=moneymeadow" title="Money Meadow"><img src="./img/moneymeadow/start.png" alt="Money Meadow"/></a>
          <footer>
            <a href="./playGame.php?gameName=moneymeadow"><h4>Money Meadow</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/zombie/index.html" title="Zombie Escape"><img src="./img/zombieescape/start.png" alt="Zombie Escape"/></a>
          <footer>
            <a href="./game/zombie/index.html"><h4>Zombie Escape</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=spacenuggle" title="Space Nuggle"><img src="./img/spacenuggle/start.png" alt="Space Nuggle"/></a>
          <footer>
            <a href="./playGame.php?gameName=spacenuggle"><h4>Space Nuggle</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=robinhood" title="Robin Hood"><img src="./img/robinhood/start.jpg" alt="Robin Hood"/></a>
          <footer>
            <a href="./playGame.php?gameName=robinhood"><h4>Robin Hood</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=perfectpresent" title="Perfect Present"><img src="./img/perfectpresent/start.png" alt="Perfect Present"/></a>
          <footer>
            <a href="./playGame.php?gameName=perfectpresent"><h4>Perfect Present</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=passporttoprizes" title="Passport to Prizes"><img src="./img/passporttoprizes/start.png" alt="Passport to Prizes"/></a>
          <footer>
            <a href="./playGame.php?gameName=passporttoprizes"><h4>Passport to Prizes</h4></a>
          </footer>
        </article>
      </section>
      <!-- ***** -->
      <section class="row">
        <h2>Big Money!</h2>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=milliongold" title="&pound;1 Million Gold"><img src="./img/onemillionpoundsgold/start.png" alt="&pound;1 Million Gold"/></a>
          <footer>
            <a href="./playGame.php?gameName=milliongold"><h4>&pound;1 Million Gold</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=moneymoneymoney" title="Money Money Money"><img src="./img/moneymoneymoney/start.png" alt="Money Money Money"/></a>
          <footer>
            <a href="./playGame.php?gameName=moneymoneymoney"><h4>Money Money Money</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=250k-rainbow" title="&pound;250k Rainbow"><img src="./img/250krainbow/start.png" alt="&pound;250k Rainbow"/></a>
          <footer>
            <a href="./playGame.php?gameName=250k-rainbow"><h4>&pound;250k Rainbow</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=bubbleburstbingo" title="Bubble Burst Bingo"><img src="./img/bubbleburstbingo/start.png" alt="Bubble Burst Bingo"/></a>
          <footer>
            <a href="./playGame.php?gameName=bubbleburstbingo"><h4>Bubble Burst Bingo</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=prizeportal" title="Prize Portal"><img src="./img/prizeportal/start.png" alt="Prize Portal"/></a>
          <footer>
           <a href="./playGame.php?gameName=prizeportal"><h4>Prize Portal</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=moneylines" title="Money Lines"><img src="./img/moneylines/start.png" alt="Money Lines"/></a>
          <footer>
            <a href="./playGame.php?gameName=moneylines"><h4>Money Lines</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=moneymultiplier" title="Money Multiplier"><img src="./img/moneymultiplier/start.jpg" alt="Money Multiplier"/></a>
          <footer>
            <a href="./playGame.php?gameName=moneymultiplier"><h4>Money Multiplier</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=getlucky" title="Get Lucky"><img src="./img/getlucky/start.png" alt="Get Lucky"/></a>
          <footer>
            <a href="./playGame.php?gameName=getlucky"><h4>Get Lucky</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=millionaire777" title="Millionaire 777"><img src="./img/millionaire/startmini.png" alt="Millionaire 777"/></a>
          <footer>
            <a href="./playGame.php?gameName=millionaire777"><h4>Millionaire 777</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=4millionpurple" title="&pound;4 Million Purple"><img src="./img/4millionpurple/startmini.png" alt="&pound;4 Million Purple"/></a>
          <footer>
            <a href="./playGame.php?gameName=4millionpurple"><h4>&pound;4 Million Purple</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=100kyellow" title="&pound;100k Yellow"><img src="./img/100kyellow/startmini.png" alt="&pound;100k Yellow"/></a>
          <footer>
            <a href="./playGame.php?gameName=100kyellow"><h4>&pound;100k Yellow</h4></a>
          </footer>
        </article>
      </section>
      <!-- ***** -->
      <section class="row">
        <h2>Just For Fun</h2>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=cashalley" title="Cash Alley"><img src="./img/tincancash/start.png" alt="Cash Alley"/></a>
          <footer>
            <a href="./playGame.php?gameName=cashalley"><h4>Cash Alley</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=copterrun" title="Copter Run"><img src="./img/copterrun/start.png" alt="Copter Run"/></a>
          <footer>
            <a href="./playGame.php?gameName=copterrun"><h4>Copter Run</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=backgammon" title="Backgammon"><img src="./img/backgammon/start.png" alt="Backgammon"/></a>
          <footer>
           <a href="./playGame.php?gameName=backgammon"><h4>Backgammon</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=astrominer" title="Astro Miner"><img src="./img/astrominer/start.png" alt="Astro Miner"/></a>
          <footer>
            <a href="./playGame.php?gameName=astrominer"><h4>Astro Miner</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=snooker" title="Snooker"><img src="./img/snooker/start.png" alt="Snooker"/></a>
          <footer>
           <a href="./playGame.php?gameName=snooker"><h4>Snooker</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=hangman" title="Hangman"><img src="./img/hangman/start.png" alt="Hangman"/></a>
          <footer>
            <a href="./playGame.php?gameName=hangman"><h4>Hangman</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=hangman2014" title="Hangman 2014"><img src="./img/hangman2014/start.png" alt="Hangman 2014"/></a>
          <footer>
            <a href="./playGame.php?gameName=hangman2014"><h4>Hangman 2014</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=cashcombination" title="Cash Combination"><img src="./img/cash/start.png" alt="Cash Combination"/></a>
          <footer>
            <a href="./playGame.php?gameName=cashcombination"><h4>Cash Combination</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=pigsmightfly" title="Pigs Might Fly"><img src="./img/pigsmightfly/start.jpg" alt="Pigs Might Fly"/></a>
          <footer>
            <a href="./playGame.php?gameName=pigsmightfly"><h4>Pigs Might Fly</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=treasuresofthedeep" title="Treasures of the Deep"><img src="./img/treasure/start.png" alt="Treasures of the Deep"/></a>
          <footer>
            <a href="./playGame.php?gameName=treasuresofthedeep"><h4>Treasures of the Deep</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=luckystars" title="Lucky Stars"><img src="./img/luckystars/start.jpg" alt="Lucky Stars"/></a>
          <footer>
            <a href="./playGame.php?gameName=luckystars"><h4>Lucky Stars</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=hookaduck" title="Hook A Duck"><img src="./img/hookaduck/start.png" alt="Hook A Duck"/></a>
          <footer>
            <a href="./playGame.php?gameName=hookaduck"><h4>Hook A Duck</h4></a>
          </footer>
        </article>
      </section>
      <!-- ***** -->
      <section class="row">
        <h2>Themed</h2>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=snowmethemoney" title="Snow Me The Money"><img src="./img/snowmethemoney/start.png" alt="Snow Me The Money"/></a>
          <footer>
            <a href="./playGame.php?gameName=snowmethemoney"><h4>Snow Me The Money</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=footballfortunes" title="Football Fortunes"><img src="./img/footballfortunes/start.png" alt="Football Fortunes"/></a>
          <footer>
            <a href="./playGame.php?gameName=footballfortunes"><h4>Football Fortunes</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=meteormoney" title="Meteor Money"><img src="./img/meteormoney/start.png" alt="Meteor Money"/></a>
          <footer>
            <a href="./playGame.php?gameName=meteormoney"><h4>Meteor Money</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=christmasbonanza" title="Christmas Bonanza"><img src="./img/christmasbonanza/start.png" alt="Christmas Bonanza"/></a>
          <footer>
            <a href="./playGame.php?gameName=christmasbonanza"><h4>Christmas Bonanza</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=rockstoriches" title="Rocks To Riches"><img src="./img/rockstoriches/start.png" alt="Rocks To Riches"/></a>
          <footer>
            <a href="./playGame.php?gameName=rockstoriches"><h4>Rocks To Riches</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./playGame.php?gameName=festivefortune" title="Festive Fortune"><img src="./img/festivefortunes/start.png" alt="Festive Fortune"/></a>
          <footer>
            <a href="./playGame.php?gameName=festivefortune"><h4>Festive Fortune</h4></a>
          </footer>
        </article>
      </section>
      <!-- ***** -->
      <section class="row">
        <h2>HTML5</h2>
        <article class="col-md-4">
          <a href="./game/millionaire777html5/index.html" title="Millionaire 777"><img src="./img/html5/millionaire/startmini.png" alt="Millionaire 777"/></a>
          <footer>
            <a href="./game/millionaire777html5/index.html"><h4>Millionaire 777</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/superrichhtml5/index.html" title="Super Rich"><img src="./img/html5/superrich/startmini.png" alt="Super Rich"/></a>
          <footer>
            <a href="./game/superrichhtml5/index.html"><h4>Super Rich</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/moneylineshtml5/index.html" title="Money Lines"><img src="./img/html5/moneylines/startmini.png" alt="Money Lines"/></a>
          <footer>
            <a href="./game/moneylineshtml5/index.html"><h4>Money Lines</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/4millionpurplehtml5/IWG/index.html" title="&pound;4 Million Purple"><img src="./img/html5/4mill/startmini.png" alt="&pound;4 Million Purple"/></a>
          <footer>
            <a href="./game/4millionpurplehtml5/IWG/index.html"><h4>&pound;4 Million Purple</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/100kyellowhtml5/IWG/index.html" title="&pound;100k Yellow"><img src="./img/html5/100kyellow/startmini.png" alt="&pound;100k Yellow"/></a>
          <footer>
            <a href="./game/100kyellowhtml5/IWG/index.html"><h4>&pound;100k Yellow</h4></a>
          </footer>
        </article>
      </section>
      <!-- ***** -->
      <section class="row sideplay">
        <h2>SidePlay Flash</h2>
        <article class="col-md-4">
          <a href="./game/portfolio/fruit/index.html" title="Fruit Fortune"><img src="./img/fruitfortune/start.png" alt="Fruit Fortune"/></a>
          <footer>
            <a href="./game/portfolio/fruit/index.html"><h4>Fruit Fortune</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/bakeoff/index.html" title="Bake Off"><img src="./img/bakeoff/startmini.png" alt="Bake Off"/></a>
          <footer>
            <a href="./game/portfolio/bakeoff/index.html"><h4>Bake Off</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/sherluck/index.html" title="Sherluck Holmes"><img src="./img/sherluckholmes/startmini.png" alt="Sherluck Holmes"/></a>
          <footer>
            <a href="./game/portfolio/sherluck/index.html"><h4>Sherluck Holmes</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/highstakes/index.html" title="High Stakes"><img src="./img/highstakes/startmini.jpg" alt="High Stakes"/></a>
          <footer>
            <a href="./game/portfolio/highstakes/index.html"><h4>High Stakes</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/sushimaster/index.html" title="Sushi Master"><img src="./img/sushimaster/startmini.jpg" alt="Sushi Master"/></a>
          <footer>
            <a href="./game/portfolio/sushimaster/index.html"><h4>Sushi Master</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/jackgold01pharoahsfortune/index.html" title="Jack Gold: Pharaoh's Fortune"><img src="./img/jackgold/pharoahsfortune/startmini.jpg" alt="Jack Gold: Pharoah's Fortune"/></a>
          <footer>
            <a href="./game/portfolio/jackgold01pharoahsfortune/index.html"><h4>Jack Gold: Pharaoh's Fortune</h4></a>
          </footer>
        </article>
      </section>
      <!-- ***** -->
      <section class="row sideplay">
        <h2>SidePlay HTML5</h2>
        <article class="col-md-4">
          <a href="./game/bakeoffhtml5/index.html" title="Aunt Millie's Bake Off"><img src="./img/html5/bakeoff/startmini.png" alt="Aunt Millie's Bake Off"/></a>
          <footer>
            <a href="./game/bakeoffhtml5/index.html"><h4>Aunt Millie's Bake Off</h4></a>
          </footer>
        </article>
         <article class="col-md-4">
          <a href="./game/sherluckhtml/index.html" title="Sherluck Holmes"><img src="./img/sherluckholmes/startmini.png" alt="Sherluck Holmes"/></a>
          <footer>
            <a href="./game/sherluckhtml/index.html"><h4>Sherluck Holmes</h4></a>
          </footer>
        </article>
         <article class="col-md-4">
          <a href="./game/highstakeshtml5/index.html" title="High Stakes"><img src="./img/highstakes/startmini.jpg" alt="High Stakes"/></a>
          <footer>
            <a href="./game/highstakeshtml5/index.html"><h4>High Stakes</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/fruitsoffortunehtml5/index.html" title="Fruits Of Fortune"><img src="./img/html5/fruit/startmini.png" alt="Fruits of Fortune"/></a>
          <footer>
            <a href="./game/fruitsoffortunehtml5/index.html"><h4>Fruits of Fortune</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/herdisland/index.html" title="Herd Island"><img src="./img/herdisland/startmini.jpg" alt="Herd Island"/></a>
          <footer>
            <a href="./game/portfolio/herdisland/index.html"><h4>Herd Island</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/twelve40-moneytree-html5/index.html" title="Money Tree"><img src="./img/moneytree/startmini.jpg" alt="Money Tree"/></a>
          <footer>
            <a href="./game/portfolio/twelve40-moneytree-html5/index.html"><h4>Money Tree</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/poppingcandy/index.html" title="Popping Candy"><img src="./game/portfolio/poppingcandy/small.jpg" alt="Popping Candy"/></a>
          <footer>
            <a href="./game/portfolio/poppingcandy/index.html"><h4>Popping Candy</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/diamondfortunes/index.html" title="Diamond Fortunes"><img src="./game/portfolio/diamondfortunes/small.jpg" alt="Diamond Fortunes"/></a>
          <footer>
            <a href="./game/portfolio/diamondfortunes/index.html"><h4>Diamond Fortunes</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/cihangman/index.html" title="Hangman Classic"><img src="./img/cihangman/startmini.jpg" alt="Hangman Classic"/></a>
          <footer>
            <a href="./game/portfolio/cihangman/index.html"><h4>Hangman Classic</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/sushimaster/index.html" title="Sushi Master"><img src="./img/sushimaster/startmini.jpg" alt="Sushi Master"/></a>
          <footer>
            <a href="./game/portfolio/sushimaster/index.html"><h4>Sushi Master</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/magicmoney/index.html" title="Magic Money"><img src="./game/portfolio/magicmoney/small.jpg" alt="magicmoney"/></a>
          <footer>
            <a href="./game/portfolio/magicmoney/index.html"><h4>Magic Money</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/christmas2014/iwg/index.html" title="Christmas 2014"><img src="./game/portfolio/christmas2014/small.png" alt="christmas2014"/></a>
          <footer>
            <a href="./game/portfolio/christmas2014/iwg/index.html"><h4>Channel Island Lottery: Christmas 2014</h4></a>
          </footer>
        </article>
        <article class="col-md-4">
          <a href="./game/portfolio/cowboy-cash-portfolio/index.html" title="Cowboy Cash"><img src="./game/portfolio/cowboy-cash-portfolio/small.png" alt="cowboy-cash-portfolio"/></a>
          <footer>
            <a href="./game/portfolio/cowboy-cash-portfolio/index.html"><h4>Cowboy Cash</h4></a>
          </footer>
        </article>
      </section>
    </section>
    <footer class="index">
      <div class="container">
        <h5><div id="footerDiv">&copy; SIDEPLAY ENTERTAINMENT LIMITED. </div><script>document.getElementById("footerDiv").innerHTML += new Date().getFullYear().toString();</script></h5>
      </div>
    </footer>
  </body>
</html>
