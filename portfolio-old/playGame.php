<?php
	require_once "php/connectioninfo.php";
	require_once "php/headerFooterInfo.php";
	if($_GET["gameName"] != ""){
		//$gameName = $_GET["gameName"];

   		$gameName = filter_input(INPUT_GET, 'gameName', FILTER_SANITIZE_STRING);
		echo '
			<!DOCTYPE html>
			<html>
				<head>
					<title>'.$COMPANYTITLE.'</title>

					<!-- META -->
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<meta name="Description" content="TBP" />
					<meta name="Keywords" content="TBP" />
					<meta name="location" content="United Kingdom" />
					<meta name="language" content="English" />
					<!-- CSS -->
					<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
				    <link rel="stylesheet" type="text/css" href="styles.css">

				    <link rel="javascript" type="text/css" href="bootstrap.min.js">

					<script language="javascript">AC_FL_RunContent = 0;</script>
					<script src="AC_RunActiveContent.js" language="javascript"></script>
					  <header>
					    <div class="container">
					      <img src="./img/logo.png" alt="'.$COMPANYTITLE.' Logo"/>
					    </div>
					  </header>
				</head>
		';
		$con = mysqli_connect($HOST,$USER,$PASS,$DBNAME);
		if(mysqli_connect_errno()){
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		} else {
			$queryToExecute = 'SELECT * FROM `Games` WHERE `GameName` = "'.$gameName.'"';
			if($result = mysqli_query($con, $queryToExecute)){
				$gameOutput = mysqli_fetch_array($result);

				mysqli_close($con);

				$gameFileLoc = $gameOutput["Directory"];
				$gameWidth = $gameOutput["GameWidth"];
				$gameHeight = $gameOutput["GameHeight"];
				$gameNameReadable = $gameOutput["ReadableGameName"];

				echo '
					<body>
						<section class="container">
						    <!-- ***** -->
						    <section class="row center">
						    	<h2>'.$gameNameReadable.'</h2>
						    	<article class="col-md-12">
									<script language="javascript">
										if (AC_FL_RunContent == 0) {
											alert("This page requires AC_RunActiveContent.js.");
										} else {
											AC_FL_RunContent(
												"codebase", "http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0",
												"width", "'.$gameWidth.'",
												"height", "'.$gameHeight.'",
												"src", "'.$gameFileLoc.'/test_offline",
												"quality", "high",
												"pluginspage", "http://www.macromedia.com/go/getflashplayer",
												"align", "middle",
												"play", "true",
												"loop", "true",
												"scale", "showall",
												"wmode", "transparent",
												"devicefont", "false",
												"id", "test_offline",
												"bgcolor", "#000000",
												"name", "test_offline",
												"menu", "true",
												"allowFullScreen", "false",
												"allowScriptAccess","sameDomain",
												"movie", "'.$gameFileLoc.'/test_offline",
												"salign", "",
												"base", "'.$gameFileLoc.'/"
											); //end AC code
										}
									</script>
									<noscript>
										<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="'.$gameWidth.'" height="'.$gameHeight.'" id="test_offline" align="middle">
										<param name="allowScriptAccess" value="sameDomain" />
										<param name="allowFullScreen" value="false" />
										<param name="movie" value="test_offline.swf" /><param name="quality" value="high" /><param name="wmode" value="transparent" /><param name="base" value="'.$gameFileLoc.'/" /><param name="bgcolor" value="#000000" /><embed src="'.$gameFileLoc.'/test_offline.swf" quality="high" wmode="transparent" bgcolor="#000000" width="'.$gameWidth.'" height="'.$gameHeight.'" name="test_offline" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
										</object>
									</noscript>
									<footer class="back">
										<a href="'.$PORTFOLIOPAGE.'"><h4>Go Back</h4></a>
									</footer>
								</article>
							</section>
						</section>
						<footer class="main">
							<div class="container">
							<h5><div id="footerDiv">&copy; '.$COMPANYNAME.'. </div><script>document.getElementById("footerDiv").innerHTML += new Date().getFullYear().toString();</script></h5>
							</div>
						</footer>
					</body>
				</html>
				';
			}
		}
	} else {
		echo "There has been an error. Please return to the home page and try again.";
		echo $HOMEPAGEEMBED;
	}


?>

        <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NLS84R"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NLS84R');</script>
    <!-- End Google Tag Manager -->
