/**
     * \file MasterSS.js
     * \brief Master SpriteSheet assets from Flash.
     * - EndMessBut
     * - endMessBG
     * - logo
     * - instructions
 */


(function (window) {
    "use strict";
    //set local paths to external files
    var camelot = window.com.camelot,
        iwg = camelot.iwg,
        images = window.com.camelot.core.iwgLoadQ.images,
        MasterSS = function () {
            //singletonCache
            if (typeof MasterSS.instance !== "object") MasterSS.instance = this;
            return MasterSS.instance;
        },
    //make an instance if needed
        _MasterSS = new MasterSS();
    //add and set "next":false to stop on last frame
    //Static
    MasterSS.VERSION = '0.0.1';
	MasterSS.spriteSheet = {
        "images": [images.masterSS],
        	"frames": [
				[1310, 982, 53, 230],
			    [1315, 690, 53, 230],
			    [1299, 1214, 65, 65],
			    [465, 1479, 75, 75],
			    [2, 2, 960, 640],
			    [1326, 324, 37, 65],
			    [2, 644, 860, 183,  0, 430, 92],
			    [2, 829, 859, 228],
			    [863, 854, 445, 350],
			    [1041, 1653, 138, 65],
			    [806, 1831, 138, 65],
			    [2, 1410, 543, 67],
			    [797, 1419, 543, 67],
			    [946, 1822, 157, 67, 0, 79, 33],
			    [948, 1488, 157, 67, 0, 79, 33],
			    [882, 1665, 157, 67, 0, 79, 33],
			    [789, 1499, 157, 67, 0, 79, 33],
			    [723, 1665, 157, 67, 0, 79, 33],
			    [630, 1499, 157, 67, 0, 79, 33],
			    [635, 1430, 157, 67, 0, 79, 33],
			    [638, 1361, 157, 67, 0, 79, 33],
			    [638, 1292, 157, 67, 0, 79, 33],
			    [638, 1223, 157, 67, 0, 79, 33],
			    [797, 1281, 563, 67],
			    [797, 1350, 543, 67],
			    [2, 1123, 676, 66],
			    [855, 1206, 442, 26, 0, 221, 13],
			    [434, 1765, 142, 142, 0, 71, 71],
			    [290, 1765, 142, 142, 0, 71, 71],
			    [465, 1617, 123, 123, 0, 61, 61],
			    [502, 1909, 123, 123, 0, 61, 61],
			    [377, 1909, 123, 123, 0, 61, 61],
			    [252, 1909, 123, 123, 0, 61, 61],
			    [127, 1909, 123, 123, 0, 61, 61],
			    [2, 1909, 123, 123, 0, 61, 61],
			    [1345, 162, 3, 160],
			    [695, 1059, 158, 162],
			    [1154, 690, 159, 162],
			    [1203, 526, 159, 162],
			    [146, 1765, 142, 142, 0, 71, 71],
			    [1084, 1891, 86, 86, 0, 43, 43],
			    [996, 1891, 86, 86, 0, 43, 43],
			    [982, 1734, 86, 86, 0, 43, 43],
			    [908, 1898, 86, 86, 0, 43, 43],
			    [820, 1898, 86, 86, 0, 43, 43],
			    [894, 1734, 86, 86, 0, 43, 43],
			    [732, 1898, 86, 86, 0, 43, 43],
			    [542, 1498, 86, 86, 0, 43, 43],
			    [547, 1410, 86, 86, 0, 43, 43],
			    [2, 1765, 142, 142, 0, 71, 71],
			    [1268, 1970, 59, 59],
			    [1233, 1909, 59, 59],
			    [1207, 1970, 59, 59],
			    [1172, 1909, 59, 59],
			    [1295, 1751, 59, 59],
			    [1105, 1781, 59, 59],
			    [1105, 1720, 59, 59],
			    [1304, 1690, 59, 59],
			    [1309, 1629, 59, 59],
			    [1248, 1608, 59, 59],
			    [1309, 1568, 59, 59],
			    [1125, 1549, 59, 59],
			    [1107, 1488, 59, 59],
			    [1064, 1557, 59, 59],
			    [465, 1556, 59, 59],
			    [1309, 391, 59, 59, 0, 30, 30],
			    [978, 1557, 84, 94, 0, 42, 57],
			    [964, 343, 345, 3],
			    [964, 196, 379, 95],
			    [964, 99, 379, 95],
			    [964, 2, 379, 95],
			    [797, 1734, 95, 95],
			    [881, 1568, 95, 95],
			    [784, 1568, 95, 95],
			    [700, 1740, 95, 95],
			    [687, 1568, 95, 95],
			    [590, 1586, 95, 95],
			    [627, 1901, 103, 106, 0, 51, 53],
			    [864, 741, 95, 95],
			    [864, 644, 95, 95],
			    [1228, 1869, 121, 38, 0, 60, 19],
			    [1228, 1829, 121, 38, 0, 60, 19],
			    [1166, 1789, 121, 38, 0, 60, 19],
			    [1105, 1842, 121, 38, 0, 60, 19],
			    [1172, 1749, 121, 38, 0, 60, 19],
			    [1181, 1709, 121, 38, 0, 60, 19],
			    [1181, 1669, 121, 38, 0, 60, 19],
			    [1186, 1568, 121, 38, 0, 60, 19],
			    [1125, 1610, 121, 38, 0, 60, 19],
			    [1207, 1528, 121, 38, 0, 60, 19],
			    [1207, 1488, 121, 38, 0, 60, 19],
			    [978, 1986, 121, 38, 0, 60, 19],
			    [855, 1986, 121, 38, 0, 60, 19],
			    [732, 1986, 121, 38, 0, 60, 19],
			    [578, 1861, 121, 38, 0, 60, 19],
			    [1166, 1234, 121, 38, 0, 60, 19],
			    [1043, 1234, 121, 38, 0, 60, 19],
			    [920, 1234, 121, 38, 0, 60, 19],
			    [797, 1234, 121, 38, 0, 60, 19],
			    [961, 801, 121, 38, 0, 60, 19],
			    [701, 1837, 103, 59, 0, 51, 29],
			    [2, 1091, 683, 30],
			    [2, 1059, 691, 30],
			    [964, 293, 360, 48],
			    [964, 348, 343, 45],
			    [1345, 110, 22, 50],
			    [1345, 56, 22, 52],
			    [1345, 2, 22, 52],
			    [964, 615, 188, 184],
			    [578, 1742, 120, 117],
			    [1310, 922, 58, 58, 0, 29, 29],
			    [964, 505, 237, 108, 0, 118, 54],
			    [964, 395, 237, 108, 0, 118, 54],
			    [2, 1479, 461, 284, 0, 230, 142],
			    [2, 1191, 634, 217, 0, 308, 111],
			    [1101, 1979, 104, 55],
			    [1203, 395, 104, 55],
			    [1203, 489, 161, 35],
			    [1203, 452, 161, 35],
			    [590, 1683, 131, 55, 0, 65, 27]
			],
			"animations": {
				"arrow_left":[0],
		        "arrow_right":[1],
		        "audio_OFF":[2],
		        "audio_ON":[3],
		        "bg_gradient":[4],
		        "bg_tile":[5],
		        "box_endgame":[6],
		        "box_long":[7],
		        "box_short":[8],
		        "button_finish":[9],
		        "button_finish_over":[10],
		        "endgame_congratulations":[11],
		        "endgame_lose":[12],
		        "endgame_win_10":[13],
		        "endgame_win_100":[14],
		        "endgame_win_1000":[15],
		        "endgame_win_15":[16],
		        "endgame_win_20":[17],
		        "endgame_win_200":[18],
		        "endgame_win_40":[19],
		        "endgame_win_5":[20],
		        "endgame_win_5000":[21],
		        "endgame_win_50000":[22],
		        "endgame_win_topprize":[23],
		        "endgame_youhavewon":[24],
		        "footer_bar":[25],
		        "footer_tax":[26],
		        "game1_coin":[27],
		        "game1_diamond":[28],
		        "game1_symbol_bars":[29],
		        "game1_symbol_chopper":[30],
		        "game1_symbol_coins":[31],
		        "game1_symbol_gem":[32],
		        "game1_symbol_necklace":[33],
		        "game1_symbol_safe":[34],
		        "game1and2_divs":[35],
		        "game1and2_highlight_a":[36],
		        "game1and2_highlight_b":[37],
		        "game1and2_highlight_c":[38],
		        "game2_bars":[39],
		        "game2_number1":[40],
		        "game2_number2":[41],
		        "game2_number3":[42],
		        "game2_number4":[43],
		        "game2_number5":[44],
		        "game2_number6":[45],
		        "game2_number7":[46],
		        "game2_number8":[47],
		        "game2_number9":[48],
		        "game2_wad":[49],
		        "game3_1":[50],
		        "game3_10":[51],
		        "game3_11":[52],
		        "game3_12":[53],
		        "game3_2":[54],
		        "game3_3":[55],
		        "game3_4":[56],
		        "game3_5":[57],
		        "game3_6":[58],
		        "game3_7":[59],
		        "game3_7_gold":[60],
		        "game3_8":[61],
		        "game3_9":[62],
		        "game3_equals":[63],
		        "game3_plus":[64],
		        "game3_question":[65],
		        "game3_symbol_start":[66],
		        "game3and4_divs":[67],
		        "game3and4_highlight_a":[68],
		        "game3and4_highlight_b":[69],
		        "game3and4_highlight_c":[70],
		        "game4_symbol_car":[71],
		        "game4_symbol_cards":[72],
		        "game4_symbol_champagne":[73],
		        "game4_symbol_crown":[74],
		        "game4_symbol_plane":[75],
		        "game4_symbol_ring":[76],
		        "game4_symbol_start":[77],
		        "game4_symbol_wallet":[78],
		        "game4_symbol_watch":[79],
		        "gameprize_10":[80],
		        "gameprize_100":[81],
		        "gameprize_1000":[82],
		        "gameprize_1000_win":[83],
		        "gameprize_100_win":[84],
		        "gameprize_10_win":[85],
		        "gameprize_15":[86],
		        "gameprize_15_win":[87],
		        "gameprize_20":[88],
		        "gameprize_200":[89],
		        "gameprize_200_win":[90],
		        "gameprize_20_win":[91],
		        "gameprize_40":[92],
		        "gameprize_40_win":[93],
		        "gameprize_5":[94],
		        "gameprize_5000":[95],
		        "gameprize_50000":[96],
		        "gameprize_50000_win":[97],
		        "gameprize_5000_win":[98],
		        "gameprize_5_win":[99],
		        "icon_super":[100],
		        "instruction_game1":[101],
		        "instruction_game2":[102],
		        "instruction_game3":[103],
		        "instruction_game4":[104],
		        "row1":[105],
		        "row2":[106],
		        "row3":[107],
		        "shimmer":[108],
		        "shimmerSmall":[109],
		        "shiner":[110],
		        "start_button_play":[111],
		        "start_button_play_over":[112],
		        "start_logo":[113],
		        "start_topprizes":[114],
		        "tab_winningnumber":[115],
		        "tab_winningsymbol":[116],
		        "tab_yournumbers":[117],
		        "tab_yoursymbols":[118],
		        "word_prize":[119]
			}

    };
    MasterSS.ss = null
    //private method
    iwg._class("iwg.lib.flassets.MasterSS", MasterSS);

}(window));
