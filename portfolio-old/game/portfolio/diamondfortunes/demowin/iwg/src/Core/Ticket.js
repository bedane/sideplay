/**
 * @class   Ticket
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader       = window.Loader,
        Helper       = Loader.Helper,
        ErrorInvoker = Loader.ErrorInvoker;

    /**
     * @param  Document
     * @param  (Object -> Object)?
     * @param  (Ticket -> Boolean)?
     * @param  Boolean?
     * @return Ticket
     */
    var Ticket = function(xml, parser, checker, doGenCheck) {

        // singleton pattern
        if (Ticket.instance instanceof Ticket) {
            return Ticket.instance;
        }
        Ticket.instance = this;

        // the specific parser is the identity function by default
        if (!Helper.isDefined(parser)) {
            var parser = function(xml){return xml;};
        }

        // the specific checker is the constant function for true
        if (!Helper.isDefined(checker)) {
            var checker = function(ticket){return true;};
        }

        /* resulting ticket object is the composition of the game-specific parser and general
          parser applied to the xml document */
        var _ticket             = Helper.compose(parser, genParser)(xml),
            _doGenCheck    = Helper.isDefined(doGenCheck) ? doGenCheck : true;

        /**
         * @return Object
         */
        this.getTicket = function() {
            checkPropExists();
            return _ticket;
        };

        /**
         * @return Boolean
         */
        this.getDoGenCheck = function() {
            return _doGenCheck;
        };

        init(checker);

    };

    Loader.instance.addToNamespace("window.Loader.Ticket", Ticket);
    Ticket.n = "Ticket";
    Ticket.v = "0.0.2";

    /**
     * @param  String?
     * @return Object
     */
    Ticket.prototype.getOutcome = function(prop) {
        checkPropExists("outcome");
        var ticket = this.getTicket();
        if (Helper.isDefined(prop)) {
            return ticket.outcome[prop];
        }
        return ticket.outcome;
    };

    /**
     * @param  String?
     * @return Object
     */
    Ticket.prototype.getParams = function(prop) {
        checkPropExists("params");
        var ticket = this.getTicket();
        if (Helper.isDefined(prop)) {
            return ticket.params[prop];
        }
        return ticket.params;
    };

    /**
     * @return Number
     */
    Ticket.prototype.getNumGames = function() {
        var games = 0;
        for (var prop in this.getTicket()) {
            if (prop[0] === "g") {
                games++;
            }
        }
        return games;
    };

    /**
     * @param  Number
     * @return Object
     */
    Ticket.prototype.getGame = function(num) {
        var prop = "g" + String(num);
        checkPropExists(prop);
        return this.getTicket()[prop].go;
    };

    /**
     * @return [Object]
     */
    Ticket.prototype.getGames = function() {
        var games = [];
        for (var i = 1; i <= this.getNumGames(); i++) {
            games.push(this.getGame(i));
        }
        return games;
    };

    /**
     * @param  Object -> Boolean
     * @return Undefined
     * @throws TicketError
     */
    function init(checker) {

        var ticket = Ticket.instance;

        // check for xml manipulation
        if ((ticket.getDoGenCheck() && !genChecker(ticket)) || !checker(ticket)) {
            ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "xml manipulation"});
        }

    }

    /**
     * @param  String?
     * @return Undefined
     * @throws TicketError
     */
    function checkPropExists(prop) {
        if (Helper.isDefined(prop) && !Helper.isDefined(Ticket.instance.getTicket()[prop])) {
            ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "ticket property `" +
                prop + "` is undefined"});
        }
    }

    /**
     * @param  Document
     * @return Object
     * @throws TicketError
     */
    function genParser(xml) {

        // convert the xml document to a plain javascript object
        xml = new X2JS().xml2json(xml);

        if (!Helper.isDefined(xml.ticket)) {
            ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "bad ticket structure"});
        }

        xml = xml.ticket;

        // raise a ticket error if the xml document doesn't follow the general ticket structure
        if (!Helper.isDefined(xml.outcome)  || !Helper.isDefined(xml.outcome.amount) ||
            !Helper.isDefined(xml.outcome.tier)  || !Helper.isDefined(xml.outcome.wT) ||
            !Helper.isDefined(xml.params)  || !Helper.isDefined(xml.params.pList)) {
            ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "bad ticket structure"});
        }
        var games = [];
        for (var prop in xml) {
            if (prop[0] === "g") {
                games.push(prop);
                xml[prop].go.forEach(function(go) {
                    if (!Helper.isDefined(go.p) || !Helper.isDefined(go.w)) {
                        ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "bad ticket structure"});
                    }
                });
            }
        }

        xml.outcome.wT = Number(xml.outcome.wT);
        if (xml.outcome.wT !== 0 && xml.outcome.wT !== 1) {
            ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "bad ticket data"});
        }

        // parse the xml attribute values from strings to their correct types
        xml.outcome.amount = Number(xml.outcome.amount);
        xml.outcome.tier   = Number(xml.outcome.tier);
        xml.outcome.wT     = Boolean(xml.outcome.wT);
        xml.params.pList   = xml.params.pList.split(",").map(Number);

        // raise a ticket error if any of the parsed attributes have bad values
        if ([xml.outcome.amount, xml.outcome.tier].concat(xml.params.pList).some(Helper.isNonPos) ||
            xml.params.pList.length === 0) {
            ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "bad ticket data"});
        }
        games.forEach(function(game) {
            xml[game].go.forEach(function(go) {
                go.w = Number(go.w);
                if (Helper.isNonPos(go.p) || go.p >= xml.params.pList.length ||
                    (go.w !== 0 && go.w !== 1)) {
                    ErrorInvoker.raise(ErrorInvoker.TicketError, {status: "bad ticket data"});
                }
                go.p = Number(go.p);
                go.w = Boolean(go.w);
            });
        });

        return xml;

    }

    /**
     * @param  Ticket
     * @return Boolean
     */
    function genChecker(ticket) {

        var amount = ticket.getOutcome("amount"),
            wT     = ticket.getOutcome("wT");

        if ((amount === 0 && wT) || (amount > 0 && !wT)) {
            return false;
        }

        var pList  = ticket.getParams("pList"),
            total  = 0;

        ticket.getGames().forEach(function(game) {
            game.forEach(function(go) {
                if (go.w) {
                    total += pList[go.p];
                }
            });
        });

        if (total !== amount) {
            return false;
        }

        return true;

    }

}(window));
