/**
 * @class   Legend
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader   = window.Loader,
        Defaults = Loader.Defaults;

    /**
     * @param  Object
     * @return Undefined
     */
    var Legend = function(args) {

        var _legendRows  = Defaults.getArgOrDefault("legendRows",  args, Defaults.legend),
            _revealKind  = Defaults.getArgOrDefault("revealKind",  args, Defaults.legend),
            _revealDelay = Defaults.getArgOrDefault("revealDelay", args, Defaults.legend),
            _hasWon      = false;

        /*
         * @return [{prize: GameAsset, row: [GameAsset]}]
         */
        this.getLegendRows = function() {
            return _legendRows;
        };

        /*
         * @return String
         */
        this.getRevealKind = function() {
            return _revealKind;
        };

        /*
         * @return Number
         */
        this.getRevealDelay = function() {
            return _revealDelay;
        };

        /*
         * @return Boolean
         */
        this.getHasWon = function() {
            return _hasWon;
        };

        /*
         * @param  [{prize: GameAsset, row: [GameAsset]}]
         * @return Undefined
         */
        this.setLegendRows = function(legendRows) {
            this._legendRows = legendRows;
        };

        /*
         * @param  String
         * @return Undefined
         */
        this.setRevealKind = function(revealKind) {
            this._revealKind = revealKind;
        };

        /*
         * @param  Number
         * @return Undefined
         */
        this.setRevealDelay = function(revealDelay) {
            this._revealDelay = revealDelay;
        };

        /*
         * @param  Boolean
         * @return Undefined
         */
        this.setHasWon = function(hasWon) {
            this._hasWon = hasWon;
        };

    };

    Loader.instance.addToNamespace("window.Loader.Legend", Legend);
    Legend.n = "Legend";
    Legend.v = "0.0.1";

    /**
     * @return Boolean
     */
    Legend.prototype.isSequentialReveal = function() {
        return this.getRevealKind() === "sequential";
    };

    /**
     * @return Boolean
     */
    Legend.prototype.isSingleRowReveal = function() {
        return this.getRevealKind() === "singleRow";
    };

    /**
     * @param  GameAsset
     * @param  [GameAsset]
     * @return Undefined
     */
    Legend.prototype.pushLegendRow = function(prize, row) {
        var legendRows = this.getLegendRows();
        legendRows.push({prize: prize, row: row});
        this.setLegendRows(legendRows);
    };

    /*
     * @param  [Number]
     * @param  Number
     * @return Undefined
     */
    Legend.prototype.updateLegend = function(ticketLabels, legendDelay) {

        var delay = legendDelay,
            loops = ticketLabels.length;

        // recursive iife simulates looping with a delay between loop executions
        (function loop(i, context) {

            // base case
            if (i === loops) {
                return;
            }

            setTimeout(function() {

                // row loop
                for (var j = 0; j < context.getLegendRows().length - 1; j++) {

                    var legendRowObj  = context.getLegendRows()[j],
                        revealedCount = 0;

                    // column loop for row j
                    for (var k = 0; k < legendRowObj.row.length; k++) {

                        // row j, column k has already been revealed
                        if (legendRowObj.row[k].getIsRevealed()) {
                            revealedCount++;
                        }
                        // row j, column k needs to be revealed
                        else if (legendRowObj.row[k].getTicketLabel() === ticketLabels[i].getTicketLabel()) {

                            legendRowObj.row[k].animate("reveal");
                            legendRowObj.row[k].setIsRevealed(true);
                            revealedCount++;

                            if (context.isSequentialReveal()) {
                                delay += context.getRevealDelay();
                            }
                            else if (context.isSingleRowReveal()) {
                                break;
                            }

                        }

                    }

                    // j is a winning row, so win reveal each of its columns and then win reveal its prize
                    if (revealedCount === legendRowObj.row.length && !legendRowObj.prize.getIsRevealed()) {

                        // column loop for row j
                        for (k = 0; k < legendRowObj.row.length; k++) {
                            legendRowObj.row[k].animate("winReveal");
                            legendRowObj.row[k].setIsWinRevealed(true);
                        }

                        legendRowObj.prize.animate("winReveal");
                        legendRowObj.prize.setIsWinRevealed(true);
                        delay += context.getRevealDelay();

                    }

                }

                // recursive step
                loop(i + 1, context);

            }, delay);

        })(0, this);

    };

    /*
     * @param  Number
     * @return Number
     */
    Legend.prototype.countLegendItems = function(ticketLabel) {
        return this.getLegendItems(function(item){return item.getTicketLabel() === ticketLabel;}).length;
    };

    /*
     * @param  GameAsset -> Boolean
     * @return [GameAsset]
     */
    Legend.prototype.getLegendItems = function(predicate) {
        // extract the 2d legend array's rows, concatenate the result, and filter (in) via the predicate
        return [].concat.apply([], this.getLegendRows().map(function(rowObj){return rowObj.row;})).filter(predicate);
    };

})(window);
