/**
 * @class   Sounds
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader       = window.Loader,
        Helper       = Loader.Helper,
        ErrorInvoker = Loader.ErrorInvoker;

    /**
     * @return Sounds
     */
    var Sounds = function() {

        // singleton pattern
        if (Sounds.instance instanceof Sounds) {
            return Sounds.instance;
        }
        Sounds.instance = this;

        // channels array
        var _channels = [];

        /**
         * @return [Channel]
         */
        this.getChannels = function() {
            return _channels;
        };

        /**
         * @param  Channel
         * @return Undefined
         */
        this.addChannel = function(channel) {
            _channels.push(channel);
        };

    };

    Loader.instance.addToNamespace("window.Loader.Sounds", Sounds);
    Sounds.n = "Sounds";
    Sounds.v = "0.0.1";

    /**
     * @param  Channel -> Boolean
     * @return [Channel]
     */
    Sounds.prototype.filter = function(predicate) {
        return this.getChannels().filter(predicate);
    };

    /**
     * @param  String
     * @return [Channel]
     */
    Sounds.prototype.getChannelsByKind = function(kind) {
        return this.getChannels().filter(function(channel) {
            return kind === "all" || channel.getKind() === kind;
        });
    };

    /**
     * @param  String
     * @param  String
     * @param  Object?
     * @return Undefined
     */
    Sounds.prototype.action = function(kind, action, args) {
        this.getChannelsByKind(kind).forEach(function(channel) {
            channel.do(action, args);
        });
    };

    /**
     * @param  String
     * @param  Boolean
     * @return Undefined
     */
    Sounds.prototype.setManualPause = function(kind, manualPause) {
        this.getChannelsByKind(kind).forEach(function(channel) {
            channel.setManualPause(manualPause);
        });
    };

    /**
     * @param  String
     * @return Undefined
     */
    Sounds.prototype.muteChannels = function(kind) {
        Sounds.instance.action(kind, "setMute", true);
    };

    /**
     * @param  String
     * @return Undefined
     */
    Sounds.prototype.unmuteChannels = function(kind) {
        Sounds.instance.action(kind, "setMute", false);
    };

    /**
     * @param  String
     * @param  Boolean?
     * @return Undefined
     */
    Sounds.prototype.pauseChannels = function(kind, doneManually) {
        Sounds.instance.action(kind, "pause");
        if (!Helper.isDefined(doneManually) || doneManually) {
            Sounds.instance.setManualPause(kind, true);
        }
    };

    /**
     * @param  String
     * @param  Boolean?
     * @return Undefined
     */
    Sounds.prototype.resumeChannels = function(kind, doneManually) {
        Sounds.instance.action(kind, "resume");
        if (!Helper.isDefined(doneManually) || doneManually) {
            Sounds.instance.setManualPause(kind, false);
        }
    };

    /**
     * @param  String
     * @param  Object
     * @param  Boolean?
     * @return Undefined
     */
    Sounds.prototype.playChannels = function(kind, args, doneManually) {
        Sounds.instance.action(kind, "play", args);
        if (!Helper.isDefined(doneManually) || doneManually) {
            Sounds.instance.setManualPause(kind, false);
        }
    };

    /**
     * @param  [createjs.Sound]?
     * @param  String?
     * @return Channel
     */
    Sounds.Channel = function(sounds, kind) {

        var _sounds      = Helper.isDefined(sounds) ? sounds : [],
            _kind        = Helper.isDefined(kind)   ? kind   : "effect",
            _manualPause = false;

        Sounds.instance.addChannel(this);

        /**
         * @return [createjs.Sound]
         */
        this.getSounds = function() {
            return _sounds;
        };

        /**
         * @return String
         */
        this.getKind = function() {
            return _kind;
        };

        /**
         * @return Boolean
         */
        this.getManualPause = function() {
            return _manualPause;
        };

        /**
         * @param  createjs.Sound
         * @return Undefined
         */
        this.addSound = function(sound) {
            _sounds.push(sound);
        };

        /**
         * @param  Boolean
         * @return Undefined
         */
        this.setManualPause = function(manualPause) {
            _manualPause = manualPause;
        };

    };

    /**
     * @param  String
     * @return createjs.Sound
     * @throws SoundsError
     */
    Sounds.Channel.prototype.getSoundByName = function(soundId) {

        var sound = this.getSounds().filter(function(sound) {
            return sound.name === soundId;
        })[0];

        if (!Helper.isDefined(sound)) {
            ErrorInvoker.raise(ErrorInvoker.SoundsError, {soundId: soundId});
        }

        return sound;

    };

    /**
     * @param  String
     * @param  Object?
     * @return Undefined
     */
    Sounds.Channel.prototype.do = function(action, args) {
        this.getSounds().forEach(function(sound) {
            sound[action](Helper.isDefined(args) ? args : null);
        });
    };

})(window);
