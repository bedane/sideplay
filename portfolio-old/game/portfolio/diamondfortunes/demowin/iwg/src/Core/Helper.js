/**
 * @class   Helper
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader       = window.Loader,
        SpriteSheets = Loader.SpriteSheets;

    /**
     * @return Helper
     */
    var Helper = function() {

        // singleton pattern
        if (Helper.instance instanceof Helper) {
            return Helper.instance;
        }
        Helper.instance = this;

    };

    Loader.instance.addToNamespace("window.Loader.Helper", Helper);
    Helper.n = "Helper";
    Helper.v = "0.0.2";

    /**
     * @param  Object
     * @return Boolean
     */
    Helper.isDefined = function(obj) {
        return typeof obj !== "undefined" && obj !== null;
    };

    /**
     * @param  String
     * @param  {x: Number, y: Number}?
     * @param  Number?
     * @param  Boolean?
     * @param  String?
     * @return createjs.Sprite
     */
    Helper.makeBitmapImage = function(spriteId, pos, alpha, doReg, ssId) {

        if (!Helper.isDefined(ssId)) {
            var ssId = "master";
        }

        var img = new createjs.Sprite(SpriteSheets.instance.getSpriteSheet(ssId));

        img.gotoAndStop(spriteId);
        img.name = spriteId;

        if (!Helper.isDefined(pos)) {
            var pos = {x: 0, y: 0};
        }

        img.x = pos.x;
        img.y = pos.y;

        var width  = img.getBounds().width,
            height = img.getBounds().height;

        if (Helper.isDefined(alpha) && 0 <= alpha && alpha < 1) {
            img.alpha = alpha;
        }

        if (!Helper.isDefined(doReg) || doReg) {
            img.regX = width  / 2;
            img.regY = height / 2;
        }

        return img;

    };

    /**
     * @param  String
     * @param  {width: Number, height: Number}
     * @return createjs.Shape
     */
    Helper.makeRepeatingImage = function(id, size) {

        var shape = new createjs.Shape(),
            img   = new Image();

        img.src = Loader.instance.getInclude(id).src;

        shape.graphics.beginBitmapFill(img, "repeat");
        shape.graphics.drawRect(0, 0, size.width, size.height);

        return shape;

    };

    /**
     * @param  String
     * @param  Boolean?
     * @return createjs.Sound
     */
    Helper.makeSound = function(id, playOnce) {

        var sound = createjs.Sound.createInstance(Loader.instance.getInclude(id).src);
        sound.name = id;

        if (Helper.isDefined(playOnce) && playOnce) {
            sound.on("complete", function() {
                sound.setMute(true);
            });
        }

        return sound;

    };

    /**
     * @param  createjs.Sprite
     * @param  Boolean?
     * @param  Boolean?
     * @return Undefined
     */
    Helper.setButtonStates = function(button, doDown, doOver) {

        var anim = button.currentAnimation,
            down = "",
            over = "";

        if (!Helper.isDefined(doDown) || doDown) {
            down = "_over";
        }
        if (!Helper.isDefined(doOver) || doOver) {
            over = "_over";
        }

        new createjs.ButtonHelper(button, anim, anim + over, anim + down);

    };

    /**
     * @param  createjs.Sprite
     * @param  (MouseEvent -> Undefined)?
     * @param  Boolean?
     * @param  (MouseEvent -> Undefined)?
     * @param  (MouseEvent -> Undefined)?
     * @param  (MouseEvent -> Undefined)?
     * @param  (MouseEvent -> Undefined)?
     * @return Undefined
     */
    Helper.setButtonListeners = function(button, click, clickOnce, over, out, down, up) {

        if (Helper.isDefined(click)) {
            button.on("click", click, null, Helper.isDefined(clickOnce) && clickOnce ? true : false);
        }
        if (Helper.isDefined(over)) {
            button.on("mouseover", over);
        }
        if (Helper.isDefined(out)) {
            button.on("mouseout", out);
        }
        if (Helper.isDefined(down)) {
            button.on("mousedown", down);
        }
        if (Helper.isDefined(up)) {
            button.on("mouseup", up);
        }

    };

    /**
     * @param  createjs.Sprite
     * @param  String
     * @param  String
     * @return createjs.Sprite
     */
    Helper.swapSpriteSheet = function(sprite, frame, spriteSheet) {
        return Helper.makeBitmapImage(frame, {x: sprite.x, y: sprite.y}, sprite.alpha, sprite.regX
            === sprite.getBounds().width / 2, SpriteSheets.instance.getSpriteSheet(spriteSheet));
    };

    /**
     * @param  createjs.DisplayObject
     * @return createjs.DisplayObject
     */
    Helper.centralise = function(target) {
        target.regX = target.spriteSheet._frames[target.currentFrame].rect.width  / 2;
        target.regY = target.spriteSheet._frames[target.currentFrame].rect.height / 2;
        return target;
    };

    /**
     * @param  createjs.Sprite
     * @param  Number
     * @return Undefined
     */
    Helper.setAnimSpeed = function(sprite, speed) {
        var anim = sprite.spriteSheet.getAnimation(sprite.currentAnimation);
        anim.speed = speed;
    };

    /**
     * @param  createjs.Container
     * @param  createjs.DisplayObject
     * @return Undefined
     */
    Helper.moveToTop = function(t) {

        t.parent.setChildIndex(t, t.parent.getNumChildren() - 1);

    };

    /**
     * @param  createjs.Container
     * @param  createjs.DisplayObject
     * @return Undefined
     */
    Helper.moveToBottom = function(container, child) {

    };

    /**
     * @param  createjs.Container
     * @param  createjs.DisplayObject
     * @param  createjs.DisplayObject
     * @return Undefined
     */
    Helper.moveBehind = function(container, child1, child2) {

    };

    /**
     * @param  Object -> Object
     * @param  Object -> Object
     * @return Object -> Object
     */
    Helper.compose = function(f, g) {
        return function(x) {
            return f(g(x));
        };
    };

    /**
     * @param  Undefined -> Undefined
     * @param  Undefined -> Undefined
     * @return Undefined -> Undefined
     */
    Helper.sequence = function(f, g) {
        return function() {
            f();
            g();
        };
    }

    /**
     * @param  String
     * @param  String
     * @return Boolean
     */
    Helper.hasSuffix = function(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    };

    /**
     * @param  String
     * @return String -> String
     */
    Helper.addPrefix = function(prefix) {
        return function(str) {
            return prefix + str;
        };
    };

    /**
     * @param  Number
     * @return Boolean
     */
    Helper.isOdd = function(num) {
        return num % 2 !== 0;
    };

    /**
     * @param  [Object]
     * @return Boolean
     */
    Helper.equals = function(arr1, arr2) {
        if (arr1.length !== arr2.length) {
            return false;
        }
        arr1.forEach(function(elem, i) {
            if (elem !== arr2[i]) {
                return false;
            }
        });
        return true;
    };

    /**
     * @param  Number
     * @param  Number
     * @param  (Number -> Number)?
     * @return [Number]
     */
    Helper.enumFromTo = function(from, to, next) {

        if (!Helper.isDefined(next)) {
            var next = function(x){return x + 1;};
        }

        var arr = [],
            i   = from;

        while (i <= to) {
            arr.push(i);
            i = next(i);
        }

        return arr;

    };

    /**
     * @param  [Object]
     * @param  Number?
     * @return Number
     */
    Helper.sum = function(array, length) {

        if (!Helper.isDefined(length)) {
            var length = array.length;
        }
        var arr = array.slice();
        arr.splice(length);

        return arr.reduce(function(num1, num2) {
            return num1 + num2;
        }, 0);

    };

    /**
     * @param  [Object]
     * @return [Object]
     */
    Helper.randomise = function(array) {

        var i = array.length,
            j = null,
            temp = null;

        // fisher-yates shuffle
        while (i > 0) {

            j = Math.floor(Math.random() * i);
            i -= 1;

            temp     = array[i];
            array[i] = array[j];
            array[j] = temp;

        }

        return array;

    };

    /**
     * @param  Number
     * @param  Number
     * @return Number
     */
    Helper.random = function(from, to) {
        return Math.floor(Math.random() * (to - from + 1) + from);
    };
    /**
     * @param  Number
     * @param  Number
     * @return Number
     */
    Helper.randomFloat = function(from, to) {
        return Math.random() * (to - from + 1) + from;
    };

    /**
     * @param  String
     * @param  String
     * @return String
     */
    Helper.dropAfterStr = function(str, match) {
        var index = str.indexOf(match);
        if (index !== -1) {
            return str.substring(0, index);
        }
        return str;
    };

    /**
     * @param  Object
     * @return Boolean
     */
    Helper.isNaN = function(obj) {
        return obj !== obj;
    };

    /*/
		Convert Prize Amount
	/*/
    Helper.checkObject = function (value, ref) {
        if (ref.hasOwnProperty(value)) {
            var icon = ref[value];
            return icon;
        }
    }

    /**
     * @param  Number
     * @return Boolean
     */
    Helper.isNonPos = function(num) {
        return Helper.isNaN(num) || num < 0;
    };

    /**
     * @return Undefined
     */
    Helper.emptyFunction = function(){};

})(window);
