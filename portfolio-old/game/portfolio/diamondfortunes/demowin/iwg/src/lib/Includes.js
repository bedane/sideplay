/**
 * @class   Includes
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    /**
     * @return Includes
     */
    var Includes = function() {

        // singleton pattern
        if (Includes.instance instanceof Includes) {
            return Includes.instance;
        }
        Includes.instance = this;

        var _includes = [

            // third party
            {src: "src/imports/js/TweenMax.min.js",       id: "TweenMax"},
            {src: "src/imports/js/xml2json.min.js",       id: "X2JS"},
            {src: "src/imports/js/bowser.min.js",         id: "Bowser"},
            {src: "src/imports/js/requestTimeout.min.js", id: "RequestTimeout"},

            // images
            {src: "src/imports/img/logo.png",             id: "logo"},

            // sounds
            {src: "src/imports/snd/playbutton.mp3",       id: "playSnd"},
            {src: "src/imports/snd/transition.mp3",       id: "transitionSnd"},
            {src: "src/imports/snd/game1reveal.mp3",      id: "g1RevealSnd"},
            {src: "src/imports/snd/game2reveal.mp3",      id: "g2RevealSnd"},
            {src: "src/imports/snd/prizereveal.mp3",      id: "prizeRevealSnd"},
            {src: "src/imports/snd/linewin.mp3",          id: "lineWinSnd"},
            {src: "src/imports/snd/endlose.mp3",          id: "endLoseSnd"},
            {src: "src/imports/snd/endwin.mp3",           id: "endWinSnd"},

            // ticket
            {src: "src/ticket.xml",                       id: "ticket"},

            // json
            {src: "src/imports/json/masterSS.json",       id: "master"},
            {src: "src/imports/json/g1RevealSS.json",     id: "g1Reveal"},
            {src: "src/imports/json/g2RevealSS.json",     id: "g2Reveal"},

            // javascript
            {src: "src/Core/SpriteSheets.js",             id: "SpriteSheets"},
            {src: "src/Core/Helper.js",                   id: "Helper"},
            {src: "src/Core/Defaults.js",                 id: "Defaults"},
            {src: "src/lib/Animate.js",                   id: "Animate"},
            {src: "src/lib/Overrides.js",                 id: "Overrides"},
            {src: "src/Core/ErrorInvoker.js",             id: "ErrorInvoker"},
            {src: "src/Core/Ticket.js",                   id: "Ticket"},
            {src: "src/Core/GameAsset.js",                id: "GameAsset"},
            {src: "src/Core/Events.js",                   id: "Events"},
            {src: "src/Core/Sounds.js",                   id: "Sounds"},
            {src: "src/Core/RevealQueue.js",              id: "RevealQueue"},
            {src: "src/Core/Canvas.js",                   id: "Canvas"},
            {src: "src/Core/SettingsCog.js",              id: "SettingsCog"},
            {src: "src/Core/FullScreen.js",               id: "FullScreen"},
            {src: "src/lib/MainGameLayout.js",            id: "MainGameLayout"},
            {src: "src/lib/Splash.js",                    id: "Splash"},
            {src: "src/lib/EndGame.js",                   id: "EndGame"},
            {src: "src/Game.js",                          id: "Game"}

        ];

        /**
         * @return [{src: String, id: String}]
         */
        this.getIncludes = function() {
            return _includes;
        };

    };

    window.Includes = Includes;

}(window));
