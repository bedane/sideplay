/**
 * @class   MainGameLayout
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader      = window.Loader,
        Helper      = Loader.Helper,
        Ticket      = Loader.Ticket,
        Canvas      = Loader.Canvas;

    /**
     * @return MainGameLayout
     */
    var MainGameLayout = function() {

        // singleton pattern
        if (MainGameLayout.instance instanceof MainGameLayout) {
            return MainGameLayout.instance;
        }
        MainGameLayout.instance = this;

        var _gameCont    = new createjs.Container(),
            _game1Tokens = ["chopper", "motorhome", "plane", "bars", "safe", "car", "house",
                             "bubbly", "bike", "wallet", "boat"].map(Helper.addPrefix("symbol_")),
            _game2Tokens = Helper.enumFromTo(1, 10).map(Helper.compose(Helper.addPrefix("ss"), String)).concat(
                           Helper.enumFromTo(1, 10).map(Helper.compose(Helper.addPrefix("gb"), String))),
            _gamePrizes  = ["p20000", "p500", "p100", "p50", "p20", "p10", "p8", "p5", "p2"];

        /**
         * @return createjs.Container
         */
        this.getGameContainer = function() {
            return _gameCont;
        };

        /**
         * @return [String]
         */
        this.getGame1Tokens = function() {
            return _game1Tokens;
        };

        /**
         * @return [String]
         */
        this.getGame2Tokens = function() {
            return _game2Tokens;
        };

        /**
         * @param  Number
         * @return [String]
         */
        this.getGamePrizes = function() {
            return _gamePrizes;
        };

        init();

    };

    Loader.instance.addToNamespace("window.Loader.MainGameLayout", MainGameLayout);
    MainGameLayout.n = "MainGameLayout";
    MainGameLayout.v = "0.0.1";

    /**
     * @return Undefined
     */
    MainGameLayout.prototype.showGamePanels = function() {

        var gameCont   = this.getGameContainer(),
            game1Cont  = gameCont.getChildByName("game1Container"),
            game2Cont  = gameCont.getChildByName("game2Container"),
            halfWidth  = Math.floor(Canvas.instance.getGameWidth() / 2),
            panelWidth = game1Cont.getBounds().width,
            transition = Sounds.instance.getChannels()[0].getSoundByName("transitionSnd");

        TweenMax.to(game1Cont, 15, {x: Math.floor((halfWidth - panelWidth) / 2) + 15,
            ease: Quad.easeOut, useFrames: true});
        TweenMax.to(game2Cont, 13, {x: halfWidth + Math.floor((halfWidth - panelWidth) / 2) - 15,
            ease: Quad.easeOut, useFrames: true, delay: 4});

        transition.play();
        TweenMax.delayedCall(10, function() {
            transition.play();
        }, null, null, true);

    };

    /**
     * @return Undefined
     */
    function init() {

        var canvas    = Canvas.instance,
            gameStage = canvas.getGameStage(),
            container = MainGameLayout.instance.getGameContainer();

        container.name = "gameContainer";
        container.addChild(Helper.makeBitmapImage("bg", null, null, false));
        gameStage.addChild(container);

        initGamePanels();

    }

    /**
     * @return Undefined
     */
    function initGamePanels() {

        var gameWidth     = Canvas.instance.getGameWidth(),
            main          = MainGameLayout.instance,
            game1Cont     = new createjs.Container(),
            game2Cont     = new createjs.Container(),
            game1Panel    = Helper.makeBitmapImage("panel_bg",           null, null, false),
            game2Panel    = Helper.makeBitmapImage("panel_bg",           null, null, false),
            halfPanel     = Math.floor(game1Panel.getBounds().width / 2),
            game1Tab      = Helper.makeBitmapImage("tab_game1",          {x: halfPanel,     y: 15}),
            game2Tab      = Helper.makeBitmapImage("tab_game2",          {x: halfPanel,     y: 15}),
            g1PanelSheen  = Helper.makeBitmapImage("panel_purplesheen",  {x: halfPanel - 5, y: 75}),
            g2PanelSheen  = Helper.makeBitmapImage("panel_purplesheen",  {x: halfPanel - 5, y: 75}),
            g1PanelBorder = Helper.makeBitmapImage("panel_goldframe",    {x: 0, y: -1}, null, false),
            g2PanelBorder = Helper.makeBitmapImage("panel_goldframe",    {x: 0, y: -1}, null, false),
            game1Instruct = Helper.makeBitmapImage("instructions_game1", {x: halfPanel, y: 410}),
            game2Instruct = Helper.makeBitmapImage("instructions_game2", {x: halfPanel, y: 410}),
            game1Labels   = Helper.makeBitmapImage("rows",               {x: 65, y: 207}),
            game2Labels   = Helper.makeBitmapImage("rows",               {x: 65, y: 207});

        game1Cont.x = gameWidth;
        game2Cont.x = gameWidth + game1Cont.x - 30;
        game1Cont.y = game2Cont.y = 145;

        game1Instruct.name = "game1Instructions";
        game2Instruct.name = "game2Instructions";
        game1Cont.name     = "game1Container";
        game2Cont.name     = "game2Container";

        game1Cont.addChild(game1Panel, game1Tab, g1PanelSheen);
        game2Cont.addChild(game2Panel, game2Tab, g2PanelSheen);

        var g1Tokens = main.getGame1Tokens(),
            g2Tokens = main.getGame2Tokens();

        for (var i = 1; i <= 4; i++) {
            game1Cont.addChild(makeGame1Row(i));
            game2Cont.addChild(makeGame2Row(i));
        }

        game1Cont.addChild(g1PanelBorder, makeReminder(), game1Instruct, game1Labels);
        game2Cont.addChild(g2PanelBorder, makeReminder(), game2Instruct, game2Labels);

        main.getGameContainer().addChild(game1Cont, game2Cont);
    }

    /**
     * @param  Number
     * @return createjs.Container
     */
    function makeGame1Row(row) {

        var rowCont    = makeRowCont(row),
            ticketGame = Ticket.instance.getGame(1),
            tokens     = MainGameLayout.instance.getGame1Tokens(),
            prizes     = MainGameLayout.instance.getGamePrizes(),
            isWinner   = ticketGame[row - 1].w;

        if (isWinner) {
            rowCont.addChild(makeWinHighlight("l", row));
        }

        if (row !== 1) {
            rowCont.addChild(makeDivider());
        }

        for (var i = 1; i <= 3; i++) {

            var tokenId = ticketGame[row - 1]["d" + (i - 1).toString()],
                colCont = makeColCont(1, row, i, tokens[tokenId - 1], tokenId);

            colCont.x = i * 80;
            rowCont.addChild(colCont);

        }

        rowCont.addChild(makePrizeCont(1, row, isWinner));
        return rowCont;

    }

    /**
     * @param  Number
     * @return createjs.Container
     */
    function makeGame2Row(row) {

        var rowCont    = makeRowCont(row),
            ticketGame = Ticket.instance.getGame(2),
            tokens     = MainGameLayout.instance.getGame2Tokens(),
            prizes     = MainGameLayout.instance.getGamePrizes(),
            isWinner   = ticketGame[row - 1].w;

        if (isWinner) {
            rowCont.addChild(makeWinHighlight("r", row));
        }

        if (row !== 1) {
            rowCont.addChild(makeDivider());
        }

        rowCont.addChild(Helper.makeBitmapImage("plus",   {x: 125, y: 15}));
        rowCont.addChild(Helper.makeBitmapImage("equals", {x: 220, y: 15}));

        for (var i = 1; i <= 2; i++) {

            var tokenId = ticketGame[row - 1]["b" + (i - 1).toString()],
                colCont = makeColCont(2, row, i, tokens[tokenId - 1], tokenId);

            colCont.x = i * 80 + (i - 1) * 10;

            if (isWinner) {
                var tokenW   = Helper.makeBitmapImage(tokens[tokenId + 9]);
                tokenW.name  = "tokenWin";
                tokenW.alpha = 0;
                colCont.addChild(tokenW);
            }

            rowCont.addChild(colCont);

        }

        colCont = new createjs.Container();
        colCont.x = 263;
        colCont.y = 13;

        var symbol = Helper.makeBitmapImage("questionmark", {x: -2, y: 2}),
            token  = Helper.makeBitmapImage(Helper.addPrefix("ss")(ticketGame[row - 1].t));

        token.alpha = 0;

        colCont.name = "col3";
        symbol.name  = "symbol";
        token.name   = "token";

        if (isWinner) {
            tokenW       = Helper.makeBitmapImage(Helper.addPrefix("gb")(ticketGame[row - 1].t));
            tokenW.name  = "tokenWin";
            tokenW.alpha = 0;
            colCont.addChild(tokenW);
        }

        colCont.addChild(token, symbol);
        rowCont.addChild(colCont);
        rowCont.addChild(makePrizeCont(2, row, isWinner));

        return rowCont;

    }

    /**
     * @param  Number
     * @return createjs.Container
     */
    function makeRowCont(row) {
        var rowCont = new createjs.Container();
        rowCont.name = "row" + row.toString();
        rowCont.x = 40;
        rowCont.y = row * 80 - 10;
        return rowCont;
    }

    /**
     * @oaram  Number
     * @oaram  Number
     * @oaram  Number
     * @oaram  String
     * @oaram  Number
     * @return createjs.Container
     */
    function makeColCont(game, row, col, token, tokenId) {

        var colCont = new createjs.Container(),
            token   = Helper.makeBitmapImage(token),
            symbol  = null;

        if (game === 1) {
            symbol = Helper.makeBitmapImage("g1_diamond");
        } else {
            symbol = Helper.makeBitmapImage("spin", null, null, false, "g2Reveal");
            symbol.scaleX = symbol.scaleY = 0.95;
        }

        colCont.y = 14;
        colCont.ticketLabel = tokenId;

        token.alpha = 0;

        colCont.name = "col" + col.toString();
        symbol.name  = "symbol";
        token.name   = "token";

        colCont.addChild(token, symbol);
        return colCont;

    }

    /**
     * @param  Number
     * @param  Number
     * @param  Boolean
     * @return createjs.Container
     */
    function makePrizeCont(game, row, isWinner) {

        var prizeCont   = new createjs.Container(),
            ticket      = Ticket.instance,
            ticketGame  = ticket.getGame(game),
            prizes      = MainGameLayout.instance.getGamePrizes(),
            prizeWins   = prizes.map(Helper.compose(Helper.addPrefix("pw"), function(str){return str.substring(1);})),
            prizeSymbol = Helper.makeBitmapImage("word_prize"),
            prizeToken  = Helper.makeBitmapImage(prizes[ticketGame[row - 1].p]);

        prizeCont.name   = "colPrize";
        prizeSymbol.name = "symbol";
        prizeToken.name  = "token";

        prizeCont.x = 340;
        prizeCont.y = 15;

        prizeCont.prizeValue = ticket.getParams("pList")[ticketGame[row - 1].p];
        prizeToken.alpha = 0;

        if (isWinner) {
            var prizeTokenW   = Helper.makeBitmapImage(prizeWins[ticketGame[row - 1].p], {x: 0, y: 0.5});
            prizeTokenW.name  = "tokenWin";
            prizeTokenW.alpha = 0;
            prizeCont.addChild(prizeTokenW);
        }

        prizeCont.addChild(prizeToken, prizeSymbol);
        return prizeCont;

    }

    /**
     * @param  Number
     * @return createjs.Sprite
     */
    function makeDivider() {
        return Helper.makeBitmapImage("divider", {x: 0, y: -25}, null, false);
    }

    /**
     * @param  String
     * @param  Number
     * @return createjs.Sprite
     */
    function makeWinHighlight(side, row) {
        var highlight   = Helper.makeBitmapImage(side + "h" + row.toString(), {x: 2, y: -25}, null, false);
        highlight.name  = "highlight";
        highlight.alpha = 0;
        return highlight;
    }

    /**
     * @return createjs.Shape
     */
    function makeReminder() {

        var reminder = new createjs.Shape();

        reminder.graphics.setStrokeStyle(9);
        reminder.graphics.beginStroke("#FFF");
        reminder.snapToPixel = true;
        reminder.graphics.drawRoundRect(37, 35, 410, 336, 17);

        reminder.shadow = new createjs.Shadow("#FFF", 0, 0, 15);
        reminder.alpha  = 0;
        reminder.name   = "reminder";

        return reminder;

    }

}(window));
