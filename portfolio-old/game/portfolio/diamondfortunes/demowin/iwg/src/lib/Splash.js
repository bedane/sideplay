/**
 * @class   Splash
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader         = window.Loader,
        Helper         = Loader.Helper,
        Sounds         = Loader.Sounds,
        Canvas         = Loader.Canvas,
        FullScreen     = Loader.FullScreen,
        SettingsCog    = Loader.SettingsCog,
        MainGameLayout = Loader.MainGameLayout;

    /**
     * @return Splash
     */
    var Splash = function() {

        // singleton pattern
        if (Splash.instance instanceof Splash) {
            return Splash.instance;
        }
        Splash.instance = this;

        init();

    };

    Loader.instance.addToNamespace("window.Loader.Splash", Splash);
    Splash.n = "Splash";
    Splash.v = "0.0.1";

    /**
     * @return Undefined
     */
    function init() {

        var canvas     = Canvas.instance,
            gameWidth  = Canvas.instance.getGameWidth(),
            main       = MainGameLayout.instance,
            gameCont   = main.getGameContainer(),
            logoCont   = new createjs.Container(),
            halfWidth  = Math.floor(gameWidth / 2),
            logoFunnel = Helper.makeBitmapImage("logo_splash_funnel", {x: halfWidth, y: 347}),
            logo       = Helper.makeBitmapImage("logo_splash",        {x: halfWidth, y: 190}),
            winUpTo    = Helper.makeBitmapImage("winupto",            {x: halfWidth, y: 355}),
            playButton = Helper.makeBitmapImage("play",               {x: halfWidth, y: 480});

        logoCont.name = "logoContainer";

        Helper.setButtonStates(playButton);
        Helper.setButtonListeners(playButton, function() {

            TweenMax.to(logoCont,   20, {scaleX: 0.74, scaleY: 0.74, x: Math.floor(halfWidth -
                (halfWidth * 0.74) - 6), y: -58, ease: Cubic.easeOut, useFrames: true});

            TweenMax.to(playButton, 20, {delay: 5,   alpha: 0, ease: Cubic.easeOut, useFrames: true});

            TweenMax.to(winUpTo,    20, {delay: 15,  alpha: 0, ease: Cubic.easeOut, useFrames: true,
                onComplete: function() {
                    main.showGamePanels();
                    gameCont.removeChild(winUpTo, playButton);
                    gameCont.setChildIndex(logoCont, 1);
            }});

            Sounds.instance.getChannels()[0].getSoundByName("playSnd").play();

        }, true);

        logoCont.addChild(logoFunnel, logo);
        gameCont.addChild(logoCont, winUpTo, playButton);

        canvas.getGameStage().setChildIndex(SettingsCog.instance.getContainer(), 1);

    }

}(window));
