/**
 * @class   Animate
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader = window.Loader,
        Helper = Loader.Helper;

    /**
     * @return Animate
     */
    var Animate = function() {

        // singleton pattern
        if (Animate.instance instanceof Animate) {
            return Animate.instance;
        }
        Animate.instance = this;

        var _g1Finished = false,
            _g2Finished = false;

        /**
         * @return Boolean
         */
        this.getG1Finished = function() {
            return _g1Finished;
        };

        /**
         * @return Boolean
         */
        this.getG2Finished = function() {
            return _g2Finished;
        };

        /**
         * @param Boolean
         */
        this.setG1Finished = function(g1Finished) {
            _g1Finished = g1Finished;
        };

        /**
         * @param Boolean
         */
        this.setG2Finished = function(g2Finished) {
            _g2Finished = g2Finished;
        };

    };

    Loader.instance.addToNamespace("window.Loader.Animate", Animate);
    Animate.n = "Animate";
    Animate.v = "0.0.2";

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.prototype.matchReveal = function(gameAsset) {

        var container = gameAsset.getContainer(),
            explosion = Helper.makeBitmapImage("shatter", null, null, null, "g1Reveal");

        container.addChild(explosion);
        explosion.play();
        explosion.on("animationend", function() {
            explosion.stop();
        });

        container.getChildByName("symbol").alpha = 0;
        this.tokenReveal(gameAsset);

        gameAsset.getAnimations().reveal.channel.getSoundByName("g1RevealSnd").play();

    };

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.prototype.equalsReveal = function(gameAsset) {

        var container = gameAsset.getContainer(),
            symbol    = container.getChildByName("symbol"),
            token     = container.getChildByName("token");

        symbol.play();
        Loader.Helper.setAnimSpeed(symbol, 6);

        TweenMax.to(symbol, 15, {delay: 10, alpha: 0, scaleY: 0, scaleX: 0, ease: Cubic.easeOut,
            useFrames: true});

        TweenMax.delayedCall(10, function() {
            this.tokenReveal(gameAsset);
        }, null, this, true);

        gameAsset.getAnimations().reveal.channel.getSoundByName("g2RevealSnd").play();

    };

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.prototype.equalsWinReveal = function(gameAsset) { // todo???
        var container = gameAsset.getContainer();
        container.getChildByName("token").alpha    = 0;
        container.getChildByName("tokenWin").alpha = 1;
    };

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.prototype.eqTotalReveal = function(gameAsset) {
        TweenMax.to(gameAsset.getContainer().getChildByName("symbol"), 15, {alpha: 0, ease:
            Cubic.easeOut, useFrames: true});
        this.tokenReveal(gameAsset);
    };

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.prototype.prizeReveal = function(gameAsset) {

        var container = gameAsset.getContainer(),
            symbol    = container.getChildByName("symbol"),
            token     = container.getChildByName("token");

        token.scaleX = token.scaleY = 0;

        TweenMax.to(symbol, 10, {alpha: 0, scaleX: 0.6, scaleY: 0.6, ease: Cubic.easeOut,
            useFrames: true});

        TweenMax.to(token, 5, {delay: 5, alpha: 1, scaleX: 1, scaleY: 1, ease: Cubic.easeOut,
            useFrames: true});

        gameAsset.getAnimations().reveal.channel.getSoundByName("prizeRevealSnd").play();

    };

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.prototype.prizeWinReveal = function(gameAsset) {

        var container = gameAsset.getContainer();

        TweenMax.to(container.getChildByName("token"), 10, {alpha: 0, ease: Cubic.easeOut,
            useFrames: true});

        TweenMax.to(container.getChildByName("tokenWin"), 10, {alpha: 1, ease: Cubic.easeOut,
            useFrames: true});

        TweenMax.to(container.parent.getChildByName("highlight"), 10, {alpha: 1, ease: Cubic.easeOut,
            useFrames: true});

        gameAsset.getAnimations().winReveal.channel.getSoundByName("lineWinSnd").play();

    };

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.prototype.tokenReveal = function(gameAsset) {

        var container = gameAsset.getContainer(),
            token     = container.getChildByName("token"),
            clone     = token.clone();

        clone.scaleX = clone.scaleY = 0;
        clone.y = token.y;
        clone.x = token.x;
        container.addChild(clone);

        TweenMax.to(token, 10, {scaleX: 0, scaleY: 0, ease: Cubic.easeOut, useFrames: true});

        TweenMax.to(clone, 10, {alpha: 0.9, scaleX: 1.3, scaleY: 1.3, ease: Cubic.easeOut,
            useFrames: true});

        TweenMax.to(clone, 10, {delay: 10, alpha: 1, scaleX: 1, scaleY: 1, ease: Cubic.easeOut,
            useFrames: true, onComplete: function() {
                clone.alpha = 0;
                token.scaleX = token.scaleY = token.alpha = 1;
            }
        });

    };

    /**
     * @param  Canvas
     * @return Undefined
     */
    Animate.prototype.idle = function(canvas) {

        if (!canvas.getIsPaused()) {

            var gameCont     = canvas.getGameStage().getChildByName("gameContainer"),
                g1Reminder   = gameCont.getChildByName("game1Container").getChildByName("reminder"),
                g2Reminder   = gameCont.getChildByName("game2Container").getChildByName("reminder"),
                splashCont   = gameCont.getChildByName("splashContainer"),
                playButton   = gameCont.getChildByName("play"),
                finishButton = gameCont.getChildByName("finish"),
                reminderOpts = {alpha: 1, repeat: 1, ease: "easeIn", yoyo: true, useFrames: true};

            if (Helper.isDefined(playButton) && playButton.alpha === 1) {
                Animate.instance.growShrink(playButton);
            }
            if (Helper.isDefined(finishButton) && finishButton.alpha === 1) {
                Animate.instance.growShrink(finishButton);
            }

            if (!Animate.instance.getG1Finished()) {
                TweenMax.to(g1Reminder, 10, reminderOpts);
            }
            if (!Animate.instance.getG2Finished()) {
                TweenMax.to(g2Reminder, 10, reminderOpts);
            }

        }

    };

    /**
     * @param  createjs.Sprite
     * @return Undefined
     */
    Animate.prototype.growShrink = function(sprite) {
        TweenMax.to(sprite, 10, {scaleX: 1.2, scaleY: 1.2, useFrames: true});
        TweenMax.to(sprite, 10, {delay: 15, scaleX: 1, scaleY: 1, useFrames: true});
    };

}(window));
