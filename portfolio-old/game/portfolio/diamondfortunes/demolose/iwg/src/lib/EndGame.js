/**
 * @class   EndGame
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader         = window.Loader,
        Helper         = Loader.Helper,
        Ticket         = Loader.Ticket,
        Sounds         = Loader.Sounds,
        Canvas         = Loader.Canvas,
        MainGameLayout = Loader.MainGameLayout;

    /**
     * @return EndGame
     */
    var EndGame = function() {

        // singleton pattern
        if (EndGame.instance instanceof EndGame) {
            return EndGame.instance;
        }
        EndGame.instance = this;

        init();

    };

    Loader.instance.addToNamespace("window.Loader.EndGame", EndGame);
    EndGame.n = "EndGame";
    EndGame.v = "0.0.1";

    /**
     * @return Undefined
     */
    EndGame.prototype.show = function() {

        var gameCont    = MainGameLayout.instance.getGameContainer(),
            game1Cont   = gameCont.getChildByName("game1Container"),
            game2Cont   = gameCont.getChildByName("game2Container"),
            endGameCont = gameCont.getChildByName("endGameContainer"),
            logo        = gameCont.getChildByName("logoContainer"),
            finish      = gameCont.getChildByName("finish"),
            isWinner    = Ticket.instance.getOutcome("wT"),
            effectsCh   = Sounds.instance.getChannels()[0];

        finish.alpha = 1;

        TweenMax.delayedCall(50, function() {

            /*TweenMax.to(game1Cont.getChildByName("game1Instructions"), 30, {
                alpha:     0,
                ease:      Cubic.easeOut,
                useFrames: true
            });
            TweenMax.to(game2Cont.getChildByName("game2Instructions"), 30, {
                alpha:     0,
                ease:      Cubic.easeOut,
                useFrames: true
            });*/

            TweenMax.to(game1Cont,   30, {y: 175, ease: Cubic.easeOut, useFrames: true});
            TweenMax.to(game2Cont,   30, {y: 175, ease: Cubic.easeOut, useFrames: true});
            TweenMax.to(endGameCont, 25, {delay: 5, y: 90,    ease: Cubic.easeOut, useFrames: true});
            TweenMax.to(logo,        25, {delay: 5, alpha: 0, ease: Cubic.easeOut, useFrames: true});
            TweenMax.to(finish,      25, {delay: 5, y: 186,   ease: Cubic.easeOut, useFrames: true,
                onComplete: function() {
                    if (isWinner) {
                        TweenMax.to(endGameCont.getChildByName("panel_endgame"), 10, {alpha: 0,
                            ease: Cubic.easeOut, useFrames: true, delay: 15});
                        TweenMax.to(endGameCont.getChildByName("white"), 10, {alpha: 0, ease:
                            Cubic.easeOut, useFrames: true, delay: 15});
                        TweenMax.to(endGameCont.getChildByName("gold"),  10, {alpha: 1, ease:
                            Cubic.easeOut, useFrames: true, delay: 15});
                        TweenMax.to(endGameCont.getChildByName("panel_endgame_win"), 10, {alpha: 1,
                            ease: Cubic.easeOut, useFrames: true, delay: 15});
                    }
                }});

            if (isWinner) {
                effectsCh.getSoundByName("endWinSnd").play();
            } else {
                effectsCh.getSoundByName("endLoseSnd").play();
            }

        }, null, null, true);

    };

    /**
     * @return Undefined
     */
    function init() {

        var canvas      = Canvas.instance,
            halfWidth   = Math.floor(canvas.getGameWidth() / 2),
            gameCont    = MainGameLayout.instance.getGameContainer(),
            amount      = Ticket.instance.getOutcome("amount"),
            endPanel    = Helper.makeBitmapImage("panel_endgame"),
            finish      = Helper.makeBitmapImage("finish", {x: halfWidth, y: -59}),
            endGameCont = new createjs.Container();

        finish.alpha  = 0;
        endGameCont.x = halfWidth;
        endGameCont.y = -160;

        endGameCont.name = "endGameContainer";
        endGameCont.addChild(endPanel);

        if (amount > 0) {

            var amountImg   = "end" + amount.toString(),
                whiteAmount = Helper.makeBitmapImage(amountImg,           {x: 0,  y: 34}),
                goldAmount  = Helper.makeBitmapImage(amountImg + "_win",  {x: -1, y: 36}),
                highlight   = Helper.makeBitmapImage("panel_endgame_win");

            goldAmount.alpha = 0;
            highlight.alpha  = 0;

            whiteAmount.name = "white";
            goldAmount.name  = "gold";

            endGameCont.addChild(highlight, Helper.makeBitmapImage("end_congrats",  {x: 0, y: -22}),
                goldAmount, whiteAmount);

        } else {
            endGameCont.addChild(Helper.makeBitmapImage("end_lose", {x: 0, y: -8}));
            Sounds.instance.getChannels()[0].getSoundByName("endLoseSnd").play();
        }

        Helper.setButtonStates(finish);
        Helper.setButtonListeners(finish, window.location.reload.bind(window.location), true);

        gameCont.addChild(endGameCont, finish);

    }

}(window));
