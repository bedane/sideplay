/**
 * @class   Overrides
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader       = window.Loader,
        SpriteSheets = Loader.SpriteSheets,
        Helper       = Loader.Helper,
        Animate      = Loader.Animate;

    /**
     * @return Overrides
     */
    var Overrides = function() {

        // singleton pattern
        if (Overrides.instance instanceof Overrides) {
            return Overrides.instance;
        }
        Overrides.instance = this;

    };

    Loader.instance.addToNamespace("window.Loader.Overrides", Overrides);
    Overrides.n = "Overrides";
    Overrides.v = "0.0.2";

    // sprite sheets default overrides
    Overrides.spriteSheets = {};

    // animate default overrides
    Overrides.animate = {};

    // error invoker default overrides
    Overrides.errorInvoker = {};

    // sounds default overrides
    Overrides.sounds = {};

    // events default overrides
    Overrides.events = {};

    // canvas default overrides
    Overrides.canvas = {

        screens: 0,
        init: function() {
            this.idleAnimation = new Animate().idle;
            delete this.init;
            return this;
        }

    }.init();

    // full screen default overrides
    Overrides.fullScreen = {};

    // setttings cog default overrides
    Overrides.settingsCog = {

        modalSize:     {width: 680, height: 484},
        modalCol:      "#FFF",
        modalRad:      17,
        borderWidth:   12,
        hasBackground: false,
        effectsImg:    "icon_allsounds",
        effectsPos:    function(){return {x: 0, y: 0};},
        effectsRegX:   false,
        init:          function() {

            if (!Helper.isDefined(SpriteSheets.instance)) {
                new SpriteSheets();
            }

            var initialScreen = new createjs.Container();
            initialScreen.addChild(Helper.makeBitmapImage("overlay_top", null, null, false));

            this.initial = initialScreen;
            delete this.init;

            return this;

        }

    }.init();

}(window));
