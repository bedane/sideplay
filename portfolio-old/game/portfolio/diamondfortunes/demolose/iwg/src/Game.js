/**
 * @class   Game
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader         = window.Loader,
        Animate        = Loader.Animate,
        Helper         = Loader.Helper,
        ErrorInvoker   = Loader.ErrorInvoker,
        Events         = Loader.Events,
        GameAsset      = Loader.GameAsset,
        Ticket         = Loader.Ticket,
        Sounds         = Loader.Sounds,
        Canvas         = Loader.Canvas,
        RevealQueue    = Loader.RevealQueue,
        Splash         = Loader.Splash,
        MainGameLayout = Loader.MainGameLayout,
        EndGame        = Loader.EndGame;

    /**
     * @return Game
     */
    var Game = function() {

        // singleton pattern
        if (Game.instance instanceof Game) {
            return Game.instance;
        }
        Game.instance = this;

        init();

    };

    Loader.instance.addToNamespace("window.Loader.Game", Game);
    Game.n = "Game";
    Game.v = "0.0.1";

    /**
     * @return Undefined
     */
    function init() {

        makeTicket();
        setupSounds();

        var main     = new MainGameLayout(),
            splash   = new Splash(),
            endGame  = new EndGame(),
            mechanic = setupMechanic(),
            animate  = Animate.instance,
            finished = function(game){return game.getIsFinished();};

        Events.instance.addListener("rowFinished", function() {

            if (mechanic.game1.every(finished)) {
                animate.setG1Finished(true);
            }
            if (mechanic.game2.every(finished)) {
                animate.setG2Finished(true);
            }
            if (animate.getG1Finished() && animate.getG2Finished()) {
                endGame.show();
            }

        });

        Loader.instance.deploy(false);

    }

    /**
     * @return Undefined
     */
    function makeTicket() {

        new Ticket(Loader.instance.getInclude("ticket"), function(obj) {
            for (var i = 0; i < obj["g1"].go.length; i++) {
                var go = obj["g1"].go[i];
                go.i  = Number(go.i);
                go.d0 = Number(go.d0);
                go.d1 = Number(go.d1);
                go.d2 = Number(go.d2);
            }
            for (i = 0; i < obj["g2"].go.length; i++) {
                go = obj["g2"].go[i];
                go.i  = Number(go.i);
                go.b0 = Number(go.b0);
                go.b1 = Number(go.b1);
                go.t  = Number(go.t);
            }
            return obj;
        }, function(ticket) {
            if (!Helper.equals(ticket.getParams("pList"), [20000,500,100,50,20,10,8,5,2])) {
                return false;
            }
            for (var i = 0; i < ticket.getGame(1).length; i++) {
                var go = ticket.getGame(1)[i];
                if ((go.w  && (go.d0 !== go.d1 || go.d1 !== go.d2)) ||
                    (!go.w &&  go.d0 === go.d1 && go.d1 === go.d2)) {
                    return false;
                }
            }
            for (i = 0; i < ticket.getGame(2).length; i++) {
                go = ticket.getGame(2)[i];
                if ((go.w  && (go.t !== 10 || go.b0 + go.b1 !== 10)) ||
                    (!go.w && (go.t === 10 || go.b0 + go.b1 === 10)) ||
                    (go.b0 + go.b1 !== go.t)) {
                    return false;
                }
            }
            return true;
        });

    }

    /**
     * @return Undefined
     */
    function setupSounds() {

        new Sounds.Channel([Helper.makeSound("playSnd"),        Helper.makeSound("transitionSnd"),
                            Helper.makeSound("g1RevealSnd"),    Helper.makeSound("g2RevealSnd"),
                            Helper.makeSound("prizeRevealSnd"), Helper.makeSound("lineWinSnd"),
                            Helper.makeSound("endLoseSnd"),     Helper.makeSound("endWinSnd")], "effect");

    }

    /**
     * @return {game1: [RevealQueue], game2: [RevealQueue]}
     */
    function setupMechanic() {

        var ticket       = Ticket.instance,
            animate      = Animate.instance,
            gameCont     = MainGameLayout.instance.getGameContainer(),
            g1Cont       = gameCont.getChildByName("game1Container"),
            g2Cont       = gameCont.getChildByName("game2Container"),
            effectsCh    = Sounds.instance.getChannels()[0],
            matchAnims   = {reveal:    {animation: animate.matchReveal.bind(animate),     delay: 20, channel: effectsCh},
                            winReveal: {animation: Helper.emptyFunction,                  delay: 0, channel: null}},
            equalsAnims  = {reveal:    {animation: animate.equalsReveal.bind(animate),    delay: 20, channel: effectsCh},
                            winReveal: {animation: animate.equalsWinReveal.bind(animate), delay: 20, channel: null}},
            eqTotalAnims = {reveal:    {animation: animate.eqTotalReveal.bind(animate),   delay: 30, channel: null},
                            winReveal: {animation: animate.equalsWinReveal.bind(animate), delay: 20, channel: null}},
            prizeAnims   = {reveal:    {animation: animate.prizeReveal.bind(animate),     delay: 20, channel: effectsCh},
                            winReveal: {animation: animate.prizeWinReveal.bind(animate),  delay: 20, channel: effectsCh}},
            games        = {game1: [], game2: []};

        for (var i = 1; i <= 4; i++) {

            var g1Row    = g1Cont.getChildByName("row" + i.toString()),
                g2Row    = g2Cont.getChildByName("row" + i.toString()),
                g1Assets = [],
                g2Assets = [];

            for (var j = 1; j <= 3; j++) {

                var cont  = g1Row.getChildByName("col" + j.toString()),
                    asset = new GameAsset(cont, matchAnims);
                asset.setTicketLabel(cont.ticketLabel);
                g1Assets.push(asset);

                cont  = g2Row.getChildByName("col" + j.toString());
                asset = new GameAsset(cont, j < 3 ? equalsAnims : eqTotalAnims, j === 3 ? false : true);
                asset.setTicketLabel(cont.ticketLabel);
                g2Assets.push(asset);

            }

            var g1PrizeCont  = g1Row.getChildByName("colPrize"),
                g2PrizeCont  = g2Row.getChildByName("colPrize"),
                g1PrizeAsset = new GameAsset(g1PrizeCont, prizeAnims),
                g2PrizeAsset = new GameAsset(g2PrizeCont, prizeAnims),
                g1IsWinner   = ticket.getGame(1)[i - 1].w,
                g2IsWinner   = ticket.getGame(2)[i - 1].w;

            g1PrizeAsset.setPrizeValue(g1PrizeCont.prizeValue);
            g2PrizeAsset.setPrizeValue(g2PrizeCont.prizeValue);

            games.game1.push(new RevealQueue(g1Assets, g1PrizeAsset, g1IsWinner));
            games.game2.push(new RevealQueue(g2Assets, g2PrizeAsset, g2IsWinner));

        }

        return games;

    }

})(window);
