/**
 * @class   SpriteSheets
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader = window.Loader;

    /**
     * @return SpriteSheets
     */
    var SpriteSheets = function() {

        // singleton pattern
        if (SpriteSheets.instance instanceof SpriteSheets) {
            return SpriteSheets.instance;
        }
        SpriteSheets.instance = this;

        // sprite sheets object of the form {master: createjs.SpriteSheet, ...}
        var _spriteSheets = {};

        /**
         * @param  String
         * @return createjs.SpriteSheet
         */
        this.getSpriteSheet = function(id) {
            return _spriteSheets[id];
        };

        /**
         * @param  String
         * @return Undefined
         */
        this.addSpriteSheet = function(id) {
            _spriteSheets[id] = new createjs.SpriteSheet(Loader.instance.getInclude(id));
        };

        init();

    };

    Loader.instance.addToNamespace("window.Loader.SpriteSheets", SpriteSheets);
    SpriteSheets.n = "SpriteSheets";
    SpriteSheets.v = "0.0.2";

    /**
     * @return Undefined
     */
    function init() {

        // make a sprite sheet for every json include
        Loader.instance.getIncludesOfType("json").forEach(function(json) {
            SpriteSheets.instance.addSpriteSheet(json.id);
        });

    }

})(window);
