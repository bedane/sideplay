/**
 * @class   Events
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader       = window.Loader,
        Helper       = Loader.Helper,
        ErrorInvoker = Loader.ErrorInvoker;

    /**
     * @return Events
     */
    var Events = function() {

        // singleton pattern
        if (Events.instance instanceof Events) {
            return Events.instance;
        }
        Events.instance = this;

        // event listeners object of the form {event1: [Object]? -> Undefined, ...}
        var _listeners = {};

        /**
         * @return Object
         */
        this.getListeners = function() {
            return _listeners;
        };

        /**
         * @param  String
         * @param  [Object]? -> Undefined
         * @return Undefined
         */
        this.addListener = function(eventId, fun) {
            if (!Helper.isDefined(_listeners[eventId])) {
                _listeners[eventId] = [fun];
            } else {
                _listeners[eventId].push(fun)
            }
        };

        /**
         * @param  String
         * @param  [Object]? -> Undefined
         * @return Undefined
         * @throws EventsError
         */
        this.removeListener = function(eventId, fun) {

            // raise an EventsError if there aren't any event listeners for `eventId`
            if (!Helper.isDefined(_listeners[eventId])) {
                ErrorInvoker.raise(ErrorInvoker.EventsError, {eventId: eventId});
            }

            var length = _listeners[eventId].length;

            // otherwise, if it exists, remove the specified listener
            for (var i = 0; i < length; i++) {
                if (_listeners[eventId][i] === fun) {
                    _listeners[eventId].splice(i, 1);
                }
            }

            // raise an EventsError if the specified listener doesn't exist
            if (length === _listeners[eventId].length) {
                ErrorInvoker.raise(ErrorInvoker.EventsError, {eventId: eventId});
            }

        };

    };

    Loader.instance.addToNamespace("window.Loader.Events", Events);
    Events.n = "Events";
    Events.v = "0.0.2";

    /**
     * @param  String
     * @param  [Object]?
     * @return Undefined
     * @throws EventsError
     */
    Events.prototype.dispatchEvent = function(eventId, args) {

        var listeners = Events.instance.getListeners()[eventId];

        // raise an EventsError if there aren't any event listeners for `eventId`
        if (!Helper.isDefined(listeners)) {
            ErrorInvoker.raise(ErrorInvoker.EventsError, {eventId: event});
        }

        // if an arguments array is not provided, set it to the empty array
        if (!Helper.isDefined(args)) {
            var args = [];
        }

        // apply the event listeners
        listeners.forEach(function(event) {
            event.apply(window, args);
        });

    };

})(window);
