/**
 * @class   SettingsCog
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader   = window.Loader,
        Helper   = Loader.Helper,
        Defaults = Loader.Defaults,
        Sounds   = Loader.Sounds,
        Canvas   = Loader.Canvas;

    /**
     * @param  Object
     * @return SettingsCog
     */
    var SettingsCog = function(args) {

        // singleton pattern
        if (SettingsCog.instance instanceof SettingsCog) {
            return SettingsCog.instance;
        }
        SettingsCog.instance = this;

        var _container  = new createjs.Container();

        /**
         * @return createjs.Container
         */
        this.getContainer = function() {
            return _container;
        };

        init.bind(this)(args);

    };

    Loader.instance.addToNamespace("window.Loader.SettingsCog", SettingsCog);
    SettingsCog.n = "SettingsCog";
    SettingsCog.v = "0.0.1";

    /**
     * @param  Object
     * @return Undefined
     */
    function init(args) {

        var cog          = Helper.makeBitmapImage(Defaults.getArgOrDefault("cogImg", args, Defaults.settingsCog),
                               Defaults.getArgOrDefault("cogPos", args, Defaults.settingsCog)),
            hitArea      = new createjs.Shape(),
            overlay      = new createjs.Shape(),
            alpha        = Defaults.getArgOrDefault("alpha", args, Defaults.settingsCog),
            modalBg      = new createjs.Shape(),
            modalSize    = Defaults.getArgOrDefault("modalSize",   args, Defaults.settingsCog),
            modalPos     = Defaults.getArgOrDefault("modalPos",    args, Defaults.settingsCog)(modalSize),
            borderWidth  = Defaults.getArgOrDefault("borderWidth", args, Defaults.settingsCog),
            modalCont    = new createjs.Container(),
            easing       = Defaults.getArgOrDefault("easing",       args, Defaults.settingsCog),
            modalPosFrom = Defaults.getArgOrDefault("modalPosFrom", args, Defaults.settingsCog)(modalSize, borderWidth, modalPos),
            modalPosTo   = Defaults.getArgOrDefault("modalPosTo",   args, Defaults.settingsCog)(modalPos),
            animTime     = Defaults.getArgOrDefault("animTime",     args, Defaults.settingsCog),
            animAlpha    = Defaults.getArgOrDefault("animFade",     args, Defaults.settingsCog) ? 0 : 1,
            gameStage    = Canvas.instance.getGameStage(),
            container    = this.getContainer();

        cog.hitArea = hitArea;
        hitArea.graphics.beginFill("#000").drawRect(0, 0, cog.getBounds().width, cog.getBounds().height);

        overlay.name  = "overlay";
        overlay.alpha = 0;

        overlay.graphics.beginFill(Defaults.getArgOrDefault("overlayCol", args, Defaults.settingsCog)).drawRect(0,
            0, 960000, 640000).endFill();

        modalBg.alpha = Defaults.getArgOrDefault("modalAlpha", args, Defaults.settingsCog);

        modalBg.graphics.beginStroke(Defaults.getArgOrDefault("borderCol", args, Defaults.settingsCog));
        modalBg.graphics.setStrokeStyle(borderWidth);

        modalBg.graphics.beginFill(Defaults.getArgOrDefault("modalCol", args, Defaults.settingsCog)).drawRoundRect(0,
            0, modalSize.width, modalSize.height, Defaults.getArgOrDefault("modalRad", args, Defaults.settingsCog)).endFill();

        modalCont.name  = "modalContainer";
        modalCont.alpha = 0;

        modalCont.x = modalPosFrom.x;
        modalCont.y = modalPosFrom.y;

        modalCont.addChild(modalBg);

        var toggle = function(event) {

            var gameCont = gameStage.getChildByName("gameContainer");
            gameCont.mouseEnabled = !gameCont.mouseEnabled;

            if (modalIsOn.bind(this)()) {
                TweenMax.to(cog,       animTime, {rotation: 0, ease: easing, useFrames: true});
                TweenMax.to(overlay,   animTime, {alpha:    0, ease: easing, useFrames: true});
                TweenMax.to(modalCont, animTime, {x: modalPosFrom.x, y: modalPosFrom.y, alpha: animAlpha,
                                ease: easing, useFrames: true});
            } else {
                TweenMax.to(cog,       animTime, {rotation: 90, ease: easing, useFrames: true});
                TweenMax.to(overlay,   animTime, {alpha: alpha, ease: easing, useFrames: true});
                TweenMax.to(modalCont, animTime, {x: modalPosTo.x, y: modalPosTo.y, alpha: 1,
                                ease: easing, useFrames: true});
            }

        }.bind(this);

        Helper.setButtonListeners(cog, toggle);
        Helper.setButtonListeners(overlay, toggle);

        container.addChild(overlay, modalCont, cog);

        if (Defaults.getArgOrDefault("hasEffects",    args, Defaults.settingsCog) ||
            Defaults.getArgOrDefault("hasBackground", args, Defaults.settingsCog)) {
            initSoundControls.bind(this)(args);
        }

        container.getChildByName("modalContainer").addChild(Defaults.getArgOrDefault("initial",
            args, Defaults.settingsCog));

        gameStage.addChild(container);
        gameStage.update();

    };

    /**
     * @param  Object
     * @return Undefined
     */
    function initSoundControls(args) {

        var buttonsCont     = new createjs.Container(),
            hasEffects      = Defaults.getArgOrDefault("hasEffects",    args, Defaults.settingsCog),
            hasBackground   = Defaults.getArgOrDefault("hasBackground", args, Defaults.settingsCog),
            modalSize       = Defaults.getArgOrDefault("modalSize",     args, Defaults.settingsCog),
            maxButtonHeight = 0;

        if (hasEffects) {

            var effectsImg    = Defaults.getArgOrDefault("effectsImg",    args, Defaults.settingsCog),
                effectsRegX   = Defaults.getArgOrDefault("effectsRegX",   args, Defaults.settingsCog),
                effectsButton = Helper.makeBitmapImage(effectsImg, null, null, effectsRegX),
                effectsSize   = {width: effectsButton.getBounds().width, height: effectsButton.getBounds().height},
                effectsPos    = Defaults.getArgOrDefault("effectsPos", args,
                                    Defaults.settingsCog)(modalSize.width, effectsSize.width),
                effectsHit    = new createjs.Shape();

            effectsButton.x = effectsPos.x;
            effectsButton.y = effectsPos.y;

            effectsHit.graphics.beginFill("#000").drawRect(0, 0, effectsSize.width, effectsSize.height);
            effectsButton.hitArea = effectsHit;

            Helper.setButtonListeners(effectsButton, function() {

                if (effectsButton.currentAnimation === effectsImg) {
                    effectsButton.gotoAndStop(effectsImg + "_off");
                    Sounds.instance.muteChannels("effect");
                } else {
                    effectsButton.gotoAndStop(effectsImg);
                    Sounds.instance.unmuteChannels("effect");
                }

            });

            buttonsCont.addChild(effectsButton);
            maxButtonHeight = effectsSize.height;

        }

        if (hasBackground) {

            var backgroundImg    = Defaults.getArgOrDefault("backgroundImg",  args, Defaults.settingsCog),
                backgroundRegX   = Defaults.getArgOrDefault("backgroundRegX", args, Defaults.settingsCog),
                backgroundButton = Helper.makeBitmapImage(backgroundImg, null, null, backgroundRegX),
                backgroundSize   = {width: backgroundButton.getBounds().width, height: backgroundButton.getBounds().height},
                backgroundPos    = Defaults.getArgOrDefault("backgroundPos", args,
                                       Defaults.settingsCog)(modalSize.width, backgroundSize.width),
                backgroundHit    = new createjs.Shape();

            backgroundButton.x = backgroundPos.x;
            backgroundButton.y = backgroundPos.y;

            backgroundHit.graphics.beginFill("#000").drawRect(0, 0, backgroundSize.width, backgroundSize.height);
            backgroundButton.hitArea = backgroundHit;

            Helper.setButtonListeners(backgroundButton, function() {

                if (backgroundButton.currentAnimation === backgroundImg) {
                    backgroundButton.gotoAndStop(backgroundImg + "_off");
                    Sounds.instance.pauseChannels("background");
                } else {
                    backgroundButton.gotoAndStop(backgroundImg);
                    Sounds.instance.resumeChannels("background");
                }

            });

            buttonsCont.addChild(backgroundButton);
            maxButtonHeight = Math.max(maxButtonHeight, backgroundSize.height);

        }

        buttonsCont.y = Defaults.getArgOrDefault("soundPosY", args, Defaults.settingsCog)(modalSize, maxButtonHeight);
        this.getContainer().getChildByName("modalContainer").addChild(buttonsCont);

    };

    /**
     * @return Boolean
     */
    function modalIsOn() {
        var container = this.getContainer();
        if (!Helper.isDefined(container) || container.getChildByName("overlay").alpha === 0) {
            return false;
        }
        return true;
    }

})(window);
