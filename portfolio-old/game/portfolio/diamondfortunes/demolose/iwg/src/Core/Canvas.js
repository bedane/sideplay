/**
 * @file Simulated singleton class for controlling canvas-related behaviour, including size and
 *       orientation, swipe screens, game state, frame ticker and idling reminder animations.
 * @version 1.0.3
 * @author Sideplay
 * @copyright Sideplay 2014
 */

(function(window) {

    "use strict";

    // imports
    var Loader       = window.Loader,
        Helper       = Loader.Helper,
        Defaults     = Loader.Defaults,
        ErrorInvoker = Loader.ErrorInvoker,
        Events       = Loader.Events,
        Sounds       = Loader.Sounds;

    /**
     * Lazily initialises the singleton Canvas instance.
     * @class
     * @classdesc Simulated singleton class for controlling canvas-related behaviour, including size and
     *   orientation, swipe screens, game state, frame ticker and idling reminder animations.
     * @alias Canvas
     * @param {?(object|undefined)} args
     *   Canvas configuration options.
     *     <ul><li><tt>args.size : ?{width: number, height: number} | undefined</tt><br>
     *       Canvas size in pixels. <tt>{width: 960, height: 640}</tt> by default.</li>
     *     <li><tt>args.fps : ?number | undefined</tt><br>
     *       Frame rate per second. <tt>30</tt> by default.</li>
     *     <li><tt>args.screens : ?number | undefined</tt><br>
     *       Nubmer of swipe screens. <tt>2</tt> by default.</li>
     *     <li><tt>args.idleThreshold : ?number | undefined</tt><br>
     *       Amount of idle user time in frames to wait before playing the reminder animation.
     *       <tt>150</tt> by default.</li>
     *     <li><tt>args.idleAnimation : ?function | undefined</tt><br>
     *       Idle reminder animation. <tt>Helper.emptyFunction</tt> by default.</li></ul>
     */
    var Canvas = function(args) {

        // singleton pattern
        if (Canvas.instance instanceof Canvas) {
            return Canvas.instance;
        }
        Canvas.instance = this;

        /**
         * Main game stage. <tt>new createjs.Stage("game")</tt> by default.
         * @private
         * @member {!number}
         */
        var _gameStage = new createjs.Stage("game");

        /**
         * Game width in pixels. <tt>960</tt> by default.
         * @private
         * @member {!number}
         */
        var _gameWidth = _gameStage.canvas.width;

        /**
         * Game height in pixels. <tt>640</tt> by default.
         * @private
         * @member {!number}
         */
        var _gameHeight = _gameStage.canvas.height;

        /**
         * Indicates game is in a paused state. <tt>true</tt> by default.
         * @private
         * @member {!boolean}
         */
        var _isPaused = true;

        /**
         * Indicates game view is locked on current swipe screen. <tt>false</tt> by default.
         * @private
         * @member {!boolean}
         */
        var _isLocked = false;

        /**
         * Swipe screen array. <tt>[]</tt> by default.
         * @private
         * @member {!createjs.Container[]}
         */
        var _screens = [];

        /**
         * Current number of ticks of idle user time. <tt>0</tt> by default.
         * @private
         * @member {!number}
         */
        var _idleTime = 0;

        /**
         * Amount of idle user time in frames to wait before playing the reminder animation.
         *   <tt>150</tt> by default.
         * @private
         * @member {!number}
         */
        var _idleThreshold = Defaults.getArgOrDefault("idleThreshold", args,
                Defaults.canvas);

        /**
         * Idle reminder animation. <tt>Helper.emptyFunction</tt> by default.
         * @private
         * @member {!function}
         */
        var _idleAnimation = Defaults.getArgOrDefault("idleAnimation", args,
                Defaults.canvas);

        var _initialResize = true;

        /**
         * Gets the main game stage.
         * @public
         * @return {!createjs.Stage}
         */
        this.getGameStage = function() {
            return _gameStage;
        };

        /**
         * Gets the game width in pixels.
         * @public
         * @return {!number}
         */
        this.getGameWidth = function() {
            return _gameWidth;
        };

        /**
         * Gets the game height in pixels.
         * @public
         * @return {!number}
         */
        this.getGameHeight = function() {
            return _gameHeight;
        };

        /**
         * Gets the game state.
         * @public
         * @return {!boolean}
         */
        this.getIsPaused = function() {
            return _isPaused;
        };

        /**
         * Gets whether the game view is locked on the current swipe screen.
         * @public
         * @return {!boolean}
         */
        this.getIsLocked = function() {
            return _isLocked;
        };

        /**
         * Gets the swipe screen array.
         * @public
         * @return {!createjs.Container[]}
         */
        this.getScreens = function() {
            return _screens;
        };

        /**
         * Gets the idle user time in ticks.
         * @public
         * @return {!number}
         */
        this.getIdleTime = function() {
            return _idleTime;
        };

        /**
         * Gets the amount of idle user time in frames to wait before playing the reminder animation.
         * @public
         * @return {!number}
         */
        this.getIdleThreshold = function() {
            return _idleThreshold;
        };

        /**
         * Gets the idle reminder animation.
         * @public
         * @return {!function}
         */
        this.getIdleAnimation = function() {
            return _idleAnimation;
        };

        /**
         *
         */
        this.getInitialResize = function() {
            if (_initialResize) {
                _initialResize = false;
                return true;
            }
            return false;
        };

        /**
         * Sets the game width in pixels.
         * @public
         * @param {!number} gameWidth - Game width in pixels to set.
         */
        this.setGameWidth = function(gameWidth) {
            _gameWidth = gameWidth;
        };

        /**
         * Sets the game height in pixels.
         * @public
         * @param {!number} gameHeight - Game height in pixels to set.
         */
        this.setGameHeight = function(gameHeight) {
            _gameHeight = gameHeight;
        };

        /**
         * Sets the game state.
         * @public
         * @param {!boolean} isPaused - Game state to set.
         */
        this.setIsPaused = function(isPaused) {
            _isPaused = isPaused;
        };

        /**
         * Sets whether the game view is locked on the current swipe screen.
         * @public
         * @param {!boolean} isLocked - Set whether the game view is locked on the current swipe screen.
         */
        this.setIsLocked = function(isLocked) {
            _isLocked = isLocked;
        };

        /**
         * Adds a container to the swipe screen array.
         * @public
         * @param {!createjs.Container} container - Swipe screen container to add.
         */
        this.addScreen = function(container) {
            _screens.push(container);
        };

        /**
         * Increments the amount of user idle time.
         * @public
         */
        this.incIdleTime = function() {
            _idleTime++;
        };

        /**
         * Resets the amount of user idle time.
         * @public
         */
        this.resetIdleTime = function() {
            _idleTime = 0;
        };

        // initialise with the overriding arguments object
        _init.bind(this)(args);

    };

    /**
     * Gets the games's main HTML canvas element.
     * @public
     * @return {!HTMLCanvasElement}
     */
    Canvas.prototype.getCanvas = function() {
        return this.getGameStage().canvas;
    };

    /**
     * Gets a specific screen container from the swipe screen array.
     * @public
     * @param {!number} pos - Position of the screen to get from the swipe screen array.
     * @return {!createjs.Container}
     * @throws {CanvasError} Screen at the given position must exist.
     */
    Canvas.prototype.getScreenAt = function(pos) {
        var screen = this.getScreens()[pos - 1];
        if (!Helper.isDefined(screen)) {
            ErrorInvoker.raise(ErrorInvoker.CanvasError, {screenNum: pos});
        }
        return screen;
    };

    /**
     * Resizes the canvas to fit the screen whilst preserving the aspect ratio.
     * @public
     */
    Canvas.prototype.resizeCanvas = function() {
        _resizeCanvas.bind(this)(null);
    };

    /**
     * Rotates the canvas if it is displayed in portrait.
     * @public
     */
    Canvas.prototype.fixOrientation = function() {
        _fixOrientation.bind(this)(null);
    };

    /**
     * Initialises the canvas with the overriding arguments object.
     * @private
     * @instance
     * @memberof Canvas
     * @this Canvas
     * @param {?(object|undefined)} args
     */
    function _init(args) {

        var events    = Events.instance,
            gameStage = this.getGameStage(),
            size      = Defaults.getArgOrDefault("size",    args, Defaults.canvas),
            screens   = Defaults.getArgOrDefault("screens", args, Defaults.canvas),
            fps       = Defaults.getArgOrDefault("fps",     args, Defaults.canvas);

        // enable touch and hover events on the game stage
        createjs.Touch.enable(gameStage);
        gameStage.enableMouseOver();

        // resize and flip the canvas when the window size and orientation is changed
        window.addEventListener("resize", _resizeCanvas.bind(this), false);
        window.addEventListener("orientationchange", _fixOrientation.bind(this), false);

        // reset the idle timer when the user clicks or moves and when a reveal plays and ends
        window.addEventListener("click",     this.resetIdleTime, false);
        window.addEventListener("mousemove", this.resetIdleTime, false);
        events.addListener("revealStart",    this.resetIdleTime);
        events.addListener("revealEnd",      this.resetIdleTime);
        events.addListener("winRevealStart", this.resetIdleTime);
        events.addListener("winRevealEnd",   this.resetIdleTime);

        // populate the swipe screen array
        for (var i = 0; i < screens; i++) {
            var cont  = new createjs.Container();
            cont.name = "screen" + (i + 1).toString();
            this.addScreen(cont);
        }

        // set the createjs ticker and synchronise its fps rate with TweenMax's ticker
        createjs.Ticker.addEventListener("tick", _tick.bind(this));
        createjs.Ticker.setFPS(fps);
        TweenMax.ticker.fps(fps);

    }

    /**
     * Resizes the canvas to fit the screen whilst preserving the aspect ratio.
     * @private
     * @instance
     * @memberof Canvas
     * @this Canvas
     * @param {?Event} event - Event object passed in when the function is called on
     *                         <tt>window.resize</tt> dispatch.
     */
    function _resizeCanvas(event) {

        var gameCanvas = this.getCanvas(),
            gameStage  = this.getGameStage(),
            width      = this.getGameWidth(),  // game width
            height     = this.getGameHeight(), // game height
            ratio      = width / height,       // aspect ratio
            newWidth   = window.innerWidth,    // window width
            newHeight  = window.innerHeight,   // window height
            newRatio   = newWidth / newHeight, // new aspect ratio
            scale      = 0;                    // scale amount

        // if the new aspect ratio is greater, adjust the width and scaling based on width
        if (newRatio > ratio) {
            newWidth = newHeight * ratio;
            scale    = ((width - newWidth) / width);

        // otherwise adjust the height and scaling based on height
        } else {
            newHeight = newWidth / ratio;
            scale     = ((height - newHeight) / height);
        }

        // apply the size and scaling adjustements
        if (!this.getInitialResize() && window.innerHeight < window.innerWidth) { // fix: prevents initial scale when in portrait
            gameCanvas.width  = newWidth;
            gameCanvas.height = newHeight;
            gameStage.scaleY  = gameStage.scaleX = 1 - scale;
        }

    }

    /**
     * Rotates the canvas if it is displayed in portrait.
     * @private
     * @instance
     * @memberof Canvas
     * @this Canvas
     * @param {?Event} event - Event object passed in when the function is called on
     *                         <tt>window.orientationchange</tt> dispatch.
     */
    function _fixOrientation(event) {

        // orientation is not an issue on desktop devices
        if (bowser.mobile || bowser.tablet) {

            var canvas      = this.getCanvas(),
                className   = canvas.className,
                loading     = document.getElementById("loading"),
                orientation = window.orientation;

            // if the orientation is defined, rotate the canvas accordingly
            if (Helper.isDefined(orientation)) {
                switch (orientation) {
                    case 0:
                        canvas.className = "portrait-up";
                        break;
                    case 90:
                        canvas.className = "landscape-right";
                        break;
                    default:
                        canvas.className = "landscape-right";
                }
            }

            // resize the canvas if it has been flipped
            if (className !== canvas.className) {
                _resizeCanvas.bind(this)(null);
            }

        }

    }

    /**
     * Called on the frame tick. Pauses/plays sounds and animations based on the game state and
     *   controls the idle user timer.
     * @private
     * @instance
     * @memberof Canvas
     * @this Canvas
     */
    function _tick() {

        var sounds    = Sounds.instance,
            gameStage = this.getGameStage();

        // pause all sounds and tweens when the game is paused
        if (this.getIsPaused()) {

            sounds.pauseChannels("all", false);
            TweenMax.getAllTweens(true).forEach(function(tween){tween.pause();});

        // otherwise resume all sounds (that haven't been manually paused) and tweens
        } else {

            sounds.filter(function(channel) {
                return !channel.getManualPause();
            }).forEach(function(channel) {
                channel.do("resume");
                channel.setManualPause(false);
            });

            TweenMax.getAllTweens(true).forEach(function(tween){tween.play();});
            gameStage.update();

        }

        // increment the idle timer
        this.incIdleTime();

        // if the idle time has met the threshold, play the prompt animation and reset the timer
        if (this.getIdleTime() >= this.getIdleThreshold()) {
            this.getIdleAnimation()(this);
            this.resetIdleTime();
        }

    }

    // add to the global loader namespace
    Loader.instance.addToNamespace("window.Loader.Canvas", Canvas);
    Canvas.n = "Canvas";
    Canvas.v = "1.0.3";

})(window);
