/**
 * @class   GameAsset
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader       = window.Loader,
        Helper       = Loader.Helper,
        ErrorInvoker = Loader.ErrorInvoker;

    /**
     * @param  createjs.Container
     * @param  Object
     * @param  Boolean?
     * @return GameAsset
     */
    var GameAsset = function(container, animations, isClickable, isEnabled) {

        var _container     = container,  // parent container

            /* animations object of the form
               {reveal: {animation: GameAsset -> Undefined, delay: Number, channel: Channel}, ...} */
            _animations    = animations,

            _isRevealed    = false,
            _isWinRevealed = false,
            _prizeValue    = 0,
            _isWinner      = false,
            _ticketLabel   = null,

            // game asset is clickable and enabled by default
            _isClickable   = Helper.isDefined(isClickable) ? isClickable : true,
            _isEnabled     = Helper.isDefined(isEnabled)   ? isEnabled   : true;

        /**
         * @return createjs.Container
         */
        this.getContainer = function() {
            return _container;
        };

        /**
         * @return Object
         */
        this.getAnimations = function() {
            return _animations;
        };

        /**
         * @return Boolean
         */
        this.getIsRevealed = function() {
            return _isRevealed;
        };

        /**
         * @return Boolean
         */
        this.getIsWinRevealed = function() {
            return _isWinRevealed;
        };

        /**
         * @return Number
         */
        this.getPrizeValue = function() {
            return _prizeValue;
        };

        /**
         * @return Boolean
         */
        this.getIsWinner = function() {
            return _isWinner;
        };

        /**
         * @return Number
         */
        this.getTicketLabel = function() {
            return _ticketLabel;
        };

        /**
         * @return Boolean
         */
        this.getIsClickable = function() {
            return _isClickable;
        };

         /**
         * @return Boolean
         */
        this.getIsEnabled = function() {
            return _isEnabled;
        };


        /**
         * @param  Boolean
         * @return Undefined
         */
        this.setIsRevealed = function(isRevealed) {
            _isRevealed = isRevealed;
        };

        /**
         * @param  Boolean
         * @return Undefined
         */
        this.setIsWinRevealed = function(isWinRevealed) {
            _isWinRevealed = isWinRevealed;
        };

        /**
         * @param  Number
         * @return Undefined
         */
        this.setPrizeValue = function(prizeValue) {
            _prizeValue = prizeValue;
        };

        /**
         * @param  Boolean
         * @return Undefined
         */
        this.setIsWinner = function(isWinner) {
            _isWinner = isWinner;
        };

        /**
         * @param  Number
         * @return Undefined
         */
        this.setTicketLabel = function(ticketLabel) {
            _ticketLabel = ticketLabel;
        };

        /**
         * @param  Boolean
         * @return Undefined
         */
        this.setIsEnabled = function(isEnabled) {
            _isEnabled = isEnabled;
            if (!_isEnabled) {
                _isClickable = false;
            } else {
                _isClickable = true;
            }
        };

    };

    Loader.instance.addToNamespace("window.Loader.GameAsset", GameAsset);
    GameAsset.n = "GameAsset";
    GameAsset.v = "0.0.2";

    /**
     * @param  String
     * @return Undefined
     * @throws GameAssetError
     */
    GameAsset.prototype.animate = function(id) {

        var anims = this.getAnimations();

        // animate the game asset with the function `id` in its animation object, if it exists
        if (Helper.isDefined(anims[id]) && Helper.isDefined(anims[id].animation)) {
            anims[id].animation(this);
        } else {
            ErrorInvoker.raise(ErrorInvoker.GameAssetError, {animKey: id});
        }

    };

}(window));
