/**
 * @class   FullScreen
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader       = window.Loader,
        Helper       = Loader.Helper,
        Defaults     = Loader.Defaults,
        ErrorInvoker = Loader.ErrorInvoker,
        Events       = Loader.Events,
        Canvas       = Loader.Canvas;

    /**
     * @param  Object
     * @return FullScreen
     */
    var FullScreen = function(args) {

        // singleton pattern
        if (FullScreen.instance instanceof FullScreen) {
            return FullScreen.instance;
        }
        FullScreen.instance = this;

        init(args);

    };

    Loader.instance.addToNamespace("window.Loader.FullScreen", FullScreen);
    FullScreen.n = "FullScreen";
    FullScreen.v = "0.0.1";

    /**
     * @param  Object
     * @return Undefined
     */
    function init(args) {

        var isWebApp = Boolean(window.navigator.standalone),
            loading  = document.getElementById("loading"),
            menuBars = "80px";

        if (Boolean(bowser.mobile) && !isWebApp) {

            if (Boolean(bowser.android)) {
                menuBars   = "48px";
                killLoader = setupFullScreenAPI;
            }

            window.requestTimeout(function() {
                setOverflows("visible");
                document.getElementById("title").innerText = Defaults.getArgOrDefault("text", args,
                    Defaults.fullScreen);
                loading.style.paddingBottom = menuBars;
                loading.addEventListener("touchend", killLoader, false);
            }, 400);

        } else {
            killLoader();
        }

    }

    /**
     * @return Undefined
     */
    function killLoader() {

        var loading = document.getElementById("loading");
        if (loading.style.display === "none") {
            return;
        }

        var canvas   = Canvas.instance,
            gameArea = document.getElementById("gameArea");

        canvas.setIsPaused(false);

        window.requestTimeout(function() {
            gameArea.style.height = String(window.innerHeight) + "px";
            window.requestTimeout(function() {
                loading.style.display = "none";
            }, 200);
            setOverflows("hidden");
            canvas.resizeCanvas();
            loading.removeEventListener("touchend", killLoader, false);
        }, 200);

    }

    /**
     * @return Undefined
     */
    function setupFullScreenAPI() {

        var events  = Events.instance,
            canvas  = Canvas.instance,
            loading = document.getElementById("loading");

        canvas.resizeCanvas();

        events.addListener("fullScreenChange", function(isFullScreen) {

            if (isFullScreen) {
                canvas.setIsPaused(false);
                setOverflows("hidden");
                window.requestTimeout(function() {
                    loading.style.display = "none";
                }, 400);
            } else {
                canvas.setIsPaused(true);
                window.scrollTo(0, 0);
                setOverflows("visible");
                loading.style.opacity = "1";
                loading.style.display = "block";
            }

        });

        enterFullScreen();

    }

    /**
     * @return Undefined
     * @throws BrowserError
     */
    function enterFullScreen() {

        var canvas = document.getElementById("game"),
            engine = null;

        if (Helper.isDefined(canvas.requestFullscreen)) {

            engine = {prefix: "", isFullScreen: function(){return document.fullscreen;}};
            canvas.requestFullscreen();

        } else if (Helper.isDefined(canvas.webkitRequestFullscreen)) {

            engine = {prefix: "webkit", isFullScreen: function(){return document.webkitIsFullScreen;}};
            canvas.webkitRequestFullscreen();

        } else if (Helper.isDefined(canvas.mozRequestFullScreen)) {

            engine = {prefix: "moz", isFullScreen: function(){return document.mozFullScreen;}};
            canvas.mozRequestFullScreen();

        } else if (Helper.isDefined(canvas.msRequestFullscreen)) {

            engine = {prefix: "ms", isFullScreen: function(){return document.msFullscreenElement;}};
            canvas.msRequestFullscreen();

        } else {
            ErrorInvoker.raise(ErrorInvoker.BrowserError, {support: "full screen"});
        }

        canvas.addEventListener(engine.prefix + "fullscreenchange", function() {

            if (engine.isFullScreen()) {
                Events.instance.dispatchEvent("fullScreenChange", [true]);
            } else {
                Events.instance.dispatchEvent("fullScreenChange", [false]);
            }

        }, false);

    }

    /**
     * @param  String
     * @return Undefined
     */
    function setOverflows(value) {
        Array.prototype.slice.call(document.getElementsByClassName("fillPage")).forEach(function(elem) {
            elem.style.overflowY = value;
        });
    }

})(window);
