//Herd Island
var gameStage = new createjs.Stage(game),
maskerCont = new createjs.Container();

function start(){
	"use strict";
	herd.game.start();
}

function preload(){
	"use strict";
	herd.game.preload();
}

var herd = herd || {};

herd.game = (function(){
	"use strict";
	var self = {};

	self.endGameContainer 	= new createjs.Container();
	self.bgCont 			= new createjs.Container();
	self.gameCont 			= new createjs.Container();
	self.topCont 			= new createjs.Container();
	self.gearCont 			= new createjs.Container();
	self.gamesInstances 	= [];
	self.gameInstanceXTarget= [50,50,50,610,610,610];
	self.gameInstanceYTarget= [50,225,430,50,275,430];

	self.gameInstanceX 		= [-350,-350,-350,1010,1010,1010];
	self.gameInstanceY		= [50,225,430,50,275,430];

	self.bgDisplay 			= {};
	self.gameSets 			= [];
	self.soundObj 			= {};

	self.highImageManifest 	= [{id:"sSheet",src:"img/herdisland.png"},{id:"JSON",src:"img/herdisland.json"}, {id:"cowsSheet",src:"img/cow_revised.png"},{id:"cowJSON",src:"img/cow_revised.json"}, {id:"brandsSheet",src:"img/brander.png"},{id:"brandJSON",src:"img/brander.json"},{id:"butterflysSheet",src:"img/butterfly_new.png"},{id:"butterflyJSON",src:"img/butterfly_new.json"}];
	self.highSoundManifest 	= [{id:"clickSnd",src:"snd/click.mp3"},{id:"revealSnd",src:"snd/reveal.mp3"},{id:"winSnd",src:"snd/linewin.mp3"},{id:"bgSnd",src:"snd/backgroundnew.mp3"},{id:"egWinSnd",src:"snd/endgamewin.mp3"},{id:"egLoseSnd",src:"snd/endgamelose.mp3"},{id:"brandSnd",src:"snd/brand.mp3"},{id:"mooSnd",src:"snd/moo.mp3"}];
	self.ticketRaw 			= {};
	self.ticket 			= {};
	self.quality 			= null;
	self.banked 			= 0.00;

	self.isMuted			= false;
	self.isBGMuted			= false;

	self.turnGameAssoc 		= [-1,-1,-1,-1,-1,-1];
	self.triggeredGames 	= [false,false,false,false,false,false];
	self.turnsTaken 		= 0;
	self.endGameTriggered 	= false;
	self.winStatus			= false;

	self.statuses			= [[false,false,false],[false,false,false],[false,false,false],[false,false,false],[false,false,false],[false,false,false]];
	self.completedGames		= [false,false,false,false,false,false];
	self.winRevealGame		= [false,false,false,false,false,false];

	self.symbols 			= ["symbol_cheese","symbol_tractor","symbol_barn","symbol_strawberry","symbol_tools","symbol_shoot","symbol_tree","symbol_maize","symbol_radish","symbol_corn"];
	self.newSymbols 		= [];

	self.tailAnims			= ["tailUpDown","tailDownUp"];
	self.eyeAnims			= ["leftEye","rightEye","blink"];
	self.headAnims			= ["leftHead","rightHead"];
	self.butterflyAnims		= ["leftTop","rightRight","rightRight2","nose1"];

	self.butterflyTimer		= 400;
	self.butterflyTimerTarget = 500+Math.floor(Math.random()*20);
	self.cowHeadTimer		= 0;
	self.cowHeadTimerTarget = 1500+Math.floor(Math.random()*20);
	self.cowHeadButterflyPause	= false;
	self.cowTailTimer		= 0;
	self.cowTailTimerTarget = 30+Math.floor(Math.random()*20);
	self.cowEyeTimer		= 0;
	self.cowEyeTimerTarget  = 50+Math.floor(Math.random()*20);
	self.mooTimer			= 0;
	self.mooTimerTarget 	= 500+Math.floor(Math.random()*250);

	self.inactiveTimer		= 0;
	self.inactiveTimerTarget= 150;

	self.gameStarted		= false;
	self.animsEnabled		= false;
	self.allowMoo			= true;

	self.preload = function(){
		createjs.Ticker.addEventListener("tick", self.tick);
		createjs.Ticker.setFPS(30);
		TweenMax.ticker.fps(30);
		escapeIWLoader.scaleOrient.startResizeHandler();

		escapeIWLoader.overlayMasker.setMaskerColour("#52a032");
		escapeIWLoader.overlayMasker.mask();

		escapeIWLoader.preloader.setQualityEnabled(false);
		escapeIWLoader.preloader.setTicketFile({id:"ticketFromFile",src:"ticket.xml", type:createjs.LoadQueue.XML});
		escapeIWLoader.preloader.setManifests(self.highImageManifest,self.highSoundManifest,null);
		escapeIWLoader.preloader.startPreload();
		self.quality = escapeIWLoader.preloader.getQuality();
	};
	self.start = function(){
		self.newSymbols 		= escapeIWLoader.commonFunctions.randomiseArray(self.symbols);
		self.ticketRaw 			= escapeIWLoader.preloader.getPreloaderResults("ticketFromFile");
		self.ssImage 			= escapeIWLoader.preloader.getPreloaderResults("sSheet");
		self.sSheetJSON 		= escapeIWLoader.preloader.getPreloaderResults("JSON");
		self.sSheetJSON.images 	= [self.ssImage];

		self.cowssImage 			= escapeIWLoader.preloader.getPreloaderResults("cowsSheet");
		self.cowsSheetJSON 			= escapeIWLoader.preloader.getPreloaderResults("cowJSON");
		self.cowsSheetJSON.images 	= [self.cowssImage];

		self.brandssImage 			= escapeIWLoader.preloader.getPreloaderResults("brandsSheet");
		self.brandsSheetJSON 		= escapeIWLoader.preloader.getPreloaderResults("brandJSON");
		self.brandsSheetJSON.images = [self.brandssImage];

		self.butterflyssImage 			= escapeIWLoader.preloader.getPreloaderResults("butterflysSheet");
		self.butterflysSheetJSON 		= escapeIWLoader.preloader.getPreloaderResults("butterflyJSON");
		self.butterflysSheetJSON.images = [self.butterflyssImage];

		self.readTicketInfo();
		if(self.validateTicket()){
			gameStage.enableMouseOver();

			gameStage.addChild(self.bgCont);
			gameStage.addChild(self.gameCont);
			gameStage.addChild(self.topCont);
			gameStage.addChild(self.gearCont);
			gameStage.addChild(maskerCont);

			self.background();
			self.displayGames();
			self.registerSounds();
			self.landingScreen();
		} else {
			escapeIWLoader.errorInvoker.invokeError("Ticket failed validation test.");
		}
	};
	self.tick = function(){
		if(!self.endGameTriggered && self.gameStarted && self.animsEnabled){
			self.cowTailTimer+=1;
			self.cowEyeTimer+=1;
			if(!self.cowHeadButterflyPause){
				//head
				self.cowHeadTimer+=1;
				if(self.cowHeadTimer > self.cowHeadTimerTarget){
					self.cowAnim(self.headAnims[Math.floor(Math.random()*self.headAnims.length)]);
					self.cowHeadTimer = 0;
					self.cowHeadTimerTarget	= 150+Math.floor(Math.random()*20);
				}
				//butterfly
				self.butterflyTimer+=1;
				if(self.butterflyTimer > self.butterflyTimerTarget){
					self.butterflyAnim(self.butterflyAnims[Math.floor(Math.random()*self.butterflyAnims.length)]);
					self.butterflyTimer		= 0;
					self.butterflyTimerTarget = 1100+Math.floor(Math.random()*20);
				}
			}
			//tail
			if(self.cowTailTimer > self.cowTailTimerTarget){
				self.cowAnim(self.tailAnims[Math.floor(Math.random()*self.tailAnims.length)]);
				self.cowTailTimer = 0;
				self.cowTailTimerTarget	= 30+Math.floor(Math.random()*20);
			}
			//eye
			if(self.cowEyeTimer > self.cowEyeTimerTarget){
				self.cowAnim(self.eyeAnims[Math.floor(Math.random()*self.eyeAnims.length)]);
				self.cowEyeTimer = 0;
				self.cowEyeTimerTarget	= 50+Math.floor(Math.random()*20);
			}
			//inactive
			self.inactiveTimer+=1;
			if(self.inactiveTimer > self.inactiveTimerTarget){
				self.inactiveTimer = 0;
				self.inactiveHighlight();
			}
		}
		if(self.gameStarted && self.animsEnabled && self.allowMoo){
			self.mooTimer+=1;
			if(self.mooTimer > self.mooTimerTarget){
				self.soundObj.moo.play();
				self.mooTimer = 0;
				self.mooTimerTarget = 500+Math.floor(Math.random()*250);
			}
		}
		//workaround for the inability to tween the master volume.
		if(self.isMuted){
			if(createjs.Sound.getVolume() > 0){
				createjs.Sound.setVolume(createjs.Sound.getVolume() - (1/createjs.Ticker.getFPS()));
			} else {
				createjs.Sound.setVolume(0);
			}
		} else {
			if(createjs.Sound.getVolume() < 1){
				createjs.Sound.setVolume(createjs.Sound.getVolume() + (1/createjs.Ticker.getFPS()));
			} else {
				createjs.Sound.setVolume(1);
			}
		}

		gameStage.update();
	};
	self.readTicketInfo = function(){
		var i;
		self.ticket.prizeAmount	= parseInt(escapeIWLoader.commonFunctions.getTicketData(self.ticketRaw,"outcome",0,"amount"),10);
		self.ticket.prizeTier	= escapeIWLoader.commonFunctions.getTicketData(self.ticketRaw,"outcome",0,"prizeTier");
		self.ticket.winToken	= escapeIWLoader.commonFunctions.getTicketData(self.ticketRaw,"params",0,"wT");
		self.ticket.turns 		= [];
		for(i = 0; i < escapeIWLoader.commonFunctions.getTicketDataLength(self.ticketRaw,"turn"); i+=1){
			self.ticket.turns[i] 	= {};
			self.ticket.turns[i].s0 = parseInt(escapeIWLoader.commonFunctions.getTicketData(self.ticketRaw,"turn",i,"s0"),10);
			self.ticket.turns[i].s1 = parseInt(escapeIWLoader.commonFunctions.getTicketData(self.ticketRaw,"turn",i,"s1"),10);
			self.ticket.turns[i].p 	= parseInt(escapeIWLoader.commonFunctions.getTicketData(self.ticketRaw,"turn",i,"p"),10);
			self.ticket.turns[i].w 	= parseInt(escapeIWLoader.commonFunctions.getTicketData(self.ticketRaw,"turn",i,"w"),10);
		}
	};
	self.validateTicket = function(){return true;};

	self.background = function(){
		self.bgDisplay.background 	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,480,320,"bg","stop","center","center",1);
		self.bgDisplay.grass 		= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,0,640,"foreground","stop","left","bottom",1);
		self.bgDisplay.butterfly	= escapeIWLoader.commonFunctions.createSprites(self.butterflysSheetJSON,-100,100,"flap","play",null,null,1);
		self.bgDisplay.butterfly2	= escapeIWLoader.commonFunctions.createSprites(self.butterflysSheetJSON,580,-250,"flap","play",null,null,0);
		self.bgDisplay.logo 		= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,480,150,"logo","stop","center","center",1);
		self.bgDisplay.winPrize		= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,480,225,"end10000", "stop","center","center",0);
		self.bgDisplay.finishBody	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,480,525,"finish_bg","stop","center","center",1);
		self.bgDisplay.finishText	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,480,525,"word_play", "stop", "center", "center",1);
		self.bgDisplay.winUpTo		= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,480,280,"winupto","stop","center","center",1);

		self.cowCont 				= new createjs.Container();
		self.cowCont.x = 475; self.cowCont.y = 400;
		self.cowHeadCont 			= new createjs.Container();
		self.cowHeadCont.x = 0; self.cowHeadCont.y = 0;

		self.bgDisplay.cowTail		= escapeIWLoader.commonFunctions.createSprites(self.cowsSheetJSON,0,25,"tail","stop",null,null,1);
		self.bgDisplay.cowBody		= escapeIWLoader.commonFunctions.createSprites(self.cowsSheetJSON,-2,105,"body","stop",null,null,1);
		self.bgDisplay.cowHead		= escapeIWLoader.commonFunctions.createSprites(self.cowsSheetJSON,0,0,"head","stop",null,null,1);
		self.bgDisplay.cowLeftEye	= escapeIWLoader.commonFunctions.createSprites(self.cowsSheetJSON,-60,-30,"blink","stop",null,null,1);
		self.bgDisplay.cowRightEye	= escapeIWLoader.commonFunctions.createSprites(self.cowsSheetJSON,26,-60,"blink","stop",null,null,1);

		self.bgDisplay.cowRightEye.scaleX = -1;
		self.bgDisplay.cowRightEye.rotation = -40;
		self.bgDisplay.cowTail.regX = 52;
		self.bgDisplay.cowTail.regY = -72;

		self.cowCont.scaleX = 0.75;
		self.cowCont.scaleY = 0.75;

		self.bgDisplay.background.scaleX = 0.75;
		self.bgDisplay.background.scaleY = 0.75;

		self.bgCont.addChild(self.bgDisplay.background,self.bgDisplay.grass,self.bgDisplay.butterfly,self.cowCont);
		self.cowCont.addChild(self.bgDisplay.cowTail,self.bgDisplay.cowBody,self.cowHeadCont);
		self.cowHeadCont.addChild(self.bgDisplay.cowHead, self.bgDisplay.cowLeftEye, self.bgDisplay.cowRightEye, self.bgDisplay.butterfly2);
		self.topCont.addChild(self.bgDisplay.logo,self.bgDisplay.winUpTo, self.bgDisplay.winPrize, self.bgDisplay.finishBody, self.bgDisplay.finishText);
	};
	self.displayGames = function(){
		var i;
		self.churn0Coords 		= [[50,50], [50,50], [50,50], [150,45], [150,45], [150,45]];
		self.churn1Coords 		= [[150,45], [150,45], [150,45], [250,40], [250,40], [250,40]];
		self.symbol0Coords 		= [[50,50],  [50,50],  [50,50],  [150,45], [150,45], [150,45]];
		self.symbol1Coords 		= [[150,40], [150,40], [150,40], [250,50], [250,50], [250,50]];
		self.prizeTextCoords 	= [[250,40], [250,40], [250,40], [50,40], [50,40], [50,40]];
		self.brandCoords	 	= [[255,35], [255,35], [255,35], [55,35], [55,35], [55,35]];
		self.panelScaleX 		= [1,1,1,-1,-1,-1];
		self.gameRotation 		= [0,10,0,0,-10,0];
		self.churn0Rotation 	= [0,5,0,5,10,5];
		self.churn1Rotation 	= [-5,-5,-5,0,0,0];
		self.prizeTextRotation 	= [0,0,0,0,0,0];
		self.prizebrandRotation	= [-3,-3,-3,-3,-3,-3];

		for(i = 0; i < 6; i+=1){
			self.gameSets[i] = {};
			self.gamesInstances[i] 		= new createjs.Container();
			self.gameSets[i].panel 		= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,150,50,"panel","stop","center","center",1);
			self.gameSets[i].churn0 	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,self.churn0Coords[i][0],self.churn0Coords[i][1],"symbol_churn","stop","center","center",1);
			self.gameSets[i].churn1 	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,self.churn1Coords[i][0],self.churn1Coords[i][1],"symbol_churn","stop","center","center",1);
			self.gameSets[i].symbol0 	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,self.symbol0Coords[i][0],self.symbol0Coords[i][1],null,"stop","center","center",0);
			self.gameSets[i].symbol1 	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,self.symbol1Coords[i][0],self.symbol1Coords[i][1],null,"stop","center","center",0);
			self.gameSets[i].prizeText 	= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,self.prizeTextCoords[i][0],self.prizeTextCoords[i][1],"prize","stop","center","center",1);
			self.gameSets[i].NprizeText = escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,self.prizeTextCoords[i][0],self.prizeTextCoords[i][1],"prize","stop","center","center",0);
			self.gameSets[i].prizeBrand = escapeIWLoader.commonFunctions.createSprites(self.brandsSheetJSON,self.brandCoords[i][0],self.brandCoords[i][1],"reveal","stop",null,null,1);

			self.gamesInstances[i].x 			= self.gameInstanceX[i];
			self.gamesInstances[i].y 			= self.gameInstanceY[i];
			self.gameSets[i].panel.scaleX 		= self.panelScaleX[i];
			self.gamesInstances[i].rotation 	= self.gameRotation[i];
			self.gameSets[i].churn0.rotation 	= self.churn0Rotation[i];
			self.gameSets[i].churn1.rotation 	= self.churn1Rotation[i];
			self.gameSets[i].prizeText.rotation = self.prizeTextRotation[i];
			self.gameSets[i].prizeBrand.rotation= self.prizebrandRotation[i];

			self.gameSets[i].churn0.isRevealed = false;
			self.gameSets[i].churn1.isRevealed = false;
			self.gameSets[i].prizeText.isRevealed = false;

			self.gameSets[i].pTHitArea = new createjs.Shape();
			self.gameSets[i].pTHitArea.graphics.f("#000").dr(0,0,self.gameSets[i].prizeText.getBounds().width,self.gameSets[i].prizeText.getBounds().height);
			self.gameSets[i].prizeText.hitArea = self.gameSets[i].pTHitArea;

			self.gamesInstances[i].addChild(self.gameSets[i].panel,self.gameSets[i].churn0,self.gameSets[i].churn1,self.gameSets[i].symbol0,self.gameSets[i].symbol1,self.gameSets[i].NprizeText,self.gameSets[i].prizeText,self.gameSets[i].prizeBrand);
			self.gameCont.addChild(self.gamesInstances[i]);
		}
	};
	self.registerSounds = function(){
		self.soundObj.background	= new createjs.Sound.createInstance("bgSnd");
		self.soundObj.background.setVolume(0.25);
		self.soundObj.click 		= new createjs.Sound.createInstance("clickSnd");
		self.soundObj.reveal 		= new createjs.Sound.createInstance("revealSnd");
		self.soundObj.win 			= new createjs.Sound.createInstance("winSnd");
		self.soundObj.prizeReveal 	= new createjs.Sound.createInstance("brandSnd");
		self.soundObj.endGameWin	= new createjs.Sound.createInstance("egWinSnd");
		self.soundObj.endGameLose	= new createjs.Sound.createInstance("egLoseSnd");
		self.soundObj.moo 			= new createjs.Sound.createInstance("mooSnd");
	};
	self.setMute = function(){
		self.isMuted = !self.isMuted;
		//createjs.Sound.setMute(self.isMuted);

		//tweening mute stuff. not currently supported. will do something in the ticker.
		/*if(self.isMuted){
			//createjs.Tween.get(self.soundObj,{useTicks:true}).to({volume:0},30);
		} else {
			//createjs.Tween.get(self.soundObj,{useTicks:true}).to({volume:1},30);
		}*/
	};
	self.setBGMute = function(){
		self.isBGMuted = !self.isBGMuted;
		if(self.isBGMuted){
			createjs.Tween.get(self.soundObj.background,{useTicks:true}).to({volume:0},30);
			createjs.Tween.get(self.soundObj.moo,{useTicks:true}).to({volume:0},30);
		} else {
			createjs.Tween.get(self.soundObj.background,{useTicks:true}).to({volume:0.25},30);
			createjs.Tween.get(self.soundObj.moo,{useTicks:true}).to({volume:1},30);
		}
	};
	self.randomFromInterval = function (from, to) {
        return Math.floor(Math.random() * (to - from + 1) + from);
    };
    self.landingScreen = function(){
    	self.setupSettingsGear();
		self.bgDisplay.finishBody.on("mouseover",self.onStartButtonMouseOver);
		self.bgDisplay.finishBody.on("mouseout",self.onStartButtonMouseOut);
		self.bgDisplay.finishBody.on("click",self.onStartButtonClick);

		self.bgDisplay.finishText.on("mouseover",self.onStartButtonMouseOver);
		self.bgDisplay.finishText.on("mouseout",self.onStartButtonMouseOut);
		self.bgDisplay.finishText.on("click",self.onStartButtonClick);
    };
    self.setupSettingsGear = function(){
    	var styling 	= {},
		mutebutton 		= {},
		musicbutton 	= {};

		styling.background = {};
		styling.background.bgImage = escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,30,30,"settings_overlay","stop","left","top",1);

		styling.trim = {};
		styling.trim.trimImage = escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,0,0,"settings_overlay_glow","stop","left","top",1);

		//styling.containerX = 100;
		//styling.containerY = -600;

		//styling.animStyle = "fade";
		//styling.animEffectIn = createjs.Ease.elasticOut;
		//styling.animEffectOut= createjs.Ease.linear;
		//styling.animSpeed = 30;

		styling.gear = escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON, 920, 600, "settings_cog","stop","center","center",1);
		styling.cross= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON, 680, 60, "close","stop","center","center",1);

		mutebutton.create 		= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,200,460,"icon_allsounds","stop","center","center",1);
		mutebutton.button 		= "icon_allsounds";
		mutebutton.buttonPress 	= "icon_allsounds_off";
		mutebutton.action 		= herd.game.muteUnmuteAll;
		mutebutton.statusCheck 	= herd.game.getMuteStatus;

		musicbutton.create 		= escapeIWLoader.commonFunctions.createSprites(self.sSheetJSON,541,460,"icon_music","stop","center","center",1);
		musicbutton.button 		= "icon_music";
		musicbutton.buttonPress = "icon_music_off";
		musicbutton.action 		= herd.game.muteUnmuteBG;
		musicbutton.statusCheck = herd.game.getBGStatus;

		g.setup(self.gearCont,self.gameCont,styling,null,mutebutton,musicbutton);
    };
	self.landingScreenAction = function(){

		self.soundObj.background.play();
		self.soundObj.background.on("complete",function(){self.soundObj.background.play();});

		createjs.Tween.get(self.bgDisplay.finishText,{useTicks:true}).to({alpha:0},15);
		createjs.Tween.get(self.bgDisplay.finishBody,{useTicks:true}).to({alpha:0},15).call(function(){
			self.bgDisplay.finishBody.removeAllEventListeners();
			self.bgDisplay.finishText.removeAllEventListeners();
			createjs.Tween.get(self.bgDisplay.background,{useTicks:true}).to({scaleX:0.9, scaleY:0.9},30);
			createjs.Tween.get(self.bgDisplay.logo,{useTicks:true}).to({scaleX:0.75,scaleY:0.77},30);
			createjs.Tween.get(self.bgDisplay.winUpTo,{useTicks:true}).to({alpha:0},20);
			createjs.Tween.get(self.cowCont,{useTicks:true}).to({scaleX:1,scaleY:1},30);
			createjs.Tween.get(self.gamesInstances[0],{useTicks:true}).wait(15).to({x:self.gameInstanceXTarget[0]},30,createjs.Ease.bounceOut);
			createjs.Tween.get(self.gamesInstances[3],{useTicks:true}).wait(15).to({x:self.gameInstanceXTarget[3]},30,createjs.Ease.bounceOut);
			createjs.Tween.get(self.gamesInstances[1],{useTicks:true}).wait(25).to({x:self.gameInstanceXTarget[1]},30,createjs.Ease.bounceOut);
			createjs.Tween.get(self.gamesInstances[4],{useTicks:true}).wait(25).to({x:self.gameInstanceXTarget[4]},30,createjs.Ease.bounceOut);
			createjs.Tween.get(self.gamesInstances[2],{useTicks:true}).wait(35).to({x:self.gameInstanceXTarget[2]},30,createjs.Ease.bounceOut);
			createjs.Tween.get(self.gamesInstances[5],{useTicks:true}).wait(35).to({x:self.gameInstanceXTarget[5]},30,createjs.Ease.bounceOut).call(self.addGameListeners);
			//sounds
			createjs.Tween.get({},{useTicks:true}).wait(30).call(function(){self.soundObj.click.play();});
			createjs.Tween.get({},{useTicks:true}).wait(40).call(function(){self.soundObj.click.play();});
			createjs.Tween.get({},{useTicks:true}).wait(50).call(function(){self.soundObj.click.play();});

		});
	};
	self.butterflyAnim = function(anim){
		switch(anim){
			case "leftTop":
				self.bgDisplay.butterfly.x = -100; self.bgDisplay.butterfly.y = 100;
				TweenMax.to(self.bgDisplay.butterfly, 1000, {
					useFrames:true,
					bezier:{
						type:"soft",
						values:[
							{x:self.bgDisplay.butterfly.x += 300,	y:self.bgDisplay.butterfly.y += 0},
							{x:self.bgDisplay.butterfly.x += 180,	y:self.bgDisplay.butterfly.y += 140},
							{x:self.bgDisplay.butterfly.x += 240,	y:self.bgDisplay.butterfly.y += -120},
							{x:self.bgDisplay.butterfly.x += 80,	y:self.bgDisplay.butterfly.y += 200},
							{x:self.bgDisplay.butterfly.x += 0,		y:self.bgDisplay.butterfly.y += 140},
							{x:self.bgDisplay.butterfly.x += -100,	y:self.bgDisplay.butterfly.y += 60},
							{x:self.bgDisplay.butterfly.x += -400,	y:self.bgDisplay.butterfly.y += -280},
							{x:self.bgDisplay.butterfly.x += -200,	y:self.bgDisplay.butterfly.y += 240},
							{x:self.bgDisplay.butterfly.x += 220,	y:self.bgDisplay.butterfly.y += 0},
							{x:self.bgDisplay.butterfly.x += 200,	y:self.bgDisplay.butterfly.y += -280},
							{x:self.bgDisplay.butterfly.x += -220,	y:self.bgDisplay.butterfly.y += -200},
							{x:self.bgDisplay.butterfly.x += -300,	y:self.bgDisplay.butterfly.y += 100},
							{x:self.bgDisplay.butterfly.x = -100+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 100+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 300+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 100+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 480+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 240+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 720+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 120+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 800+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 320+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 800+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 460+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 700+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 520+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 300+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 240+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 100+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 480+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 320+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 480+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 520+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 200+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 240+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = -100+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = -100+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 100+self.randomFromInterval(-20,20)}
							],
					 	autoRotate:["x","y","rotation",90,false]},
						ease:Power0.easeInOut
					}
				);
			break;
			case "rightRight":
				self.bgDisplay.butterfly.x = 1060; self.bgDisplay.butterfly.y = 160;
				TweenMax.to(self.bgDisplay.butterfly, 1000, {
					useFrames:true,
					bezier:{
						type:"soft",
						values:[
							{x:self.bgDisplay.butterfly.x += -260,	y:self.bgDisplay.butterfly.y += 300},
							{x:self.bgDisplay.butterfly.x += -100,	y:self.bgDisplay.butterfly.y += 60},
							{x:self.bgDisplay.butterfly.x += -400,	y:self.bgDisplay.butterfly.y += -280},
							{x:self.bgDisplay.butterfly.x += -200,	y:self.bgDisplay.butterfly.y += 280},
							{x:self.bgDisplay.butterfly.x += 220,	y:self.bgDisplay.butterfly.y += 0},
							{x:self.bgDisplay.butterfly.x += 400,	y:self.bgDisplay.butterfly.y += -360},
							{x:self.bgDisplay.butterfly.x += 340,	y:self.bgDisplay.butterfly.y += 120},
							{x:self.bgDisplay.butterfly.x += 0,	y:self.bgDisplay.butterfly.y += 160},
							{x:self.bgDisplay.butterfly.x = 800+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 460+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 700+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 520+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 300+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 240+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 100+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 480+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 320+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 480+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 720+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 20+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 1060+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 0+self.randomFromInterval(-20,20)}
							],
					 	autoRotate:["x","y","rotation",90,false]},
						ease:Power0.easeInOut
					}
				);
			break;
			case "rightRight2":
				self.bgDisplay.butterfly.x = 1060; self.bgDisplay.butterfly.y = 0;
				TweenMax.to(self.bgDisplay.butterfly, 1000, {
					useFrames:true,
					bezier:{
						type:"soft",
						values:[
							{x:self.bgDisplay.butterfly.x += -340,	y:self.bgDisplay.butterfly.y += 120},
							{x:self.bgDisplay.butterfly.x += -240,	y:self.bgDisplay.butterfly.y += 120},
							{x:self.bgDisplay.butterfly.x += -180,	y:self.bgDisplay.butterfly.y += -140},
							{x:self.bgDisplay.butterfly.x += -200,	y:self.bgDisplay.butterfly.y += 380},
							{x:self.bgDisplay.butterfly.x += 220,	y:self.bgDisplay.butterfly.y += 0},
							{x:self.bgDisplay.butterfly.x += 400,	y:self.bgDisplay.butterfly.y += -360},
							{x:self.bgDisplay.butterfly.x += 80,	y:self.bgDisplay.butterfly.y += 200},
							{x:self.bgDisplay.butterfly.x += 0,		y:self.bgDisplay.butterfly.y += 140},
							{x:self.bgDisplay.butterfly.x += -100,	y:self.bgDisplay.butterfly.y += 60},
							{x:self.bgDisplay.butterfly.x += -220,	y:self.bgDisplay.butterfly.y += -280},
							{x:self.bgDisplay.butterfly.x += 40,	y:self.bgDisplay.butterfly.y += -40},
							{x:self.bgDisplay.butterfly.x += 200,	y:self.bgDisplay.butterfly.y += -80},
							{x:self.bgDisplay.butterfly.x += 80,	y:self.bgDisplay.butterfly.y += 200},
							{x:self.bgDisplay.butterfly.x += 260,	y:self.bgDisplay.butterfly.y += 160},
							{x:self.bgDisplay.butterfly.x += 0,		y:self.bgDisplay.butterfly.y += -480},
							{x:self.bgDisplay.butterfly.x = 720+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 120+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 480+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 240+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 300+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 100+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 100+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 480+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 320+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 480+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 720+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 120+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 800+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 320+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 800+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 460+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 700+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 520+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 480+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 240+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 520+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 200+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 720+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 120+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 800+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 320+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 1060+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 480+self.randomFromInterval(-20,20)},
							{x:self.bgDisplay.butterfly.x = 1060+self.randomFromInterval(-20,20),	y:self.bgDisplay.butterfly.y = 0+self.randomFromInterval(-20,20)}
							],
					 	autoRotate:["x","y","rotation",90,false]},
						ease:Power0.easeInOut
					}
				);
			break;
			case "nose1":
				self.cowHeadButterflyPause = true;
				self.cowAnim("leftHead");
				self.bgDisplay.butterfly2.alpha = 1;
				self.bgDisplay.butterfly2.x = 580; self.bgDisplay.butterfly2.y = -250;
				TweenMax.to(self.bgDisplay.butterfly2, 450, {
					useFrames:true,
					bezier:{
						type:"soft",
						values:[
							{x:580,	y:-250},
							{x:480,	y:-200},
							{x:300,	y:60},
							{x:0,	y:120},
							{x:-300,y:50},
							{x:-360,y:-200},
							{x:-240,y:-300},
							{x:100,	y:-300},
							{x:280,y:-50},
							{x:0,  y:120},
							{x:-60,y:60},
							{x:0,  y:0  }
							],
					 	autoRotate:["x","y","rotation",90,false]},
						ease:Power0.easeInOut,
						onComplete:function(){
							self.bgDisplay.butterfly2.gotoAndStop("sit");
							self.bgDisplay.butterfly2.scaleX = -1;
							createjs.Tween.get({},{useTicks:true}).wait(30).call(function(){
								createjs.Tween.get(self.cowHeadCont,{useTicks:true}).to({rotation:10},3).call(function(){
									createjs.Tween.get(self.cowHeadCont,{useTicks:true}).to({rotation:0},3).call(function(){
										createjs.Tween.get(self.cowHeadCont,{useTicks:true}).to({rotation:10},3).call(function(){
											createjs.Tween.get(self.cowHeadCont,{useTicks:true}).to({rotation:0},3).call(function(){});
											self.bgDisplay.butterfly2.gotoAndPlay("flap");
											self.bgDisplay.butterfly2.scaleX =1;
											TweenMax.to(self.bgDisplay.butterfly2, 200, {
												useFrames:true,
												bezier:{
													type:"soft",
													values:[
														{x:0,	y:-50},
														{x:100,	y:-70},
														{x:240,	y:10},
														{x:280,	y:-50},
														{x:260,	y:-180},
														{x:100,	y:-280},
														{x:-140,y:-200},
														{x:-320,y:-150},
														{x:-450,y:-200},
														{x:-500,y:-210}
														],
												 	autoRotate:["x","y","rotation",90,false]},
													ease:Power0.easeInOut,
													onComplete:function(){
														self.cowHeadButterflyPause = false;
														self.bgDisplay.butterfly2.alpha = 0;
													}
												}
											);
										});
									});
								});
							});
						}
					}
				);
			break;
		}
	};
	self.cowAnim = function(anim){
		//anim.
		switch(anim){
			case "leftEye":
				self.bgDisplay.cowLeftEye.play();
				self.bgDisplay.cowLeftEye.on("animationend",function(){self.bgDisplay.cowLeftEye.gotoAndStop("blink");},null,true);
			break;
			case "rightEye":
				self.bgDisplay.cowRightEye.play();
				self.bgDisplay.cowRightEye.on("animationend",function(){self.bgDisplay.cowRightEye.gotoAndStop("blink");},null,true);
			break;
			case "blink":
				self.bgDisplay.cowLeftEye.play();
				self.bgDisplay.cowRightEye.play();
				self.bgDisplay.cowLeftEye.on("animationend",function(){self.bgDisplay.cowLeftEye.gotoAndStop("blink");},null,true);
				self.bgDisplay.cowRightEye.on("animationend",function(){self.bgDisplay.cowRightEye.gotoAndStop("blink");},null,true);
			break;
			case "tailUpDown": //up down center. either 0-20-0--15-0 or 0-15-0--15-0
				createjs.Tween.get(self.bgDisplay.cowTail,{useTicks:true}).to({rotation:5+(Math.floor(Math.random()*15))},15).call(function(){
					createjs.Tween.get(self.bgDisplay.cowTail,{useTicks:true}).to({rotation:-15-(Math.floor(Math.random()*55))},15).call(function(){
						createjs.Tween.get(self.bgDisplay.cowTail,{useTicks:true}).to({rotation:0},10,createjs.Ease.cubicOut);
					});
				});
			break;
			case "tailDownUp":
				createjs.Tween.get(self.bgDisplay.cowTail,{useTicks:true}).to({rotation:-15-(Math.floor(Math.random()*55))},15).call(function(){
					createjs.Tween.get(self.bgDisplay.cowTail,{useTicks:true}).to({rotation:5+(Math.floor(Math.random()*15))},15).call(function(){
						createjs.Tween.get(self.bgDisplay.cowTail,{useTicks:true}).to({rotation:0},10,createjs.Ease.cubicOut);
					});
				});
			break;
			case "leftHead":
				createjs.Tween.get(self.cowHeadCont,{useTicks:true}).to({rotation:0},15);
			break;
			case "rightHead":
				createjs.Tween.get(self.cowHeadCont,{useTicks:true}).to({rotation:40},15);
			break;
		}
	};
	self.inactiveHighlight = function(){
		var i;
		for(i = 0; i < 6; i+=1){
			if(!self.gameSets[i].churn0.isRevealed){
				createjs.Tween.get(self.gameSets[i].churn0,{useTicks:true}).to({rotation:self.gameSets[i].churn0.rotation + 2},3).wait(0).call(function(i){
					createjs.Tween.get(self.gameSets[i].churn0,{useTicks:true}).to({rotation:self.gameSets[i].churn0.rotation - 4},3).wait(0).call(function(i){
						createjs.Tween.get(self.gameSets[i].churn0,{useTicks:true}).to({rotation:self.gameSets[i].churn0.rotation + 4},3).wait(0).call(function(i){
							createjs.Tween.get(self.gameSets[i].churn0,{useTicks:true}).to({rotation:self.gameSets[i].churn0.rotation - 2},3).wait(0).call(function(i){
								self.inactiveTimer = 100;
							},[i],this);
						},[i],this);
					},[i],this);
				},[i],this);
			}
			if(!self.gameSets[i].churn1.isRevealed){
				createjs.Tween.get(self.gameSets[i].churn1,{useTicks:true}).to({rotation:self.gameSets[i].churn1.rotation + 2},3).wait(0).call(function(i){
					createjs.Tween.get(self.gameSets[i].churn1,{useTicks:true}).to({rotation:self.gameSets[i].churn1.rotation - 4},3).wait(0).call(function(i){
						createjs.Tween.get(self.gameSets[i].churn1,{useTicks:true}).to({rotation:self.gameSets[i].churn1.rotation + 4},3).wait(0).call(function(i){
							createjs.Tween.get(self.gameSets[i].churn1,{useTicks:true}).to({rotation:self.gameSets[i].churn1.rotation - 2},3).wait(0).call(function(i){
								self.inactiveTimer = 100;
							},[i],this);
						},[i],this);
					},[i],this);
				},[i],this);
			}
			if(!self.gameSets[i].prizeText.isRevealed){
				createjs.Tween.get(self.gameSets[i].prizeText,{useTicks:true}).to({rotation:self.gameSets[i].prizeText.rotation + 2},3).wait(0).call(function(i){
					createjs.Tween.get(self.gameSets[i].prizeText,{useTicks:true}).to({rotation:self.gameSets[i].prizeText.rotation - 4},3).wait(0).call(function(i){
						createjs.Tween.get(self.gameSets[i].prizeText,{useTicks:true}).to({rotation:self.gameSets[i].prizeText.rotation + 4},3).wait(0).call(function(i){
							createjs.Tween.get(self.gameSets[i].prizeText,{useTicks:true}).to({rotation:self.gameSets[i].prizeText.rotation - 2},3).wait(0).call(function(i){
								self.inactiveTimer = 100;
							},[i],this);
						},[i],this);
					},[i],this);
				},[i],this);
			}
		}
	};
	self.addGameListeners = function(){
		self.gameStarted = true;
		self.animsEnabled = true;
		for(var i = 0; i < 6; i+=1){
			self.gameSets[i].churn0.on("click", function(ev,i){
				self.inactiveTimer = 0;
				self.soundObj.reveal.play();
				self.associateTurns(ev,i);
				self.statuses[self.turnGameAssoc.indexOf(i)][0] = true;
				self.gameSets[i].churn0.isRevealed = true;
				self.gameSets[i].symbol0.gotoAndStop(self.newSymbols[self.ticket.turns[self.turnGameAssoc.indexOf(i)].s0]);
				escapeIWLoader.commonFunctions.centraliseRegPoints(self.gameSets[i].symbol0);
				createjs.Tween.get(self.gameSets[i].churn0,{useTicks:true}).to({alpha:0},15);
				createjs.Tween.get(self.gameSets[i].symbol0,{useTicks:true}).wait(2).to({alpha:1},15).wait(5).call(function(){
					if(self.statuses[self.turnGameAssoc.indexOf(i)].indexOf(false) == -1){
						self.completedGames[self.turnGameAssoc.indexOf(i)] = true;
						createjs.Tween.get({},{useTicks:true}).wait(15).call(self.winChecker);
					}
				});
			},null,true,i);
			self.gameSets[i].churn1.on("click", function(ev,i){
				self.inactiveTimer = 0;
				self.soundObj.reveal.play();
				self.associateTurns(ev,i);
				self.statuses[self.turnGameAssoc.indexOf(i)][1] = true;
				self.gameSets[i].churn1.isRevealed = true;
				self.gameSets[i].symbol1.gotoAndStop(self.newSymbols[self.ticket.turns[self.turnGameAssoc.indexOf(i)].s1]);
				escapeIWLoader.commonFunctions.centraliseRegPoints(self.gameSets[i].symbol1);
				createjs.Tween.get(self.gameSets[i].churn1,{useTicks:true}).to({alpha:0},15);
				createjs.Tween.get(self.gameSets[i].symbol1,{useTicks:true}).wait(2).to({alpha:1},15).wait(5).call(function(){
					if(self.statuses[self.turnGameAssoc.indexOf(i)].indexOf(false) == -1){
						self.completedGames[self.turnGameAssoc.indexOf(i)] = true;
						createjs.Tween.get({},{useTicks:true}).wait(15).call(self.winChecker);
					}
				});
			},null,true,i);
			self.gameSets[i].prizeText.on("click",function(ev,i){
				self.inactiveTimer = 0;
				self.associateTurns(ev,i);
				self.statuses[self.turnGameAssoc.indexOf(i)][2] = true;
				self.gameSets[i].prizeText.isRevealed = true;
				self.gameSets[i].prizeText.removeAllEventListeners();
				self.gameSets[i].prizeText.scaleX = 1;
				self.gameSets[i].prizeText.scaleY = 1;

				//play the brander animation. on frame 2 (its lowest and hottest point) change the prizeText to the prizeAmount.
				var ticks = 0;
				self.gameSets[i].prizeBrand.gotoAndPlay("reveal");
				self.gameSets[i].prizeBrand.on("tick",function(ev,i){
					ticks++;
					if(ticks == 23 && createjs.Sound.getCapabilities().mp3){ //if the mp3 is being used. the mp3 has about 0.05 seconds of silence at the start of the file.
						self.soundObj.prizeReveal.play();
					}
					if(ticks == 27){
						if(!createjs.Sound.getCapabilities().mp3){ //if the mp3 isnt being used.
							self.soundObj.prizeReveal.play();
						}

						self.gameSets[i].prizeText.gotoAndStop("prize"+parseInt(self.ticket.turns[self.turnGameAssoc.indexOf(i)].p,10));
						self.gameSets[i].NprizeText.gotoAndStop("p"+parseInt(self.ticket.turns[self.turnGameAssoc.indexOf(i)].p,10));
						self.gameSets[i].NprizeText.alpha = 1;
						escapeIWLoader.commonFunctions.centraliseRegPoints(self.gameSets[i].prizeText);
						escapeIWLoader.commonFunctions.centraliseRegPoints(self.gameSets[i].NprizeText);
					}
				},this,false,i);
				self.gameSets[i].prizeBrand.on("animationend",function(ev,i){
					self.inactiveTimer = 0;
					self.gameSets[i].prizeBrand.stop();
					self.gameSets[i].prizeBrand.removeAllEventListeners();
					createjs.Tween.get(self.gameSets[i].prizeText,{useTicks:true}).wait(10).to({alpha:0},20);
					if(self.statuses[self.turnGameAssoc.indexOf(i)].indexOf(false) == -1){
						self.completedGames[self.turnGameAssoc.indexOf(i)] = true;
						createjs.Tween.get({},{useTicks:true}).wait(40).call(self.winChecker);
					}
				},this,true,i);
			},null,true,i);
		//make the churns respond to the mouse
		//upscale
		self.gameSets[i].churn0.on("mouseover",function(ev,i){self.gameSets[i].churn0.scaleX = 1.1; self.gameSets[i].churn0.scaleY = 1.1; self.inactiveTimer = 0;},null,false,i);
		self.gameSets[i].churn1.on("mouseover",function(ev,i){self.gameSets[i].churn1.scaleX = 1.1; self.gameSets[i].churn1.scaleY = 1.1; self.inactiveTimer = 0;},null,false,i);
		self.gameSets[i].prizeText.on("mouseover",function(ev,i){self.gameSets[i].prizeText.scaleX = 1.1; self.gameSets[i].prizeText.scaleY = 1.1; self.inactiveTimer = 0;},null,false,i);
		//downscale
		self.gameSets[i].churn0.on("mouseout",function(ev,i){self.gameSets[i].churn0.scaleX = 1.0; self.gameSets[i].churn0.scaleY = 1.0; self.inactiveTimer = 0;},null,false,i);
		self.gameSets[i].churn1.on("mouseout",function(ev,i){self.gameSets[i].churn1.scaleX = 1.0; self.gameSets[i].churn1.scaleY = 1.0; self.inactiveTimer = 0;},null,false,i);
		self.gameSets[i].prizeText.on("mouseout",function(ev,i){self.gameSets[i].prizeText.scaleX = 1.0; self.gameSets[i].prizeText.scaleY = 1.0; self.inactiveTimer = 0;},null,false,i);
		}
	};
	self.associateTurns = function(evt,arr){//associate turn with game. turnGameAssoc array index increments turns from tickets, values are games.
		if((self.turnGameAssoc[self.turnsTaken]) == -1 && (self.triggeredGames[arr] == false)) {
			self.triggeredGames[arr] = true;
			self.turnGameAssoc[self.turnsTaken] = arr;
			self.turnsTaken++;
		}
	};
	self.winChecker = function(){
		for(var i = 0;i < 6; i+=1){
			if(self.triggeredGames[i] && self.completedGames[self.turnGameAssoc.indexOf(i)]){
				if(self.gameSets[i].symbol0.currentAnimation == self.gameSets[i].symbol1.currentAnimation){//if they have the same anim
					if(self.ticket.turns[self.turnGameAssoc.indexOf(i)].s0 == self.ticket.turns[self.turnGameAssoc.indexOf(i)].s1){ //if they match on the ticket
						if(self.ticket.turns[self.turnGameAssoc.indexOf(i)].w == 1){ // and the win flag says the game is a winner
							self.winStatus = true;
							//self.gameSets[i].prizeText.gotoAndStop("w"+parseInt(self.ticket.turns[self.turnGameAssoc.indexOf(i)].p,10)); //replaceed with winPulser.
							escapeIWLoader.commonFunctions.centraliseRegPoints(self.gameSets[self.turnGameAssoc.indexOf(i)].prizeText);
							if(!self.winRevealGame[self.turnGameAssoc.indexOf(i)]){

								self.winRevealGame[self.turnGameAssoc.indexOf(i)] = true;
								self.banked += parseInt(self.ticket.turns[self.turnGameAssoc.indexOf(i)].p,10);
								createjs.Tween.get({},{useTicks:true}).wait(15).call(function(i){
									self.soundObj.win.play();
									self.winPulser(i);
								},[i],this);

							}
						}
					}
				}
			}
		}
		if(self.completedGames.indexOf(false) == -1){
			createjs.Tween.get({},{useTicks:true}).wait(40).call(function(){
				if(self.banked == self.ticket.prizeAmount){
					if(!self.endGameTriggered){
						self.endGameTriggered = true;
						createjs.Tween.get({},{useTicks:true}).wait(50).call(self.endGame);
					}
				} else {
					escapeIWLoader.errorInvoker.invokeError("Banked != Ticket");
				}
			});
		}
	};
	self.winPulser = function(i){
		if(i < 3){
			createjs.Tween.get(self.gameSets[i].panel,{useTicks:true}).to({scaleX:0.95, scaleY:0.95},10).wait(5).call(function(i){
				createjs.Tween.get(self.gameSets[i].panel,{useTicks:true}).to({scaleX:1, scaleY:1},10).wait(5).call(self.winPulser,[i],this);
			},[i],this);
		} else {
			createjs.Tween.get(self.gameSets[i].panel,{useTicks:true}).to({scaleX:-0.95, scaleY:0.95},10).wait(5).call(function(i){
				createjs.Tween.get(self.gameSets[i].panel,{useTicks:true}).to({scaleX:-1, scaleY:1},10).wait(5).call(self.winPulser,[i],this);
			},[i],this);
		}

		createjs.Tween.get(self.gameSets[i].prizeText,{useTicks:true}).to({alpha:1},10).wait(5).call(function(){
			createjs.Tween.get(self.gameSets[i].prizeText,{useTicks:true}).to({alpha:0},10);
		});
	};
	self.endGame = function(){
		g.removeGearEventListener();
		self.endGameTriggered = true;
		self.allowMoo = false;
		createjs.Tween.get(self.bgDisplay.logo,{useTicks:true}).to({alpha:0},15).wait(5).call(function(){
			self.bgDisplay.logo.scaleX = 1;
			self.bgDisplay.logo.scaleY = 1;
			if(self.winStatus){
				//winner
				self.bgDisplay.logo.gotoAndStop("end_win");
				self.soundObj.endGameWin.play();
				escapeIWLoader.commonFunctions.centraliseRegPoints(self.bgDisplay.logo);

				self.bgDisplay.winPrize.gotoAndStop("end"+parseInt(self.ticket.prizeAmount,10));
				escapeIWLoader.commonFunctions.centraliseRegPoints(self.bgDisplay.winPrize);

				createjs.Tween.get(self.bgDisplay.logo,{useTicks:true}).to({alpha:1},15).wait(5).call(function(){});
				createjs.Tween.get(self.bgDisplay.winPrize,{useTicks:true}).to({alpha:1},15).wait(5);//.call(/*self.deployFinishButton*/);
			} else {
				//loser
				self.bgDisplay.logo.gotoAndStop("end_lose");
				self.soundObj.endGameLose.play();
				escapeIWLoader.commonFunctions.centraliseRegPoints(self.bgDisplay.logo);
				createjs.Tween.get(self.bgDisplay.logo,{useTicks:true}).to({alpha:1},15).wait(5);//.call(/*self.deployFinishButton*/);

			}
			self.deployFinishButton();
		});
	};
	self.deployFinishButton = function(){
		self.bgDisplay.finishText.gotoAndStop("finish");
		self.bgDisplay.finishBody.y = 60;
		self.bgDisplay.finishText.y = 60;
		//self.bgDisplay.finishBody.y = -60;
		//self.bgDisplay.finishText.y = -60;
		//self.bgDisplay.finishBody.alpha = 1;
		//self.bgDisplay.finishText.alpha = 1;
		escapeIWLoader.commonFunctions.centraliseRegPoints(self.bgDisplay.finishText);

		//createjs.Tween.get(self.bgDisplay.finishBody,{useTicks:true}).to({y:60},30);
		//createjs.Tween.get(self.bgDisplay.finishText,{useTicks:true}).to({y:60},30);
		createjs.Tween.get(self.bgDisplay.finishBody,{useTicks:true}).to({alpha:1},15);
		createjs.Tween.get(self.bgDisplay.finishText,{useTicks:true}).to({alpha:1},15);
		createjs.Tween.get({},{useTicks:true}).wait(35).call(function(){
			self.bgDisplay.finishBody.on("mouseover",self.onFinishButtonMouseOver);
			self.bgDisplay.finishBody.on("mouseout",self.onFinishButtonMouseOut);
			self.bgDisplay.finishBody.on("click",self.onFinishButtonClick);

			self.bgDisplay.finishText.on("mouseover",self.onFinishButtonMouseOver);
			self.bgDisplay.finishText.on("mouseout",self.onFinishButtonMouseOut);
			self.bgDisplay.finishText.on("click",self.onFinishButtonClick);

			self.allowMoo = true;
			g.addGearEventListener();
		});
	};
	self.onStartButtonMouseOver = function(){self.bgDisplay.finishText.gotoAndStop("word_play_over");};
	self.onStartButtonMouseOut = function(){self.bgDisplay.finishText.gotoAndStop("word_play");};
	self.onStartButtonClick = function(){self.landingScreenAction(); self.soundObj.click.play();};
	self.onFinishButtonMouseOver = function(){self.bgDisplay.finishText.gotoAndStop("finish_over");};
	self.onFinishButtonMouseOut = function(){self.bgDisplay.finishText.gotoAndStop("finish");};
	self.onFinishButtonClick = function(){window.location = "http://www.sideplay.com/portfolio/";};
	var returnable = {};
	returnable.preload = function(){self.preload();};
	returnable.start = function(){self.start();};
	returnable.muteUnmuteAll = function(){self.setMute();};
	returnable.getMuteStatus = function(){return self.isMuted;};
	returnable.muteUnmuteBG = function(){self.setBGMute();};
	returnable.getBGStatus = function(){return self.isBGMuted;};
	returnable.test = function(){return self;};
	return returnable;
})();
