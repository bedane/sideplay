/*
*/

var g = g || {};

g = (function(){
	"use strict";
	var self = {},
	returnable = {};
	self.shown = false;
	//buttonOne is the mute button, buttonTwo is the music button.
	self.setup = function(container,gameContainer,styling,text,mutebutton,musicbutton){
		self.container 	= container;
		self.gameContainer = gameContainer;
		self.styling 	= styling;
		self.text 		= text;
		self.mutebutton = mutebutton;
		self.musicbutton= musicbutton;

		if(container == undefined || styling == undefined){
			return false;
		} else {
			self.gearContainer 	= new createjs.Container();

			self.gearContainer.x = 100; self.gearContainer.y = 50;
			self.gearContainer.alpha = 0;

			if(styling.background.stroke == "" || styling.background.stroke == undefined){
				self.gcBackground = styling.background.bgImage;
			} else {
				self.gcBackground = new createjs.Shape();
				self.gcBackground.graphics.s(styling.background.stroke).f(styling.background.fill).rr(0,0,760,540,20).ef();
			}
			if(styling.trim != undefined){
				if(styling.trim.stroke == "" || styling.trim.stroke == undefined){
					self.gcTrim = styling.trim.trimImage;
				} else {
					self.gcTrim	= new createjs.Shape();
					self.gcTrim.graphics.s(styling.trim.stroke).f(styling.trim.fill).rr(10,10,740,520,15).ef();
				}
			}


			//styling.gear, styling.darken
			if(styling.darken != undefined){
				container.addChild(styling.darken);
			}

			container.addChild(styling.gear);

			//hitArea on the gear
			self.gearHitArea = new createjs.Shape();
			self.gearHitArea.graphics.f("#000").dr(0,0,styling.gear.getBounds().width,styling.gear.getBounds().height);
			styling.gear.hitArea = self.gearHitArea;

			self.gearContainer.addChild(self.gcBackground,self.gcTrim);
			container.addChild(self.gearContainer);

			self.addGearEventListener();
		}
		if(text != undefined){
			if(typeof(text) == "object" && !Array.isArray(text)){
				if(text.textValue == "" || text.textValue == undefined){
					self.gcTextImage = text.textImage;
					self.gearContainer.addChild(self.gcTextImage);
				} else {
					self.gcText			= new createjs.Text();
					self.gcText.text 	= text.textValue;
					self.gcText.font 	= text.textFont;
					self.gcText.color 	= text.textColour;
					self.gcText.x 		= 50;
					self.gcText.y 		= 50;
					self.gcText.maxWidth= 660;
					self.gearContainer.addChild(self.gcText);
				}
			}
		}
		if(mutebutton != undefined && musicbutton != undefined){ //if they are both present create both buttons.
			if(self.musicbutton.create != undefined){
				self.buttonTwo = self.musicbutton.create;
				self.gearContainer.addChild(self.buttonTwo);
			}
			if(self.mutebutton.create != undefined){
				self.buttonOne = self.mutebutton.create;
				self.gearContainer.addChild(self.buttonOne);
			}
		} else { //there is only one or there isnt one present
			if(mutebutton != undefined){
				if(typeof(mutebutton) == "object" && !Array.isArray(mutebutton)){
					if(self.mutebutton.create != undefined){
						self.buttonOne = self.mutebutton.create;
						self.gearContainer.addChild(self.buttonOne);
					}
				}
			}
			if(musicbutton != undefined){
				if(typeof(musicbutton) == "object" && !Array.isArray(musicbutton)){
					if(self.musicbutton.create != undefined){
						self.buttonTwo = self.musicbutton.create;
						self.gearContainer.addChild(self.buttonTwo);
					}
				}
			}
		}

		if(self.styling.cross != undefined){
			self.gearContainer.addChild(self.styling.cross);
			//hitArea on the cross
			self.crossHitArea = new createjs.Shape();
			self.crossHitArea.graphics.f("#000").dr(-10,-10,self.styling.cross.getBounds().width+20,self.styling.cross.getBounds().height+20);
			self.styling.cross.hitArea = self.crossHitArea;
		}
	};
	self.show = function(){
		self.shown = true;

		//transition speed.
		if(typeof(self.styling.animSpeed) != "number" || self.styling.animSpeed == undefined){
			self.styling.animSpeed = 15;
		}
		//rotate gear
		createjs.Tween.get(self.styling.gear,{useTicks:true}).to({rotation:90},self.styling.animSpeed);

		if(self.mutebutton != undefined){
			self.updateMuteButton();
		}
		if(self.musicbutton != undefined){
			self.updateMusicButton();
		}

		//remove normal game listeners
		self.gameContainer.mouseEnabled = false;

		//transition.
		switch(self.styling.animStyle){
			case "slide":
				if(typeof(self.styling.containerX) != "number" || typeof(self.styling.containerY) != "number"){
					self.styling.containerX = -800;
					self.styling.containerY = 50;
				}
				if(self.styling.animEffectIn == "" || self.styling.animEffectIn == undefined){
					self.styling.animEffectIn = createjs.Ease.linear;
				}
				self.gearContainer.x = self.styling.containerX;
				self.gearContainer.y = self.styling.containerY;

				if(self.styling.darken != undefined){
					createjs.Tween.get(self.styling.darken,{useTicks:true}).to({alpha:1},5).call(function(){
						self.gearContainer.alpha = 1;
						createjs.Tween.get(self.gearContainer,{useTicks:true}).to({x:100,y:50},self.styling.animSpeed,self.styling.animEffectIn).call(function(){
							self.enableButtonListeners();
						});
					});
				} else {
					self.gearContainer.alpha = 1;
					createjs.Tween.get(self.gearContainer,{useTicks:true}).to({x:100,y:50},self.styling.animSpeed,self.styling.animEffectIn).call(function(){
						self.enableButtonListeners();
					});
				}

				break;
			case "fade": //cascade fade into default, as it is the default anim.
			default:
				if(self.styling.darken != undefined){
					createjs.Tween.get(self.styling.darken,{useTicks:true}).to({alpha:1},self.styling.animSpeed/2).call(function(){
						createjs.Tween.get(self.gearContainer, {useTicks:true}).to({alpha:1},self.styling.animSpeed/2).call(function(){
							self.enableButtonListeners();
						});
					});
				} else {
					createjs.Tween.get(self.gearContainer, {useTicks:true}).to({alpha:1},self.styling.animSpeed).call(function(){
						self.enableButtonListeners();
					});
				}

				break;
		}
	};
	self.hide = function(){
		self.shown = false;
		self.disableButtonListeners();
		//transition
		switch(self.styling.animStyle){
			case "slide":
				if(typeof(self.styling.containerX) != "number" || typeof(self.styling.containerY) != "number"){
					self.styling.containerX = -800;
					self.styling.containerY = 50;
				}
				if(self.styling.animEffectOut == "" || self.styling.animEffectOut == undefined){
					self.styling.animEffectOut = createjs.Ease.linear;
				}

				createjs.Tween.get(self.gearContainer,{useTicks:true}).to({x:self.styling.containerX,y:self.styling.containerY},self.styling.animSpeed,self.styling.animEffectOut).call(function(){
					self.gearContainer.alpha = 0;
					if(self.styling.darken != undefined){
						createjs.Tween.get(self.styling.darken,{useTicks:true}).to({alpha:0},5).call(function(){
							//enable normal game listeners
							self.gameContainer.mouseEnabled = true;
						});
					} else {
						self.gameContainer.mouseEnabled = true;
					}
				});
				break;
			case "fade": //cascade fade into default, as it is the default anim.
			default:
				if(self.styling.darken != undefined){
					createjs.Tween.get(self.gearContainer,{useTicks:true}).to({alpha:0},self.styling.animSpeed/2).call(function(){
						createjs.Tween.get(self.styling.darken, {useTicks:true}).to({alpha:0},self.styling.animSpeed/2).call(function(){
							//enable normal game listeners
							self.gameContainer.mouseEnabled = true;
						});
					});
				} else {
					createjs.Tween.get(self.gearContainer,{useTicks:true}).to({alpha:0},self.styling.animSpeed).call(function(){
						self.gameContainer.mouseEnabled = true;
					});
				}
			break;
		}

		//rotate gear back
		createjs.Tween.get(self.styling.gear,{useTicks:true}).to({rotation:0},self.styling.animSpeed);
	};
	self.updateMuteButton = function(){
		if(self.mutebutton.statusCheck()){
			self.buttonOne.gotoAndStop(self.mutebutton.buttonPress);
		} else {
			self.buttonOne.gotoAndStop(self.mutebutton.button);
		}
	};
	self.updateMusicButton = function(){
		if(self.musicbutton.statusCheck()){
			self.buttonTwo.gotoAndStop(self.musicbutton.buttonPress);
		} else {
			self.buttonTwo.gotoAndStop(self.musicbutton.button);
		}};
	self.enableButtonListeners = function(){
		if(self.mutebutton != undefined){
			self.buttonOne.on("click",function(){
				self.mutebutton.action();
				self.updateMuteButton();
			});
		}
		if(self.musicbutton != undefined){
			self.buttonTwo.on("click",function(){
				self.musicbutton.action();
				self.updateMusicButton();
			});
		}
	};
	self.disableButtonListeners = function(){
		if(self.mutebutton != undefined){
			self.buttonOne.removeAllEventListeners();
		}
		if(self.musicbutton != undefined){
			self.buttonTwo.removeAllEventListeners();
		}
	};
	self.addGearEventListener = function(){
		self.styling.gear.on("click",function(){
			if(self.shown){
				self.hide();
				self.shown = false;
			} else {
				self.show();
				self.shown = true;
			}
		});
		self.styling.cross.on("click",function(){
			if(self.shown){
				self.hide();
				self.shown = false;
			}
		});
	};
	self.removeGearEventListener = function(){
		self.hide();
		self.styling.gear.removeAllEventListeners();
	};
	returnable.setup = function(container,gameContainer,styling,text,mutebutton,musicbutton){self.setup(container,gameContainer,styling,text,mutebutton,musicbutton);};
	returnable.removeGearEventListener = function(){self.removeGearEventListener();};
	returnable.addGearEventListener = function(){self.addGearEventListener();};
	return returnable;
})();
