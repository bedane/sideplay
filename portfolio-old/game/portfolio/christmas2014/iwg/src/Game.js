/**
 * @class   Game
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader         = window.Loader,
        Animate        = Loader.Animate,
        Helper         = Loader.Helper,
        ErrorInvoker   = Loader.ErrorInvoker,
        Sounds         = Loader.Sounds,
        Events         = Loader.Events,
        GameAsset      = Loader.GameAsset,
        Ticket         = Loader.Ticket,
        Canvas         = Loader.Canvas,
        RevealQueue    = Loader.RevealQueue,
        Splash         = Loader.Splash,
        MainGameLayout = Loader.MainGameLayout,
        SettingsCog    = Loader.SettingsCog,
        EndGame        = Loader.Endgame;

    /**
     * @return Game
     */
    var Game = function() {

        if (Game.instance instanceof Game) {
            return Game.instance;
        }
        Game.instance = this;

        // name all containers needed in game here
        var _background         = new createjs.Container(),
            _treeContainer      = new createjs.Container(),
            _mainContainer      = new createjs.Container(),
            _splashContainer    = new createjs.Container(),
            _gameContainer      = new createjs.Container(),
            _santaContainer     = new createjs.Container(),
            _snow               = Animate.snow(),
            _assetArray         = [],
            _turn               = 0,
            _iconRef             = {
                0: "santa",
                1: "holly",
                2: "mistletoe",
                3: "candle",
                4: "cane",
                5: "crackers",
                6: "gingerbread",
                7: "pudding",
                8: "stocking",
                9: "sleigh"
            };

        this.getBackgroundContainer = function(){
            return _background;
        };
        this.getSantaContainer = function(){
            return _santaContainer;
        }
        this.getTreeContainer = function(){
            return _treeContainer;
        };
        this.getMainContainer = function() {
            return _mainContainer;
        };
        this.getAssetArray = function() {
            return _assetArray;
        }
        this.getTurn = function() {
            return _turn;
        }
        this.getIconRef = function() {
            return _iconRef;
        }


        this.setTurn = function(prv) {
            _turn = prv;
        }

        _santaContainer.name = "santaContainer";

        var gameContainer       = new createjs.Container();
        gameContainer.name      = "gameContainer";

        gameContainer.addChild(_background, _snow, _santaContainer, _treeContainer, _mainContainer);
        Canvas.instance.getGameStage().addChild(gameContainer);

        Canvas.instance.getGameStage().setChildIndex(SettingsCog.instance.getContainer(), 1);
        init();

    };

    Loader.instance.addToNamespace("window.Loader.Game", Game);
    Game.n = "Game";
    Game.v = "0.0.1";

    /**
     * @return Undefined
     */
    function init() {

        makeTicket();
        setupSounds();
        playSanta();

        // setup listeners
        Events.instance.addListener("iconIntro", iconIntro);
        Events.instance.addListener("turn", turn);


        // background
        var background = new createjs.Shape();
        background.graphics.beginLinearGradientFill(
            ["#83c4ec","#fdfbfc", "#ea1d24"], [0, 0.5, 1], 0, 200, 0, 640).drawRect(-100, 0, 1260, 1600);
        Loader.Game.instance.getBackgroundContainer().addChild( background );

        var treeContainer = new createjs.Container(),
            tree = Helper.makeBitmapImage("tree",{x: 5, y: 0}, 1, false),
            starContainer = new createjs.Container(),
            star = Helper.makeBitmapImage("star", { x: 0, y: 0}, 1, true),
            starSparkles = new createjs.Container(),
            treeSparkles = new createjs.Container();

        // co-ords
        treeContainer.y     = 150;
        treeContainer.x     = -10;
        starContainer.y     = 120;
        starContainer.x     = Canvas.instance.getGameWidth() / 2;

        var starShimmers = [
            [3, -85],
            [-40, -10],
            [-100, -25]
        ]
        var treeShimmers = [
            [380, 270],
            [530, 290],
            [600, 250],

            [270, 355],
            [700, 380],

            [280, 450],
            [490, 430],
            [780, 470],
            [500, 570],

            [340, 630],
            [140, 645],
            [40, 720],
            [380, 760],
            [680, 720],
            [780, 790],
            [70, 830]

        ]

        for(var i = 0; i < starShimmers.length; i++) {

            var sparkleX    = starShimmers[i][0],
                sparkleY    = starShimmers[i][1],
                sparkle     = Animate.sparkles(sparkleX, sparkleY, "shiner");

            starSparkles.addChild(sparkle);

        }

        for(var i = 0; i < treeShimmers.length; i++) {

            var sparkleX    = treeShimmers[i][0],
                sparkleY    = treeShimmers[i][1],
                sparkle     = Animate.sparkles(sparkleX, sparkleY, "tree_light");

            treeSparkles.addChild(sparkle);

        }

        starContainer.addChild( star, starSparkles );
        treeContainer.addChild( tree, treeSparkles );

        Loader.Game.instance.getTreeContainer().addChild(treeContainer, starContainer)

        // splash
        var splash = new Splash();
        Loader.Game.instance.getMainContainer().addChild(splash.getContainer());


        // call stuff in lib
        var mainGameLayout = new MainGameLayout();

        Canvas.instance.getGameStage().addChild(mainGameLayout.getGameContainer());

        Loader.instance.deploy(false);

    }

    /*
     *  Play santa on intro
     *
     */
    function playSanta(){

        var santa   = Helper.makeBitmapImage("santaFly", {x: -250, y: 400}, 1, true),
            canvas  = Canvas.instance.getGameStage();

        TweenMax.to(santa, 4, {x: Canvas.instance.getGameWidth(), y: -100, delay: 2, ease: "linear", onComplete: function(){
             canvas.removeChild(canvas.getChildByName("santaContainer"));
        }});

        Game.instance.getSantaContainer().addChild(santa);

    }

    function makeTicket() {

        return new Ticket(Loader.instance.getInclude("ticket"), function(obj) {
            return obj;
        }, function(ticket) {
            return true;
        }, false );

    }

    /**
     * @description Setup the main game sounds into the Sound Channel
     * @return Undefined
     */
    function setupSounds() {

        new Sounds.Channel([Helper.makeSound("boxReminderSound"), Helper.makeSound("boxRevealSound"),
                            Helper.makeSound("boxStartSound"),    Helper.makeSound("boxWinSound"),
                            Helper.makeSound("endWinSound"),      Helper.makeSound("endLoseSound")], "effect");
    }

    function iconIntro() {

        // possibly put this in animate
        var assets      = Game.instance.getAssetArray(),
            staggerTL   = new TimelineMax(),
            assetCont   = [];

        for(var i = 0; i < assets.length; i++){
            assetCont.push(assets[i].getContainer());
        }

        var randArray   = Helper.randomise(assetCont);

        // stagger in
        staggerTL.staggerTo(randArray, 0.4,{ scaleX: 1, scaleY: 1, alpha: 1, ease: "easeOutBounce"}, 0.05)


    }

    function turn(asset) {

        // get click turn
        var turn    = Game.instance.getTurn();

        // get turn data
        var go      = Ticket.instance.getTicket().g1.go[turn],
            // get p value
            t       = Number(go.t),
            w       = Number(go.w),
            p       = String(Ticket.instance.getParams().pList[Number(go.p)]);

        // get correct string
        var string  = Helper.checkObject(t, Game.instance.getIconRef()),
            token   = Helper.makeBitmapImage(string, {x: 0, y: 0}, 1, true),
            prize   = Helper.makeBitmapImage("p"+p, {x: 0, y: 90}, 1, true);
        token.y     = 30;

        token.scaleX = token.scaleY = prize.scaleX = prize.scaleY =  0;

        asset.getContainer().addChild(token, prize);

        TweenMax.to(token, 0.7, {scaleX: 1, scaleY: 1, ease: "easeInOutElastic"});
        TweenMax.to(prize, 0.7, {scaleX: 1, scaleY: 1, delay: .1, ease: "easeInOutElastic"});

        // play reveal sound
        Sounds.instance.getChannels()[0].getSoundByName("boxRevealSound").play();

        if(w === 1){

            var win = Helper.makeBitmapImage("w"+p, {x: 0, y:90}, 1, true);
            win.scaleX = win.scaleY = 0;

            TweenMax.to(prize, 0.7, {scaleX: 0, scaleY: 0, delay: 1, ease: "easeInOutElastic"});
            TweenMax.to(win, 0.7, {scaleX: 1, scaleY: 1, delay: 1.2, ease: "easeInOutElastic", onStart: function(){
                // play win sound
                Animate.explode(30, asset);
                Sounds.instance.getChannels()[0].getSoundByName("boxWinSound").play();
            },onComplete: function(){
                win.scaleX = win.scaleY = 1;
            }});

            asset.getContainer().addChild(win);

        }

        // increment click
        turn++;
        Game.instance.setTurn(turn);

        if(Game.instance.getTurn() === 8){
            var endGame = new Loader.EndGame();
        }


    }

    Loader.instance.deploy(false);

})(window);
