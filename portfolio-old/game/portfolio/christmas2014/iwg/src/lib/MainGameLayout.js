/**
 * @class   MainGameLayout
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    //"use strict";

    var Loader      = window.Loader,
        Helper      = Loader.Helper,
        Ticket      = Loader.Ticket,
        Canvas      = Loader.Canvas,
        Events      = Loader.Events,
        Swipe       = Loader.Swipe,
        Legend      = Loader.Legend,
        Game        = Loader.Game,
        Animate     = Loader.Animate,
        GameAsset   = Loader.GameAsset,
        Splash      = Loader.Splash;


    /**
     * @return MainGameLayout
     */
    var MainGameLayout = function() {

        if (MainGameLayout.instance instanceof MainGameLayout) {
            return MainGameLayout.instance;
        }
        MainGameLayout.instance = this;

        var _gameContainer = new createjs.Container(),
            _firstGameWindow = Canvas.instance.getScreenAt(1);

        // getter
        this.getGameContainer = function(){
            return _gameContainer;
        }
        this.getFirstGameWindow = function(){
            return _firstGameWindow;
        }

        _gameContainer.addChild(_firstGameWindow);
        init.bind(this)();

    };

    Loader.instance.addToNamespace("window.Loader.MainGameLayout", MainGameLayout);
    MainGameLayout.n = "MainGameLayout";
    MainGameLayout.v = "0.0.1";

    /**
     * @return Undefined
     */
    function init() {

        // add event listeners
        Events.instance.addListener("mainGameIntro", mainGameIntro);

        // rest of game layout
        setupFirstGameWindow(this);

        this.getGameContainer().x = Canvas.instance.getGameWidth();

    }

    function setupFirstGameWindow(self){

        var container   = new createjs.Container();

        container.setBounds(0,0, Canvas.instance.getGameWidth(), Canvas.instance.getGameHeight());

        var instantCash     = Helper.makeBitmapImage("brand", { x: Canvas.instance.getGameWidth() / 2, y: 0 }, 1, true),
            ballRed1        = Helper.makeBitmapImage("bauble_red", { x: 130, y: 140 }, 1, true),
            ballRed2        = Helper.makeBitmapImage("bauble_red", { x: 60, y: 390 }, 1, true),
            ballRed3        = Helper.makeBitmapImage("bauble_red", { x: 950, y: 340 }, 1, true),
            ballYellow1     = Helper.makeBitmapImage("bauble_yellow", { x: 40, y: 290 }, 1, true),
            ballYellow2     = Helper.makeBitmapImage("bauble_yellow", { x: 905, y: 190 }, 1, true),
            ballYellow3     = Helper.makeBitmapImage("bauble_yellow", { x: 890, y: 420 }, 1, true),
            instructions    = Helper.makeBitmapImage("instruction", { x: Canvas.instance.getGameWidth() / 2, y: 460 }, 1, true);
        instantCash.name    = "instantCash";


        var iconX           = 250,
            iconY           = 140;

        // icons
        for(var i = 0; i < 8; i++){

            // icon container
            var assetContainer  = new createjs.Container(),
                assetAnimations  = {
                    reveal: {
                        animation:  Animate.reveal,
                        delay:      0,
                        channel:    null
                    },
                    intro: {
                        animmation: Animate.intro,
                        delay:      0,
                        channel:    null
                    }
                },
                asset           = new GameAsset(assetContainer, assetAnimations, true, false),
                iconCont        = new createjs.Container(),
                topBox          = "present_red_top",
                botBox          = "present_red_bottom",
                explosion       = null;

            assetContainer.name     = "boxContainer";
            assetContainer.asset    = asset;

            iconCont.name           = "iconContainer";
            // add assets to array
            Loader.Game.instance.getAssetArray().push(asset);

            // sort co-ordinates
            var gridCol = i % 4,
                gridRow = 0;


            if( i > 3 ){
                gridRow = 1;
            }
            assetContainer.x      = iconX + (170 * gridCol);
            assetContainer.y      = iconY + (150 * gridRow);
            // hide for creation
            assetContainer.alpha  = 0;
            assetContainer.scaleX = assetContainer.scaleY = 0.5;

            // icon
            if( i === 0 || i === 5 || i === 2 || i === 7 ){

                topBox = "present_green_top";
                botBox = "present_green_bottom";

            }

            var top     = Helper.makeBitmapImage(topBox, {x: 0, y: 0}, 1, true),
                bot     = Helper.makeBitmapImage(botBox, {x: 0, y: 0}, 1, true)

            top.y       = 0;
            top.x       = 3;
            if( i === 0 || i === 5 || i === 2 || i === 7 ){
                top.x       = -3;
            }
            bot.y       = 52;

            // name box halves
            top.name    = "top";
            bot.name    = "bot";
            // add shimmer
            var sparkle = Animate.sparkles(5, -10, "shiner");
            sparkle.name = "sparkle"

            // asset cont click
            assetContainer.on("click", function(ev){

                var container   = ev.currentTarget,
                    asset       = container.asset,
                    top         = container.getChildByName('iconContainer').getChildByName("top"),
                    bot         = container.getChildByName('iconContainer').getChildByName("bot"),
                    sparkle     = container.getChildByName('sparkle'),
                    topYTarg    = top.y - 100,
                    botYTarg    = bot.y + 100;

                Helper.moveToTop(container);

                sparkle.alpha  = 0;

                TweenMax.to(bot, 0.2, {directionalRotation:"4_cw", yoyo: 1, repeat: -1});
                TweenMax.to(top, 0.2, {directionalRotation:"4_cw", yoyo: 1, repeat: -1, delay: 0.05});
                TweenMax.to(bot, 0.3, {y: botYTarg, delay: 1.2, ease:"easeInOutCirc"});
                TweenMax.to(top, 0.3, {y: topYTarg, delay: 1.2, ease:"easeInOutCirc", onStart: function(){

                    Events.instance.dispatchEvent('turn', [asset]);
                    explosion = Animate.explode(15 , asset );

                }});
                TweenMax.to(bot, 0.4, {alpha: 0, delay: 1.2});
                TweenMax.to(top, 0.4, {alpha: 0, delay: 1.2});

                // play Opening sound
                Sounds.instance.getChannels()[0].getSoundByName("boxReminderSound").play();


            }, null, true)


            iconCont.addChild(bot, top, explosion);

            // add to asset container
            assetContainer.addChild(iconCont, sparkle);

            // add to main container
            container.addChild(assetContainer);

        }

        container.addChild(instantCash, ballRed1, ballRed2, ballRed3, ballYellow1, ballYellow2, ballYellow3, instructions );
        container.y = 740;

        Loader.Game.instance.getMainContainer().addChild(container);
        self.getFirstGameWindow().x = 0;
        self.getFirstGameWindow().y = Canvas.instance.getGameHeight();
        self.getFirstGameWindow().name = "firstGameWindow";
        self.getGameContainer().addChild(self.getFirstGameWindow());

    }


    /*
     *  mainGameIntro
     *  Called from splash screen button
     *  sets up swipe and tweens game in
     *
     */
    function mainGameIntro(){

        var t = Loader.Splash.instance.getContainer();

        TweenMax.to(Loader.Game.instance.getBackgroundContainer(), 0.6, { y: -460});
        TweenMax.to(t, 0.4, {alpha: 0});
        // tree
        var tree = Loader.Game.instance.getTreeContainer();
        TweenMax.to(tree, 0.8, { y : -Canvas.instance.getGameHeight() + 240});

        // main container
        TweenMax.to(Loader.Game.instance.getMainContainer(), 0.8, { y : -Canvas.instance.getGameHeight(), onComplete: function(){
            Loader.Game.instance.getMainContainer().removeChild(t);
            Events.instance.dispatchEvent('iconIntro');
        }});

    }


}(window));
