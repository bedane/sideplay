/**
 * @class   Includes
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    /**
     * @return Includes
     */
    var Includes = function() {

        // singleton pattern
        if (Includes.instance instanceof Includes) {
            return Includes.instance;
        }
        Includes.instance = this;

        var _includes = [

           // third party
            {src: "src/imports/js/TweenMax.min.js",   id: "TweenMax"},
            {src: "src/imports/js/xml2json.min.js",   id: "X2JS"},
            {src: "src/imports/js/bowser.min.js",     id: "bowser"},
            {src: "src/imports/js/requestTimeout.min.js", id: "request"},

            // images
            {src: "src/imports/img/logo.png",         id: "logo"},

            // sounds
            {src: "src/imports/snd/background.mp3",   id: "bgSound"},
            {src: "src/imports/snd/box-reminder.mp3", id: "boxReminderSound"},
            {src: "src/imports/snd/box-reveal.mp3",   id: "boxRevealSound"},
            {src: "src/imports/snd/box-start.mp3",    id: "boxStartSound"},
            {src: "src/imports/snd/box-win.mp3",      id: "boxWinSound"},
            {src: "src/imports/snd/end-win.mp3",      id: "endWinSound"},
            {src: "src/imports/snd/end-lose-loop.mp3",id: "endLoseSound"},

            // ticket
            {src: "src/ticket.xml",                   id: "ticket"},


            // json
            {src: "src/imports/json/master.json",     id: "master"},

            // core
            // javascript
            {src: "src/Core/SpriteSheets.js",         id: "SpriteSheets"},
            {src: "src/Core/Helper.js",               id: "Helper"},
            {src: "src/Core/Defaults.js",             id: "Defaults"},
            {src: "src/lib/Animate.js",               id: "Animate"},
            {src: "src/lib/Overrides.js",             id: "Overrides"},
            {src: "src/Core/ErrorInvoker.js",         id: "ErrorInvoker"},
            {src: "src/Core/Ticket.js",               id: "Ticket"},
            {src: "src/Core/GameAsset.js",            id: "GameAsset"},
            {src: "src/Core/Events.js",               id: "Events"},
            {src: "src/Core/Sounds.js",               id: "Sounds"},
            {src: "src/Core/Canvas.js",               id: "Canvas"},
            {src: "src/Core/Legend.js",               id: "Legend"},
            {src: "src/Core/FullScreen.js",           id: "FullScreen"},
            {src: "src/Core/SettingsCog.js",          id: "SettingsCog"},
            {src: "src/lib/MainGameLayout.js",        id: "MainGameLayout"},
            {src: "src/lib/Splash.js",                id: "Splash"},
            {src: "src/lib/EndGame.js",               id: "EndGame"},
            {src: "src/Game.js",                      id: "Game"},



        ];

        /**
         * @return [{src: String, id: String}]
         */
        this.getIncludes = function() {
            return _includes;
        };

    };

    window.Includes = Includes;

}(window));
