/**
 * @class   EndGame
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader          = window.Loader,
        Helper          = Loader.Helper,
        Ticket          = Loader.Ticket,
        Sounds          = Loader.Sounds,
        Canvas          = Loader.Canvas,
        MainGameLayout  = Loader.MainGameLayout;

    /**
     * @return Animate
     */
    var EndGame = function() {

        var _endGameContainer = new createjs.Container();

        this.getEndGameContainer = function() {
            return _endGameContainer;
        }

        // singleton pattern
        if (EndGame.instance instanceof EndGame) {
            return EndGame.instance;
        }
        EndGame.instance = this;

        init();

    };

    function init(){

        var canvas      = Canvas.instance,
            halfWidth   = Math.floor(canvas.getGameWidth() / 2),
            gameCont    = Loader.Game.instance.getMainContainer().children[0],
            amount      = Ticket.instance.getOutcome("amount"),
            endSound    = "endLoseSound",
            instant     = gameCont.getChildByName('instantCash');


        // make end game screen
        var container   = new createjs.Container();
        container.name  = "endGameContainer";
        container.x     = halfWidth;
        container.y     = -220;

        var endGameBG   = Helper.makeBitmapImage("endgame_bg", {x: 0, y: 0}, 1, true),
            finish      = Helper.makeBitmapImage("finish", {x: 0, y: 0}, 1, true),
            message     = new createjs.Container();


        if (amount > 0){
            var amountString= "end" + amount.toString(),
                amount      = Helper.makeBitmapImage(amountString, {x: 0, y: 15}, 1, true),
                congrats    = Helper.makeBitmapImage("endgame_congrats", {x: 0, y: -30}, 1, true),
                endSound    = "endWinSound";

            message.addChild(amount, congrats);
        } else {
            var condolences = Helper.makeBitmapImage('endgame_lose', {x:0, y:0}, 1, true);

            message.addChild(condolences);
        }

        finish.y = 70;

        finish.on('click', function(){
            console.log('click');
        }, null, true)

        container.addChild(endGameBG, message, finish);

        gameCont.addChild(container);

        // slide in the end game
        TweenMax.to(container, 0.5, { y: 0, delay: 2.5, onStart: function(){
                // play end game sound
                Sounds.instance.getChannels()[0].getSoundByName(endSound).play();
            }});

        // fade instance Cash off
        TweenMax.to(instant, 0.5, {alpha: 0, delay: 2});

    }

    Loader.instance.addToNamespace("window.Loader.EndGame", EndGame);
    EndGame.n = "EndGame";
    EndGame.v = "0.0.2";


})(window);
