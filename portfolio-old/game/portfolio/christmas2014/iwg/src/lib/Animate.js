/**
 * @class   Animate
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader  = window.Loader,
        Helper  = Loader.Helper;

    /**
     * @return Animate
     */
    var Animate = function() {

        // singleton pattern
        if (Animate.instance instanceof Animate) {
            return Animate.instance;
        }
        Animate.instance = this;

    };

    Loader.instance.addToNamespace("window.Loader.Animate", Animate);
    Animate.n = "Animate";
    Animate.v = "0.0.2";


    Animate.intro = function(gameAsset){



    };
    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.reveal = function(gameAsset) {

    };

    /**
     * @param  GameAsset
     * @return Undefined
     */
    Animate.matchWinReveal = function(gameAsset) {

    };

    /**
     * @return Undefined
     */
    Animate.idle = function() {
        console.log("idle user");
    };
    /**
     * @return Undefined
     */
    Animate.idleAnimation = function() {
        console.log("idle user");
    };

    /* Animate.Snow
     *
     */
    Animate.snow = function(maxParticles) {

        if(!Helper.isDefined(maxParticles)){
            maxParticles = 100;
        }

        var container   = new createjs.Container();

        for(var i = 0; i < maxParticles; i++){
            var snow    = new createjs.Shape(),
                size    = Helper.random(2,7);

            snow.graphics.beginFill("white").drawCircle(0, 0, size);
            snow.x      = Helper.random(0, 980);
            snow.y      = Helper.random(-7, -50);

            // density
            snow.size   = size;

            snow.name   = "snow" + i;
            container.addChild(snow);

            // side to side
            var snowMoveX   = new TimelineMax({repeat: -1, yoyo: 1}),
                snowMoveY   = new TimelineMax({repeat: -1}),
                origX       = snow.x;

            snowMoveX.to(snow, 10 / (snow.size / 2), { x: origX + Helper.random(150,350), ease: "easeInOutSine" })
                     .to(snow, 10 / (snow.size / 2), { x: origX, ease: "easeInOutSine" });

            snowMoveY.to(snow, Helper.random(10, 25), {y: 650, delay: Helper.random(1, 20)});

        }

        return container;

    }

    /* Animate.explode
     *
     */
    Animate.explode = function(maxParticles, asset) {

        var explosionContainer  = new createjs.Container();
        explosionContainer.x    = 0;
        explosionContainer.y    = 30;

        for ( var angle = 0; angle < 360; angle += Math.round(360/maxParticles)){

            var particle = new createjs.Shape();
            particle.graphics.beginFill("white").drawCircle(0, 0, Helper.random(10, 30));
            TweenMax.to(particle, 0.5, { y: Helper.random( -200, 200), x: Helper.random(-200, 200), alpha: 0});
            explosionContainer.addChild(particle);

        }

        Helper.moveToTop(asset.getContainer().getChildByName('iconContainer').getChildByName('bot'));
        Helper.moveToTop(asset.getContainer().getChildByName('iconContainer').getChildByName('top'));
        asset.getContainer().addChild( explosionContainer );
    }

    /* Animate.sparkles
     *
     */
    Animate.sparkles = function(shimmerX, shimmerY, img ) {

        // make image
        var shimmer = Helper.makeBitmapImage(img, { x: shimmerX, y: shimmerY}, 1, true);
        shimmer.scaleX = shimmer.scaleY = 0;

        var scale = Helper.randomFloat(0.5)

        // animate image
        TweenMax.to(shimmer, Helper.randomFloat(0.8, 1), {scaleX: 0.7, scaleY: 0.7, rotation: 360, yoyo: true, repeat: -1})

        return shimmer;

    }
}(window));
