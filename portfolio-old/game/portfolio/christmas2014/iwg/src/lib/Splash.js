(function (window) {
    "use strict";

    var Loader  = window.Loader,
        Canvas  = Loader.Canvas,
        Sounds  = Loader.Sounds,
        Events  = Loader.Events,
        Helper  = Loader.Helper,
        Game    = Loader.Game;


    var Splash = function(name, width, height){

        var _name   = name,
            _width  = width,
            _height = height,
            _container  = new createjs.Container();

        _container.name = _name;

        this.getWidth = function(){
            return _width;
        }
        this.getHeight = function(){
            return _height;
        }
        this.getContainer = function(){
            return _container;
        }

        init(this);

        if (Splash.instance instanceof Splash) {
            return Splash.instance;
        }
        Splash.instance = this;
    };

    function init(self){

        var layout = setupLayout(self);
        self.getContainer().addChild(layout);

        // event
        Events.instance.addListener("startButtonClick", startButtonClick);

    }


    function setupLayout(self){

        var gameWidth   = Canvas.instance.getGameWidth(),
            gameHeight  = Canvas.instance.getGameHeight();

        var winupto     = Helper.makeBitmapImage("winupto", {x: gameWidth / 2, y: 120}, 1, true),
            hat         = Helper.makeBitmapImage("start_hat", {x: 580, y: 330}, 1, true),
            button      = Helper.makeBitmapImage("play", {x: gameWidth / 2, y: 0}, 1, true),
            ballRed1    = Helper.makeBitmapImage("bauble_red", {x: 250, y: 210}, 1, true),
            ballRed2    = Helper.makeBitmapImage("bauble_red", {x: 140, y: 410}, 1, true),
            ballRed3    = Helper.makeBitmapImage("bauble_red", {x: 750, y: 340}, 1, true),
            ballYellow1 = Helper.makeBitmapImage("bauble_yellow", {x: 210, y: 310}, 1, true),
            ballYellow2 = Helper.makeBitmapImage("bauble_yellow", {x: 705, y: 190}, 1, true),
            ballYellow3 = Helper.makeBitmapImage("bauble_yellow", {x: 690, y: 420}, 1, true),
            container   = new createjs.Container();

        button.on('click', function(){
            Events.instance.dispatchEvent('startButtonClick');
        });



        container.y     = 200;
        container.name  = "splashContainer";
        button.y        = 340;

        // everyAsset to have a name
        button.name     = "button";

        container.addChild(winupto, button, hat, ballRed1, ballRed2, ballRed3, ballYellow1, ballYellow2, ballYellow3);

        return container;

    }

    function startButtonClick(){
        Sounds.instance.getChannels()[0].getSoundByName("boxStartSound").play();
        Canvas.instance.getGameStage().removeChild(Canvas.instance.getGameStage().getChildByName("santaContainer"));

        Events.instance.dispatchEvent('mainGameIntro');

    }

    Loader.instance.addToNamespace("Loader.Splash", Splash);
    Splash.n = "Splash";
    Splash.v = "0_0_1";
}(window));
