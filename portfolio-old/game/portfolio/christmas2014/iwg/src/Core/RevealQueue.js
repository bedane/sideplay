/**
 * @class   RevealQueue
 * @author  Sideplay
 * @version 0.0.1
 */

(function(window) {

    "use strict";

    var Loader = window.Loader,
        Helper = Loader.Helper,
        Events = Loader.Events;

    /**
     * @param  [GameAsset]
     * @param  GameAsset
     * @param  Boolean
     * @return RevealQueue
     */
    var RevealQueue = function(assets, prizeAsset, isWinner) {

        var _assets     = assets,
            _prizeAsset = prizeAsset,
            _isWinner   = isWinner,
            _animLock   = false,
            _isFinished = false;

        /**
         * @return [GameAsset]
         */
        this.getAssets = function() {
            return _assets;
        };

        /**
         * @return GameAsset
         */
        this.getPrizeAsset = function() {
            return _prizeAsset;
        };

        /**
         * @return Boolean
         */
        this.getIsWinner = function() {
            return _isWinner;
        };

        /**
         * @return Boolean
         */
        this.getAnimLock = function() {
            return _animLock;
        };

        /**
         * @return Boolean
         */
        this.getIsFinished = function() {
            return _isFinished;
        };

        /**
         * @param  Boolean
         * @return Undefined
         */
        this.setAnimLock = function(animLock) {
            _animLock = animLock;
        };

        /**
         * @param  Boolean
         * @return Undefined
         */
        this.setIsFinished = function(isFinished) {
            _isFinished = isFinished;
            if (_isFinished) {
                Events.instance.dispatchEvent("rowFinished");
            }
        };

        init.bind(this)();

    };

    Loader.instance.addToNamespace("window.Loader.RevealQueue", RevealQueue);
    RevealQueue.n = "RevealQueue";
    RevealQueue.v = "0.0.1";

    /**
     * @param  GameAsset
     * @return Undefined
     */
    RevealQueue.prototype.reveal = function(asset) {

        if (this.getAnimLock()) {
            return;
        }

        var key    = "reveal",
            queue  = this.getAssets(),
            prize  = this.getPrizeAsset(),
            events = Events.instance;

        if (asset !== prize) {
            var index = queue.indexOf(asset);
            queue.splice(index, 1);
            queue.unshift(asset);
            queue.push(prize);
        } else {
            queue.unshift(prize);
        }

        var delays = queue.map(function(asset) {
            return asset.getAnimations()[key].delay;
        });
        delays[0] = 0;

        this.setAnimLock(true);

        for (var i = 0; i < queue.length; i++) {

            TweenMax.delayedCall(Helper.sum(delays, i) + delays[i], function(asset, i) {

                events.dispatchEvent("revealStart", [asset]);
                asset.animate(key);
                asset.setIsRevealed(true);

                TweenMax.delayedCall(delays[i], function() {

                    events.dispatchEvent("revealEnd", [asset]);

                    if (i === queue.length - 1) {
                        this.winReveal();
                    }

                }, null, this, true);

            }, [queue[i], i], this, true);

        }

    };

    /**
     * @return Undefined
     */
    RevealQueue.prototype.winReveal = function() {

        var key      = "winReveal",
            assets   = this.getAssets().concat([this.getPrizeAsset()]),
            events   = Events.instance,
            delays   = assets.map(function(asset) {
                return asset.getAnimations()[key].delay;
            }),
            maxDelay = Math.max.apply(Math, delays);

        if (this.getIsWinner()) {

            assets.forEach(function(asset, i) {

                TweenMax.delayedCall(delays[i], function() {
                    events.dispatchEvent("winRevealStart", [assets]);
                    asset.animate(key);
                    asset.setIsWinRevealed(true);
                }, null, null, true);

            });

            TweenMax.delayedCall(maxDelay, function() {
                this.setAnimLock(false);
                this.setIsFinished(true);
                events.dispatchEvent("winRevealEnd", [assets]);
            }, null, this, true);

        } else {
            this.setAnimLock(false);
            this.setIsFinished(true);
        }

    };

    /**
     * @param  RevealQueue
     * @return Undefined
     */
    function init() {

        this.getAssets().forEach(function(asset) {
            if (asset.getIsClickable()) {
                Helper.setButtonListeners(asset.getContainer().getChildByName("symbol"), function() {
                    this.reveal(asset);
                }.bind(this), true);
            }
        }.bind(this));

        var prize = this.getPrizeAsset();

        if (prize.getIsClickable()) {
            Helper.setButtonListeners(prize.getContainer().getChildByName("symbol"), function() {
                this.reveal(prize);
            }.bind(this), true);
        }

    }

}(window));
