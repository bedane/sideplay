/**
 * @class   ErrorInvoker
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    var Loader    = window.Loader,
        Helper    = Loader.Helper,
        Defaults  = Loader.Defaults,
        Overrides = Loader.Overrides;

    /**
     * @return ErrorInvoker
     */
    var ErrorInvoker = function() {

        // singleton pattern
        if (ErrorInvoker.instance instanceof ErrorInvoker) {
            return ErrorInvoker.instance;
        }
        ErrorInvoker.instance = this;

    };

    Loader.instance.addToNamespace("window.Loader.ErrorInvoker", ErrorInvoker);
    ErrorInvoker.n = "ErrorInvoker";
    ErrorInvoker.v = "0.0.2";

    /**
     * @param  {progress: Number}
     * @return String
     */
    ErrorInvoker.LoaderError = function(data) {
        return "LoaderError: loading failed at " + String(data.progress) + "%, please " +
            "refresh the page to try again";
    };

    /**
     * @param  {includeId: String}
     * @return String
     */
    ErrorInvoker.IncludesError = function(data) {
        return "IncludesError: game include `" + data.includeId + "` is undefined";
    };

    /**
     * @param  {support: String}
     * @return String
     */
    ErrorInvoker.BrowserError = function(data) {
        return "BrowserError: " + data.support + " is not supported by your web browser";
    };

    /**
     * @param  {soundId: String}
     * @return String
     */
    ErrorInvoker.SoundsError = function(data) {
        return "SoundsError: sound `" + data.soundId + "` is undefined";
    };

    /**
     * @param  {eventId: String}
     * @return String
     */
    ErrorInvoker.EventsError = function(data) {
        return "EventsError: event listener `" + data.eventId + "` is undefined";
    };

    /**
     * @param  {animId: String}
     * @return String
     */
    ErrorInvoker.GameAssetError = function(data) {
        return "GameAssetError: animation `" + data.animId + "` is undefined";
    };

    /**
     * @param  {status: String}
     * @return String
     */
    ErrorInvoker.TicketError = function(data) {
        return "TicketError: " + data.status;
    };

    /**
     * @param  {screenNum: Number}
     * @return String
     */
    ErrorInvoker.CanvasError = function(data) {
        return "CanvasError: screen " + String(data.screenNum) + " is undefined";
    };

    /**
     * @param  Object -> String
     * @param  Object
     * @return Undefined
     */
    ErrorInvoker.raise = function(exception, data) {

        var gameArea = document.getElementById("gameArea"),
            loading  = document.getElementById("loading");

        // mute all sounds
        createjs.Sound.setMute(true);

        // remove all scripts
        Array.prototype.slice.call(document.getElementsByTagName("script")).forEach(function(js) {
            js.remove();
        });

        // display error screen
        document.body.appendChild(Helper.compose(createHTMLOverlay, exception)(data));

        // remove all other page elements
        [loading, gameArea].filter(Helper.isDefined).forEach(function(node) {
            node.remove();
        });

        // raise (uncaught) exception to halt execution
        throw new Helper.compose(Error, exception)(data);

    };

    /**
     * @param  String
     * @return HTMLDivElement
     */
    function createHTMLOverlay(message) {

        var cont  = document.createElement("div"),
            inner = document.createElement("div"),
            logo  = document.createElement("img");

        inner.appendChild(Helper.isDefined(Overrides.errorInvoker.htmlLogo) ?
            Overrides.errorInvoker.htmlLogo : Defaults.errorInvoker.htmlLogo);

        inner.innerHTML += (Helper.isDefined(Overrides.errorInvoker.htmlText) ?
                               Overrides.errorInvoker.htmlText : Defaults.errorInvoker.htmlText) + message;

        cont.id = "error";
        cont.className = "fillPage";

        cont.appendChild(inner);

        return cont;

    }

})(window);
