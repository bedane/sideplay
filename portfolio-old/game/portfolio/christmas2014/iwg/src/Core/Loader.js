/**
 * @class   Loader
 * @author  Sideplay
 * @version 0.0.2
 */

(function(window) {

    "use strict";

    /**
     * @return Loader
     */
    var Loader = function() {

        if (Loader.instance instanceof Loader) {
            return Loader.instance;
        }
        Loader.instance = this;

        /* loading queue for images, sounds, xml, json and javascript includes, all of the form
           {src: String, id: String, ext: String, ...} */
        var _queue = new createjs.LoadQueue();

        /**
         * @return createjs.LoadQueue
         */
        this.getQueue = function() {
            return _queue;
        };

        init();

    };

    window.Loader = Loader;

    /**
     * @param  String
     * @return Object
     * @throws IncludesError
     */
    Loader.prototype.getInclude = function(includeId) {
        var queue = this.getQueue();
        if (!Loader.Helper.isDefined(queue.getResult(includeId))) {
            Loader.ErrorInvoker.raise(Loader.ErrorInvoker.IncludesError, {includeId: id});
        }
        return queue.getResult(includeId);
    };

    /**
     * @param  String
     * @return Object
     */
    Loader.prototype.getIncludesOfType = function(type) {

        var includes = this.getQueue()._loadItemsById,
            filtered = [];

        for (var include in includes) {
            if (includes[include].type === type) {
                filtered.push(includes[include]);
            }
        }

        return filtered;

    };

    /**
     * @param  String
     * @return Object
     */
    Loader.prototype.addToNamespace = function(location, name) {

        var pathParts   = location.split("."),
            currentPath = null,
            previousObj = null;

        for (var i = 0; i < pathParts.length; ++i) {
            previousObj = window[pathParts[i] - 1];
            currentPath = pathParts[i];
        }

        if (!previousObj.hasOwnProperty(name)) {
            Loader[currentPath] = name;
        }

    };

    /**
     * @param  Boolean
     * @return Undefined
     */
    Loader.prototype.deploy = function(deploy) {

        if (deploy) {
            window.Loader   = null;
            window.Includes = null;
            delete window.Includes;
            delete window.Loader;
        } else {
            for (var obj in Loader) {
                if (obj !== "instance") {
                    window[obj] = null;
                    window[obj] = Loader[obj];
                }
            }
            window.log   = console.log.bind(console);
            window.warn  = console.warn.bind(console);
            window.error = console.error.bind(console);
        }

    };

    /**
     * @return Undefined
     */
    function init() {
        setupLoadingScreen();
        setupProgressBar();
        loadIncludes();
    }

    /**
     * @return Undefined
     */
    function setupLoadingScreen() {

        var container  = document.createElement("div"),
            topHalf    = document.createElement("div"),
            botHalf    = document.createElement("div"),
            logo       = document.createElement("div"),
            title      = document.createElement("p");

        window.scrollTo(0, 0);

        container.id = "loading";
        container.className = "fillPage";

        topHalf.id      = "topHalf";
        botHalf.id      = "botHalf";
        logo.id         = "logo";
        title.id        = "title";
        title.innerText = document.title;

        topHalf.appendChild(logo);
        botHalf.appendChild(title);
        container.appendChild(topHalf);
        container.appendChild(botHalf);
        document.body.appendChild(container);

    }

    /**
     * @return Undefined
     */
    function setupProgressBar() {

        var left    = document.createElement("div"),
            right   = document.createElement("div"),
            botHalf = document.getElementById("botHalf");

        left.id         = "loaderLeft";
        right.id        = "loaderRight";
        left.className  = "loaderBar";
        right.className = "loaderBar";

        botHalf.appendChild(left);
        botHalf.appendChild(right);

    }

    /**
     * @return Undefined
     */
    function loadIncludes() {

        var includes = new Includes().getIncludes(),
            queue    = Loader.instance.getQueue(),
            canvas   = document.getElementById("game");

        queue.installPlugin(createjs.Sound);
        createjs.Sound.alternateExtensions = ["ogg"];

        for (var file in includes) {
            queue.loadFile(includes[file]);
        }

        queue.on("complete", loadingComplete, this);
        queue.on("progress", loadingProgress, this);
        queue.on("error",    loadingError,    this);

        var bar = new createjs.Shape();
        bar.graphics.beginFill(createjs.Graphics.getRGB(255, 255, 255)).drawRect(0, 0, 110, 20).endFill();

        queue.load();

    }

    /**
     * @param  Object
     * @return Undefined
     */
    function loadingComplete(event) {

        new Loader.SpriteSheets(Loader.Overrides.spriteSheets);
        new Loader.Animate(Loader.Overrides.animate);
        new Loader.ErrorInvoker(Loader.Overrides.errorInvoker);
        new Loader.Sounds(Loader.Overrides.sounds);
        new Loader.Events(Loader.Overrides.events);
        new Loader.Canvas(Loader.Overrides.canvas);
        new Loader.SettingsCog(Loader.Overrides.settingsCog);
        new Loader.FullScreen(Loader.Overrides.fullScreen);
        new Loader.Game();

    }

    /**
     * @param  Object
     * @return Undefined
     */
    function loadingProgress(event) {
        var percentage = Math.floor(event.loaded * 100 / 2).toString() + "%";
        document.getElementById("loaderLeft").style.width  = percentage;
        document.getElementById("loaderRight").style.width = percentage;
    }

    /**
     * @param  Object
     * @return Undefined
     * @throws LoadingError
     */
    function loadingError(event) {
        var progress = Math.round(event.target.progress * 100);
        if (typeof Loader.ErrorInvoker !== "undefined") {
            Loader.ErrorInvoker.raise(Loader.ErrorInvoker.LoaderError, {progress: progress});
        } else {
            throw new Error("LoaderError: Loading game failed at " + String(progress) + "%, " +
                "please refresh the page to try again");
        }
    }

})(window);
