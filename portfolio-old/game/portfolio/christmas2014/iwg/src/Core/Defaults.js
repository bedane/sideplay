/**
 * @file Simulated singleton class for specifying game default values.
 * @version 1.0.3
 * @author Sideplay
 * @copyright Sideplay 2014
 */

(function(window) {

    "use strict";

    // imports
    var Loader = window.Loader,
        Helper = Loader.Helper;

    /**
     * Lazily initialises the singleton Defaults instance.
     * @class
     * @classdesc Simulated singleton class for specifying game default values.
     * @alias Defaults
     */
    var Defaults = function() {

        // singleton pattern
        if (Defaults.instance instanceof Defaults) {
            return Defaults.instance;
        }
        Defaults.instance = this;

    };

    /*
     * Canvas defaults.
     * @public
     * @static
     * @member {!object}
     */
    Defaults.canvas = {

        size:          {width: 960, height: 640},
        fps:           30,
        screens:       2,
        idleThreshold: 150,
        idleAnimation: Helper.emptyFunction

    };

    /*
     * Error invoker defaults.
     * @public
     * @static
     * @member {!object}
     */
    Defaults.errorInvoker = {

        telNum:   "&lt;insert number here&gt;",
        htmlLogo: Loader.instance.getInclude("logo"),
        init:     function() {
                      this.htmlText = "<p>Sorry, there was a problem loading your game.</p> "     +
                          "Please check your internet connection. You could also try refreshing " +
                          "the page or signing in to reload your purchased game. <p>If this "     +
                          "doesn't work, please contact our Customer Care Team on " + this.telNum +
                          " for further assistance, quoting reference:</p>";
                      delete this.init;
                      return this;
                  }

    }.init();

    /*
     * Full screen defaults.
     * @public
     * @static
     * @member {!object}
     */
    Defaults.fullScreen = {

        text: "Slide up to hide menu bars"

    };

    /*
     * Settings cog defaults.
     * @public
     * @static
     * @member {!object}
     */
    Defaults.settingsCog = {

        cogImg:         "settings_cog",
        cogPos:         {x: Defaults.canvas.size.width - 50, y: Defaults.canvas.size.height - 50},
        easing:         Cubic.easeOut,
        modalSize:      {width: Defaults.canvas.size.width - 170, height: Defaults.canvas.size.height - 120},
        modalPos:       function(modalSize) {
                            return {x: Math.floor((Defaults.canvas.size.width  - modalSize.width)  / 2),
                                    y: Math.floor((Defaults.canvas.size.height - modalSize.height) / 2)};
                        },
        modalCol:       "#B9B9B9",
        modalAlpha:     1,
        overlayCol:     "#000",
        modalRad:       10,
        modalPosFrom:   function(modalSize, borderWidth, modalPos) {
                            return {x: modalPos.x, y: -modalSize.height - borderWidth + 5};
                        },
        modalPosTo:     function(modalPos) {
                            return {x: modalPos.x, y: modalPos.y};
                        },
        soundPosY:      function(modalSize, buttonHeight) {
                            return modalSize.height - buttonHeight;
                        },
        borderCol:      "#FFF",
        borderWidth:    15,
        alpha:          0.6,
        hasEffects:     true,
        hasBackground:  true,
        effectsImg:     "icon_allsounds",
        backgroundImg:  "icon_music",
        effectsRegX:    true,
        backgroundRegX: true,
        animTime:       15,
        animFade:       false,
        effectsPos:     function(modalWidth) {
                            return {x: Math.floor(modalWidth / 4), y: 0};
                        },
        backgroundPos:  function(modalWidth) {
                            return {x: Math.floor(modalWidth * 0.75), y: 0};
                        },
        init:           function() {
                            this.initial = new createjs.Container();
                            delete this.init;
                            return this;
                        }

    }.init();

    /*
     * Legend defaults.
     * @public
     * @static
     * @member {!object}
     */
    Defaults.legend = {

        legendRows:  [],
        revealKind:  "sequential",
        revealDelay: 900

    };

    /**
     * ...
     * @public
     * @static
     * @param {!string}
     * @param {!object}
     * @param {!object}
     * @return {!object}
     * @throws {DefaultsError}
     */
    Defaults.getArgOrDefault = function(prop, args, defaults) {

        // if prop is overriden in arguments object
        if (Helper.isDefined(args) && Helper.isDefined(args[prop])) {
            return args[prop];
        }

        // otherwise, if prop exists, get its default value
        if (Helper.isDefined(defaults) && Helper.isDefined(defaults[prop])) {
            return defaults[prop];
        }

        // regular exception, since Defaults depending on ErrorInvoker is circular
        throw new Error("Defaults.getArgOrDefault: " + prop + " cannot be found");

    };

    Loader.instance.addToNamespace("window.Loader.Defaults", Defaults);
    Defaults.n = "Defaults";
    Defaults.v = "1.0.3";

})(window);
