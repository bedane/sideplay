//Money Tree
var gameStage = new createjs.Stage(game),
screenX = game.width,
screenY = game.height,
maskerCont = new createjs.Container(),
eIWG = escapeIWLoader,
eIWGpl = escapeIWLoader.preloader,
eIWGcF = escapeIWLoader.commonFunctions;

function start(){
	"use strict";
	MoneyTree.game.start();
}

function preload(){
	"use strict";
	MoneyTree.game.preload();
	//MoneyTree.game.fs();
}

var MoneyTree = MoneyTree || {};

MoneyTree.game = (function(){
	"use strict";
	var self = {};

	self.endGameContainer = new createjs.Container();
	self.bgCont 	= new createjs.Container();
	self.gameCont 	= new createjs.Container();
	self.charCont 	= new createjs.Container();
	self.gearCont	= new createjs.Container();
	self.fsOverlayCont = new createjs.Container();
	self.bgDisplay 	= {};
	self.character 	= {};
	self.soundObj 	= {};
	self.masker 	= [];
	self.endGameButton 	= {};

	self.highImageManifest = [{id:"sSheet",src:"img/export/money-tree.png"},{id:"JSON",src:"img/export/money-tree.json"},
	{id:"leafSheet",src:"img/export/MoneyTree.png"},{id:"leafJSON",src:"img/export/MoneyTree.json"},
	{id:"charSheet",src:"img/export/MoneyTree_ladybird_itemised.png"},{id:"charJSON",src:"img/export/MoneyTree_ladybird_itemised.json"},{id:"gearsSheet",src:"img/export/settingsGear.png"},{id:"gearJSON",src:"img/export/settingsGear.json"}];
	self.highSoundManifest = [{id:"clickSnd",src:"snd/click.mp3"},{id:"revealSnd",src:"snd/reveal.mp3"},{id:"leavesSnd",src:"snd/leaves.mp3"},{id:"branchSnd",src:"snd/branches.mp3"},{id:"bugSnd",src:"snd/bug2.mp3"},{id:"lineWinSnd",src:"snd/line win.mp3"},{id:"endWinSnd",src:"snd/end win.mp3"},{id:"endLoseSnd",src:"snd/end lose.mp3"}];

	self.ticketRaw 	= {};
	self.ticket 	= {};
	self.ssImage 	= null;
	self.sSheetJSON = null;
	self.sLeafImage = null;
	self.sLeafJSON 	= null;
	self.sCharImage = null;
	self.sCharJSON 	= null;
	self.ssBgImage 	= null;
	self.banked 	= 0.00;
	self.bankedValues = [];
	self.clickCount = 0;

	self.isMuted = false;

	self.idleTimer = 0;
	self.idleTimerTarget = 150;

	self.allLeaves 		= [];
	self.winningLeaves  = [];
	self.allAnims 		= ["rightRotate","leftRotate","bothRotate","bothRotate","bothRotate","bothBlink","bothBlink","bothBlink"];

	self.endGameTriggered 	= false;
	self.endGameStarted		= false;
	self.gameStarted 		= false;

	self.turnGameAssoc = [-1,-1,-1,-1,-1,-1];

	self.winningPrizes 	= [];
	self.winningSymbols = [];
	self.winningFlags	= [];
	self.winningIndex	= [];

	self.isWinner = false;
	self.isTicketWinner = null;

	self.startingRotations 	= [-65, 119, 18];
	self.endingRotations	= [0, 171, 0];

	self.isPaused = false;

	self.fsStart = function(){
		escapeIWLoader.fullScreen.startFullScreen(MoneyTree.game.preload, MoneyTree.game.pause, MoneyTree.game.unPause);

	};
	self.preload = function(){
		createjs.Ticker.addEventListener("tick", self.tick);
		createjs.Ticker.setFPS(30);

		escapeIWLoader.scaleOrient.setSize();
		escapeIWLoader.scaleOrient.startResizeHandler();

		escapeIWLoader.overlayMasker.setMaskerColour("black");
		escapeIWLoader.overlayMasker.mask();

		eIWGpl.setQualityEnabled(false);
		eIWGpl.setTicketFile({id:"ticketFromFile",src:"ticket.xml", type:createjs.LoadQueue.XML});
		eIWGpl.setManifests(self.highImageManifest,self.highSoundManifest,null);
		eIWGpl.startPreload();
	};
	self.start = function(){
		self.fsOverlayCont.addChild(gameStage.getChildByName("fsCont"));
		gameStage.removeChild(gameStage.getChildByName("fsCont"));

		self.ticketRaw  		= eIWGpl.getPreloaderResults("ticketFromFile");
		self.ssImage    		= eIWGpl.getPreloaderResults("sSheet");
		self.sSheetJSON 		= eIWGpl.getPreloaderResults("JSON");
		self.sSheetJSON.images  = [self.ssImage];
		self.sLeafImage    		= eIWGpl.getPreloaderResults("leafSheet");
		self.sLeafJSON 			= eIWGpl.getPreloaderResults("leafJSON");
		self.sLeafJSON.images   = [self.sLeafImage];
		self.sCharImage    		= eIWGpl.getPreloaderResults("charSheet");
		self.sCharJSON 			= eIWGpl.getPreloaderResults("charJSON");
		self.sCharJSON.images   = [self.sCharImage];
		self.gearssImage		= eIWGpl.getPreloaderResults("gearsSheet");
		self.gearsSheetJSON			= eIWGpl.getPreloaderResults("gearJSON");
		self.gearsSheetJSON.images 	= [self.gearssImage];

		self.sLeafJSON.animations.hover = {};
		self.sLeafJSON.animations.hover.frames = [0,1,2,3];

		self.readTicketInfo();
		if(self.validateTicket()){
			self.isTicketWinner = self.ticket.winToken;
			gameStage.enableMouseOver();

			gameStage.addChild(self.bgCont);

			self.endGameContainer.alpha = 0;
			self.endGameContainer.x = 375;

			gameStage.addChild(self.endGameContainer);
			gameStage.addChild(self.gameCont);
			gameStage.addChild(self.gearCont);
			gameStage.addChild(maskerCont);
			gameStage.addChild(self.fsOverlayCont);
			//self.setupMasker();
			self.setupBackground();
			self.setupLayout();
			self.setupCharacter();

			self.registerSounds();
			self.setupSettingsGear();

			self.bugSoundLoops();
			//self.updateBg();
		} else {
			escapeIWLoader.errorInvoker.invokeError("Ticket failed validation test.");
		}
  	};
  	self.setupMasker = function(){
  		var masks = [[0,0,-1000,1640],[-500,640,2000,1000],[960,0,1000,1640]],
  		i = 0;

  		for(i = 1; i < 2; i+=1){
			var s = new createjs.Shape();
			maskerCont.addChild(s);

			var img = new Image();
			img.src = 'img/tile_grass.jpg';

			s.graphics.beginBitmapFill(img, 'repeat');
			s.graphics.drawRect(masks[i][0],masks[i][1],masks[i][2],masks[i][3]);
  		}
  	};
	self.setupBackground = function(){
		self.bgDisplay.background = new createjs.Shape();
		var img = new Image();
		img.src = 'img/tile_grass.jpg';

		self.bgDisplay.background.graphics.beginBitmapFill(img, 'repeat');
		self.bgDisplay.background.graphics.drawRect(-500,0,1960,1000);


		self.bgDisplay.branchesCont 	= new createjs.Container();
		self.bgDisplay.leftBranches 	= eIWGcF.createSprites(self.sSheetJSON,-129,524,"branch_solo","stop","left","top",1);
		self.bgDisplay.rightBranches	= eIWGcF.createSprites(self.sSheetJSON,1152,21,"branch_solo","stop","left","top",1);
		self.bgDisplay.lowerBranches	= eIWGcF.createSprites(self.sSheetJSON,115,806,"branch_solo","stop","center","center",1);

		self.bgDisplay.shadowCont 		= new createjs.Container();
		self.bgDisplay.leftShadow		= eIWGcF.createSprites(self.sSheetJSON,-129,524,"branch_solo_shadow","stop","left","top",1);
		self.bgDisplay.rightShadow		= eIWGcF.createSprites(self.sSheetJSON,1202,29,"branch_solo_shadow","stop","left","top",1);
		self.bgDisplay.lowerShadow		= eIWGcF.createSprites(self.sSheetJSON,443,736,"branch_solo_shadow","stop","center","center",1);

		self.bgDisplay.innerglow 		= eIWGcF.createSprites(self.sSheetJSON,480,320,"innerglow","stop","center","center",1);
		self.bgDisplay.bushes_top 		= eIWGcF.createSprites(self.sSheetJSON,0,0,"bushes_top","stop","left","top",1);
		self.bgDisplay.bushes_left 		= eIWGcF.createSprites(self.sSheetJSON,0,0,"bushes_left","stop","left","top",1);
		self.bgDisplay.bushes_bottom 	= eIWGcF.createSprites(self.sSheetJSON,960,640,"bushes_bottom","stop","right","bottom",1);
		self.bgDisplay.bushes_right 	= eIWGcF.createSprites(self.sSheetJSON,960,640,"bushes_right","stop","right","bottom",1);

		self.bgDisplay.startCont		= new createjs.Container();
		self.bgDisplay.logo				= eIWGcF.createSprites(self.sSheetJSON,0,60,"logo","stop","center","center",1);
		self.bgDisplay.dropLogo			= eIWGcF.createSprites(self.sSheetJSON,0,-200,"logo","stop","left","top",1);
		self.bgDisplay.winupto			= eIWGcF.createSprites(self.sSheetJSON,20,120,"winupto","stop","center","center",1);
		self.bgDisplay.startButton 		= eIWGcF.createSprites(self.sSheetJSON,0,240,"button_play","stop","center","center",1);

		self.bgDisplay.innerglow.scaleX = 1.3;
		//self.bgDisplay.innerglow.scaleX = 0.1+window.innerWidth/960;
		//self.bgDisplay.innerglow.scaleY = 0.1+window.innerHeight/640;

		self.bgDisplay.leftBranches.regX = 0; self.bgDisplay.leftBranches.regY = 419;
		self.bgDisplay.rightBranches.regX = 0; self.bgDisplay.rightBranches.regY = 419;
		self.bgDisplay.lowerBranches.regX = 0; self.bgDisplay.lowerBranches.regY = 419;

		self.bgDisplay.leftShadow.regX = -339; self.bgDisplay.leftShadow.regY = 457;
		self.bgDisplay.rightShadow.regX = 0; self.bgDisplay.rightShadow.regY = 419;
		self.bgDisplay.lowerShadow.regX = 0; self.bgDisplay.lowerShadow.regY = 419;

		self.bgDisplay.leftShadow.scaleX = 0.6; self.bgDisplay.leftShadow.scaleY = 0.6;
		self.bgDisplay.rightShadow.scaleX = 0.6; self.bgDisplay.rightShadow.scaleY = 0.6;
		self.bgDisplay.lowerShadow.scaleX = 0.5; self.bgDisplay.lowerShadow.scaleY = 0.5;

		self.bgDisplay.leftBranches.rotation 	= self.startingRotations[0];
		self.bgDisplay.leftShadow.rotation 		= self.startingRotations[0];
		self.bgDisplay.rightBranches.rotation 	= self.startingRotations[1];
		self.bgDisplay.rightShadow.rotation 	= self.startingRotations[1];
		self.bgDisplay.lowerBranches.rotation 	= self.startingRotations[2];
		self.bgDisplay.lowerShadow.rotation 	= self.startingRotations[2];

		self.bgDisplay.startCont.x = 480; self.bgDisplay.startCont.y = 260;

		self.bgDisplay.startCont.addChild(self.bgDisplay.logo,self.bgDisplay.winupto,self.bgDisplay.startButton);
        self.bgDisplay.shadowCont.addChild(self.bgDisplay.leftShadow,self.bgDisplay.rightShadow,self.bgDisplay.lowerShadow);
        self.bgDisplay.branchesCont.addChild(self.bgDisplay.leftBranches,self.bgDisplay.rightBranches,self.bgDisplay.lowerBranches);
		self.bgCont.addChild(self.bgDisplay.background,self.bgDisplay.bushes_top,self.bgDisplay.bushes_left,self.bgDisplay.bushes_right,self.bgDisplay.bushes_bottom,self.bgDisplay.innerglow,self.bgDisplay.shadowCont,self.bgDisplay.branchesCont,self.bgDisplay.dropLogo,self.bgDisplay.startCont);
	};
	self.registerSounds = function(){
		self.soundObj.click 	= createjs.Sound.createInstance("clickSnd");
		self.soundObj.reveal 	= createjs.Sound.createInstance("revealSnd");

		self.soundObj.branches 	= createjs.Sound.createInstance("branchSnd");
		self.soundObj.leafStart = createjs.Sound.createInstance("leavesSnd");
		self.soundObj.bugFly	= createjs.Sound.createInstance("bugSnd");
		self.soundObj.win 		= createjs.Sound.createInstance("lineWinSnd");
		self.soundObj.endWin	= createjs.Sound.createInstance("endWinSnd");
		self.soundObj.endLose 	= createjs.Sound.createInstance("endLoseSnd");
	};
	self.setMute = function(){
		self.isMuted = !self.isMuted;
	};
	self.bugSoundLoops = function(){
		self.soundObj.bugFly.play();
		self.soundObj.bugFly.setVolume(0);
		self.soundObj.bugFly.on("complete",function(){self.soundObj.bugFly.play();});
	};
	self.crossFadeToHoverSound = function(){
		createjs.Tween.get(self.soundObj.bugFly,{useTicks:true}).to({volume:0},30);
	};
	self.crossFadeToFlyingSound = function(){
		createjs.Tween.get(self.soundObj.bugFly,{useTicks:true}).to({volume:1},30);
	};
	self.setupSettingsGear = function(){
		var styling 	= {},
		mutebutton 		= {};

		styling.background = {};
		styling.background.bgImage = eIWGcF.createSprites(self.sSheetJSON,30,30,"settings_overlay","stop","left","top",1);

		styling.trim = {};
		styling.trim.trimImage = eIWGcF.createSprites(self.gearsSheetJSON,0,0,"settings_overlay_glow","stop","left","top",1);

		//styling.containerX = 100;
		//styling.containerY = -600;

		//styling.animStyle = "fade";
		//styling.animEffectIn = createjs.Ease.elasticOut;
		//styling.animEffectOut= createjs.Ease.linear;
		//styling.animSpeed = 30;

		styling.gear = eIWGcF.createSprites(self.gearsSheetJSON, 870, 550, "settings_cog","stop","center","center",1);
		styling.cross= eIWGcF.createSprites(self.gearsSheetJSON, 680, 60, "close","stop","center","center",1);

		mutebutton.create 		= eIWGcF.createSprites(self.gearsSheetJSON,370,460,"icon_allsounds_large","stop","center","center",1);
		mutebutton.button 		= "icon_allsounds_large";
		mutebutton.buttonPress 	= "icon_allsounds_off_large";
		mutebutton.action 		= MoneyTree.game.muteUnmuteAll;
		mutebutton.statusCheck 	= MoneyTree.game.getMuteStatus;

		g.setup(self.gearCont,self.gameCont,styling,null,mutebutton,null);
	};

	// setup the main character
	self.setupCharacter = function(){
		self.character.body 		= eIWGcF.createSprites(self.sCharJSON,0,0,"body2","stop",null,null,1);
		self.character.leftBlink 	= eIWGcF.createSprites(self.sCharJSON,-40,-20,"blink","stop",null,null,1);
		self.character.rightBlink 	= eIWGcF.createSprites(self.sCharJSON,10,-40,"blink","stop",null,null,1);
		self.character.leftEye 		= eIWGcF.createSprites(self.sCharJSON,-40,-20,"eye","stop",null,null,1);
		self.character.rightEye 	= eIWGcF.createSprites(self.sCharJSON,10,-40,"eye","stop",null,null,1);
		self.character.wings 		= eIWGcF.createSprites(self.sCharJSON,-10,-30,"wings","play",null,null,1);

		self.character.rightBlink.scaleX = 1.2;
		self.character.rightBlink.scaleY = 1.2;
		self.character.rightEye.scaleX = 1.2;
		self.character.rightEye.scaleY = 1.2;

		self.endGameButton 	= eIWGcF.createSprites(self.sSheetJSON,-70,40,"finish","stop",null,null,0);
		self.endGameButton.name = "endButton";

		self.charCont.addChild(self.endGameButton,self.character.wings,self.character.body,self.character.leftEye,self.character.rightEye,self.character.leftBlink,self.character.rightBlink);

		self.charCont.x = 480;
		self.charCont.y = 260;
		self.gameCont.addChild(self.charCont);

		self.character.leftEye.x += 10;self.character.leftEye.y += 30;
		self.character.leftBlink.x += 10;self.character.leftBlink.y += 30;
		self.character.rightEye.x += 10;self.character.rightEye.y += 30;
		self.character.rightBlink.x += 10;self.character.rightBlink.y += 30;
		self.character.rightEye.rotation = -20;
		self.character.leftEye.rotation = 220;

		self.setupStartButton();
	};
	self.loopCharacterAnim = function () {
		var animCurrent = self.allAnims[Math.floor(Math.random() * self.allAnims.length)];
		TweenMax.delayedCall(self.randomFromInterval(20,100), self.characterAnim,  [animCurrent,Math.floor(Math.random() * 100)], this, true);
		if (!self.endGameStarted) {
			TweenMax.delayedCall(self.randomFromInterval(50,100), self.loopCharacterAnim, [], this, true);
		}
	};
	self.characterAnim = function (animType,rotation) {
		if (rotation == undefined) {
			rotation = 0;
		}

		switch (animType) {
		case "bothBlink":
			self.characterAnim("leftBlink");
			self.characterAnim("rightBlink");
			break;
		case "bothRotate":
			var bothRotateAngle = self.randomFromInterval(50,270);
			self.characterAnim("rightRotate",bothRotateAngle);
			self.characterAnim("leftRotate",bothRotateAngle);
			break;
        case "leftBlink":
        	self.character.leftBlink.play();
        	self.character.leftBlink.on("animationend",function(evt){
           		evt.currentTarget.gotoAndStop("blink");
        	});
            break;
        case "rightBlink":
        	self.character.rightBlink.play();
        	self.character.rightBlink.on("animationend",function(evt){
        		evt.currentTarget.gotoAndStop("blink");
        	});
            break;
        case "rightRotate":
        	TweenMax.to(self.character.rightEye, 200, {rotation:rotation, useFrames:true, ease:Elastic.easeOut});
            break;
	    case "leftRotate":
        	TweenMax.to(self.character.leftEye, 200, {rotation:rotation, useFrames:true, ease:Elastic.easeOut});
	        break;
	    case "endGameOff":
	    	self.crossFadeToFlyingSound();
		  	var endTween = TweenMax.to(self.charCont,50, {
		  		useFrames:true,
				bezier:{
			    	type:"soft",
			    	values:[
			    		{x:100,y:100},
			    		{x:0, y: -200}
			    	]
    			},
				ease:Power1.easeInOut,
				onComplete:self.endGamePickupButton}
			);
	        break;
	    case "endGameIn":
        	createjs.Tween.get(self.character.rightEye,{useTicks:true}).to({rotation:-20},15);
        	createjs.Tween.get(self.character.leftEye,{useTicks:true}).to({rotation:220},15);
        	self.charCont.x = 720; self.charCont.y = -120;
	    	var endTween = TweenMax.to(self.charCont,100, {
		  		useFrames:true,
				bezier:{
			      	type:"soft",
					values:[
						{x:600,y:0},
						{x:600,y:50},
						{x:260,y:300},
						{x:150, y:175}
			    	]
			    },
				ease:Power1.easeInOut,
				onComplete:self.crossFadeToHoverSound
			}
			);
	        break;
       	case "startOut":
       		self.crossFadeToFlyingSound();
			var endTween = TweenMax.to(self.charCont,100, {
		  		useFrames:true,
				bezier:{
			    	type:"soft",
			    	values:[
			    		{x:400, y:500},
			    		{x:1100, y:900}
			    	],
			      	autoRotate:["x","y","rotation",-90,false]
    			},
				ease:Power1.easeInOut,
				onComplete:self.onIntroComplete}
			);
       		break;
        }
	};
	self.endGamePickupButton = function(){
		self.endGameButton.alpha = 1;
		self.characterAnim("endGameIn");

		createjs.Tween.get(self.bgDisplay.dropLogo,{useTicks:true}).to({y:-100,alpha:0}, 40, createjs.Ease.elasticInOut).call(function(){
			if(self.isWinner && self.isTicketWinner){
				self.soundObj.endWin.play();
			}
			createjs.Tween.get(self.endGameContainer,{useTicks:true}).to({alpha:1},15);
			self.endGameButtonSetup();
	    });
	};
	self.endGameButtonSetup = function(){
		g.addGearEventListener();
		var endButton = self.charCont.getChildByName("endButton");
		endButton.on("click",self.onEndButtonClick,this,true);
		endButton.on("mouseover",function(){endButton.gotoAndStop("finish_over");});
		endButton.on("mouseout",function(){endButton.gotoAndStop("finish");});
	};
	self.onEndButtonClick = function() {
		//window.location = "INSERT URL HERE.";
	};
	self.setupStartButton = function(){
		self.bgDisplay.startButton.on("mouseover",self.onStartButtonMouseOver);
		self.bgDisplay.startButton.on("mouseout",self.onStartButtonMouseOut);
		self.bgDisplay.startButton.on("click",self.onStartButtonClick,null,true);
	};
	self.onStartButtonMouseOver = function(){self.bgDisplay.startButton.gotoAndStop("button_play_over");};
	self.onStartButtonMouseOut = function(){self.bgDisplay.startButton.gotoAndStop("button_play");};
	self.onStartButtonClick = function(){
		self.soundObj.click.play();
		self.bgDisplay.startButton.gotoAndStop("button_play");
		createjs.Tween.get(self.bgDisplay.startCont,{useTicks:true}).to({alpha:0},15);
		self.characterAnim("startOut");
		self.bgDisplay.startButton.removeAllEventListeners();
	};
	self.onIntroComplete = function(){
		//branch sound
		self.soundObj.branches.play();
		//hide the logo and drop a new one in
		createjs.Tween.get(self.bgDisplay.dropLogo,{useTicks:true}).wait(0).to({y:100,alpha:1}, 40, createjs.Ease.elasticInOut);
		//bring in branches
		createjs.Tween.get(self.bgDisplay.leftBranches,{useTicks:true}).to({rotation:self.endingRotations[0]},30,createjs.Ease.elasticOut);
		createjs.Tween.get(self.bgDisplay.leftShadow,{useTicks:true}).to({rotation:self.endingRotations[0]},30,createjs.Ease.elasticOut);
		createjs.Tween.get(self.bgDisplay.rightBranches,{useTicks:true}).to({rotation:self.endingRotations[1]},30,createjs.Ease.elasticOut);
		createjs.Tween.get(self.bgDisplay.rightShadow,{useTicks:true}).to({rotation:self.endingRotations[1]},30,createjs.Ease.elasticOut);
		createjs.Tween.get(self.bgDisplay.lowerBranches,{useTicks:true}).to({rotation:self.endingRotations[2]},30,createjs.Ease.elasticOut);
		createjs.Tween.get(self.bgDisplay.lowerShadow,{useTicks:true}).to({rotation:self.endingRotations[2]},30,createjs.Ease.elasticOut);
		createjs.Tween.get({},{useTicks:true}).wait(30).call(function(){
			createjs.Tween.get({},{useTicks:true}).wait(7).call(function(){self.soundObj.leafStart.play();});
			for(var i = 0; i < self.allLeaves.length; i+=1){
				createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({scaleX:1,scaleY:1},10);
			}
		});
		self.charCont.x = 500; self.charCont.y = -500;

		TweenMax.to(self.charCont, 100, {
			useFrames:true,
			bezier:{type:"soft",
			values:[{x:100, y:0}, {x:200, y:150}],
			 autoRotate:["x","y","rotation",-45,false]},
			ease:Power1.easeInOut,
			onComplete:self.crossFadeToHoverSound
		});

		TweenMax.delayedCall(150, self.gameStart, [], this, true);
		TweenMax.delayedCall(200, self.loopCharacterAnim, [], this, true);
	};
	// setup the main game leaves
	self.setupLayout = function(){
		var leafX 		 	= [	 0, 25,  70,  0, 	30, 0],
			leafY 		 	= [150, 150, 130, -120, -110, 120],
			containerX 	 	= [230, 210, 447, 615, 	840, 690],
			containerY 	 	= [430, 375, 290, 350, 225, 570],
			prizeX 		 	= [-80, 80, 60, -60, -100, 60],
			prizeY 		 	= [-20, -50,-80, 70, 50, -80],
			leafRotation 	= [-30, 20,   0,   200,  220,  20],
			dottedRotation 	= [-30, 20,   0,   180,  200,  0],
			flip = [-1,1,1,1,1,1];

		for(var i = 0; i <6; i+=1) {
			var leafCont = new createjs.Container();
			leafCont.leaf 	= eIWGcF.createSprites(self.sLeafJSON,leafX[i],leafY[i],"drop","stop","left","bottom",1);
			leafCont.prize 	= eIWGcF.createSprites(self.sSheetJSON,prizeX[i],prizeY[i],"p1","stop","center","center",0);
			leafCont.dotted = eIWGcF.createSprites(self.sSheetJSON,0,0,"leaf_dotted","stop","left","bottom",0);
			leafCont.leaf.rotation   = leafRotation[i];
			leafCont.dotted.rotation = dottedRotation[i];
			leafCont.leaf.scaleX     = flip[i];
			leafCont.dotted.scaleX   = flip[i];

			leafCont.scaleX = 0;
			leafCont.scaleY = 0;

			leafCont.prize.name = "prize";
			leafCont.leaf.name = "leaf";
			leafCont.dotted.name = "dotted";
			// set the container location
			leafCont.x = containerX[i];
			leafCont.y = containerY[i];
			leafCont.isRevealed = false;
			leafCont.isWinRevealed = false;

			leafCont.addChild(leafCont.dotted,leafCont.prize,leafCont.leaf);
			// add the container to stage
			self.gameCont.addChild(leafCont);
			// add to main holder
			self.allLeaves.push(leafCont);
		}
	};
	self.gameStart = function() {
		self.gameStarted = true;
		for (var i = 0; i < self.allLeaves.length; i++) {
			self.allLeaves[i].on("click",self.onLeafButtonClick,this,true,i);
			self.allLeaves[i].getChildByName("leaf").on("mouseover",self.onLeafMouseOver,this,false,i);
			self.allLeaves[i].getChildByName("leaf").on("mouseout",self.onLeafMouseOut,this,false,i);
		}
	};
	self.onLeafButtonClick = function(ev,i){
		self.idleTimer = 0;
		var current = ev.currentTarget;

		createjs.Tween.removeTweens(self.allLeaves[i]);
		self.allLeaves[i].rotation = 0;

		self.revealLeaf(current);
		self.associateTurns(i);

		self.clickCount++;
	};
	self.onLeafMouseOver = function(ev,i){
		self.idleTimer = 0;
		if(!self.allLeaves[i].isRevealed){
			createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation +2},3).call(function(i){
				createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation -4},3).call(function(i){
					createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation +4},3).call(function(i){
						createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation -2},3).call(function(i){
							self.idleTimer = 0;
						},[i],this);
					},[i],this);
				},[i],this);
			},[i],this);
		}
	};
	self.onLeafMouseOut = function(ev,i){
		self.idleTimer = 0;
	};
    self.randomFromInterval = function (from, to) {
        return Math.floor(Math.random() * (to - from + 1) + from);
    };
	self.revealLeaf = function(current) {
		self.soundObj.reveal.play();
		var leaf = current.getChildByName("leaf");
		var prize = current.getChildByName("prize");
		var dotted = current.getChildByName("dotted");

		createjs.Tween.get(prize,{useTicks:true}).to({alpha:1},20);
		createjs.Tween.get(dotted,{useTicks:true}).to({alpha:1},40);

		leaf.gotoAndPlay("dotted");

		leaf.on("animationend",function(evt){
			var fallenLeaf = evt.currentTarget;
			fallenLeaf.stop();
			var randX = self.randomFromInterval(-1000,-500),
				randSpin = self.randomFromInterval(360,540);

			createjs.Tween.get(fallenLeaf,{useTicks:true}).to({x:randX, rotation: randSpin, alpha:0, visible:false}, 40, createjs.Ease.circOut).call(handleComplete);
			function handleComplete() {
			    current.isRevealed = true;
			    self.checkLeaves();
			}
		});

		current.clickNumber = self.clickCount;
		current.symbol 		= self.ticket.turns[self.clickCount].s;
		current.winner		= self.ticket.turns[self.clickCount].w;
		current.prizeNumber = self.convertPrizes(current.symbol);

		// set value under
		prize.gotoAndStop(current.prizeNumber);
		eIWGcF.centraliseRegPoints(prize);
	};
	self.checkLeaves = function(){
		for (var i = 0; i < self.allLeaves.length; i++) {
			var leaf = self.allLeaves[i];
			// loop over revealed leaves only
			if(leaf.isRevealed && leaf.winner == 1 && !leaf.isWinnner) {
				self.winningIndex.push(i);
				leaf.isWinnner = true;
				self.winningLeaves.push(self.allLeaves[i]);
			}
		}
		self.bank();
	};
	self.showWinningLeaves = function(){
		if (self.winningLeaves.length == 3) {
			for (var i = 0; i < self.winningLeaves.length; i++) {
				var winningLeaf = self.winningLeaves[i].getChildByName("prize");

				if (!TweenMax.isTweening(winningLeaf) ){
					TweenMax.to(winningLeaf, 50, {
						useFrames:true,
						repeat:-1,
						yoyo:true,
						scaleX:1.5, scaleY:1.5, rotation:15,
						ease:Elastic.easeInOut}
					);
				}
			}
		}
	};
	self.checkEndGame = function() {
		var delay = 30;

		if (self.clickCount == 6 && !self.endGameTriggered) {
			self.endGameTriggered = true;
			for (var i = 0; i < self.allLeaves.length; i++) {
				var leaf = self.allLeaves[i];
				if(leaf.isWinnner) {
					delay = 80;
				}
			}
			self.endGameStart(delay);
		}
	};
	self.endGameStart = function(delay) {
		self.endGameStarted	 = true;
		g.removeGearEventListener();

		TweenMax.delayedCall(delay, self.characterAnim, ["endGameOff"], this, true);

		if(self.isWinner && self.isTicketWinner){
			self.endGameMessage 	= eIWGcF.createSprites(self.sSheetJSON,0,100,"end_congrats","stop","center","center",1);

			var prize = null;
			if(parseInt(self.ticket.prizeAmount,10) == 0.50){
				prize = "end50p";
			} else {
				prize = "end"+parseInt(self.ticket.prizeAmount,10);
			}

			self.endGamePrizeAmount	= eIWGcF.createSprites(self.sSheetJSON,0,160,prize,"stop","center","center",1);

			self.endGameContainer.addChild(self.endGameMessage,self.endGamePrizeAmount);
		} else {
			self.soundObj.endLose.play();
			self.endGameMessage = eIWGcF.createSprites(self.sSheetJSON,0,120,"end_lose","stop","center","center",1);
			self.endGameContainer.addChild(self.endGameMessage);
		}
	};
	self.convertPrizes = function(prizeSymbol){
		var prizeConverted = 0;
		prizeConverted = "p" + parseFloat(self.ticket.prizeArray[prizeSymbol]);

		switch (prizeSymbol) {
        case 0:
        	// 50p
        	prizeConverted = "p50p";
            break;
        }
		return prizeConverted;
	};
	self.readTicketInfo = function(){
		self.ticket.prizeAmount = eIWGcF.getTicketData(self.ticketRaw,"outcome",0,"amount");
		self.ticket.prizeTier   = eIWGcF.getTicketData(self.ticketRaw,"outcome",0,"prizeTier");
		self.ticket.prizeArray 	= [];
		self.ticket.prizeArray 	= eIWGcF.getTicketData(self.ticketRaw,"outcome",0,"pArray").split(',');
		self.ticket.winToken    = eIWGcF.getTicketData(self.ticketRaw,"params",0,"wT");
		self.ticket.turnLength	= eIWGcF.getTicketDataLength(self.ticketRaw,"turn");
		self.ticket.turns       = [];

		for(var i = 0; i < self.ticket.turnLength; i+=1) {
			self.ticket.turns[i]    = {};
			self.ticket.turns[i].p  = parseInt(eIWGcF.getTicketData(self.ticketRaw,"turn",i,"p"),10);
			self.ticket.turns[i].s = parseInt(eIWGcF.getTicketData(self.ticketRaw,"turn",i,"s"),10);
			self.ticket.turns[i].w  = parseInt(eIWGcF.getTicketData(self.ticketRaw,"turn",i,"w"),10);
		}
	};
	self.tick = function(){
		//workaround for the inability to tween the master volume.
		if(!self.isPaused){
			if(self.isMuted){
				if(createjs.Sound.getVolume() > 0){
					createjs.Sound.setVolume(createjs.Sound.getVolume() - (1/createjs.Ticker.getFPS()));
				} else {
					createjs.Sound.setVolume(0);
				}
			} else {
				if(createjs.Sound.getVolume() < 1){
					createjs.Sound.setVolume(createjs.Sound.getVolume() + (1/createjs.Ticker.getFPS()));
				} else {
					createjs.Sound.setVolume(1);
				}
			}
			if(self.gameStarted){
				if(!self.endGameTriggered || !self.endGameStart){
					self.idleTimer+=1;
					if(self.idleTimer > self.idleTimerTarget){
						self.handleIdle();
						self.idleTimer = 0;
					}
				}
			}

			//self.updateBg();
			gameStage.update();
		}
	};
	self.handleIdle = function(){
		for(var i = 0; i < self.allLeaves.length; i+=1){
			if(!self.allLeaves[i].isRevealed){
				createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation +2},3).call(function(i){
					createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation -4},3).call(function(i){
						createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation +4},3).call(function(i){
							createjs.Tween.get(self.allLeaves[i],{useTicks:true}).to({rotation:self.allLeaves[i].rotation -2},3).call(function(i){
								self.idleTimer = 0;
							},[i],this);
						},[i],this);
					},[i],this);
				},[i],this);
			}
		}
	};
	self.validateTicket = function(){return true;};
	self.associateTurns = function(arr){//associate turn with game. turnGameAssoc array index increments turns from tickets, values are games.
		if((self.turnGameAssoc[self.clickCount]) == -1) {
			self.turnGameAssoc[self.clickCount] = arr;
		}
	};
	self.bank = function(){
		if(self.winningLeaves.length == 3){
			for(var i = 0; i < 3; i +=1){
				self.winningPrizes.push(self.ticket.turns[self.turnGameAssoc.indexOf(self.winningIndex[i])].p);
				self.winningSymbols.push(self.ticket.turns[self.turnGameAssoc.indexOf(self.winningIndex[i])].s);
				self.winningFlags.push(self.ticket.turns[self.turnGameAssoc.indexOf(self.winningIndex[i])].w);
			}
			//if the prize amounts match
			if((self.winningPrizes[0] == self.winningPrizes[1])&&(self.winningPrizes[0] == self.winningPrizes[2])&&(self.winningPrizes[1] == self.winningPrizes[2])){
				//if the symbols match
				if((self.winningSymbols[0] == self.winningSymbols[1])&&(self.winningSymbols[0] == self.winningSymbols[2])&&(self.winningSymbols[1] == self.winningSymbols[2])){
					//and the winflags all match
					if((self.winningFlags[0] == self.winningFlags[1])&&(self.winningFlags[0] == self.winningFlags[2])&&(self.winningFlags[1] == self.winningFlags[2])){
						//and they are all equal to 1
						if(self.winningFlags[0] == 1 && self.winningFlags[1] == 1 && self.winningFlags[2] == 1){
							//and the ticket has a winToken of 1
							if(self.ticket.winToken == 1){
								//if the value hasn't already been banked (this prevents double banking)
								if(self.bankedValues.indexOf(self.winningPrizes[0]) == -1){
									//add the prize amount to the bank
									self.soundObj.win.play();
									self.bankedValues.push(self.winningPrizes[0]);
									self.banked += self.winningPrizes[0];
									self.isWinner = true;
									//then check it against the ticket
									if(self.banked == parseInt(self.ticket.prizeAmount,10)){
										//they must match to get here. so animate them.
										self.showWinningLeaves();
									} else {
										eIWG.errorInvoker.invokeError("banked != ticket: " + self.banked + " != " + parseInt(self.ticket.prizeAmount,10));
									}
								}
							}
						}
					}
				}
			}
			self.winningPrizes = []; self.winningSymbols = []; self.winningFlags = [];
			//check for the end game.
			self.checkEndGame();
		}
	};
	self.pause = function(){
		self.isPaused = true;
		setTimeout(function(){gameStage.update();},1500);
		self.gameCont.mouseEnabled = false;
		//self.updateBg();
	};
	self.unPause = function(){
		self.isPaused = false;
		self.gameCont.mouseEnabled = true;
		//self.updateBg();
	};
	self.updateBg = function(){
		if(self.bgDisplay.bushes_left != undefined){
			self.bgDisplay.bushes_left.x = -gameStage.x/SCALE;
		}
		if(self.bgDisplay.bushes_right != undefined){
			self.bgDisplay.bushes_right.x = (960)+(gameStage.x/SCALE);
		}
		/*if(self.bgDisplay.bushes_bottom != undefined){
			self.bgDisplay.bushes_bottom.y = window.innerHeight/SCALE;
		}*/
	};

	var returnable = {};
	returnable.preload = function(){self.preload();};
	returnable.start = function(){self.start();};
	returnable.muteUnmuteAll = function(){self.setMute();};
	returnable.getMuteStatus = function(){return self.isMuted;};
	returnable.pause = function(){self.pause();};
	returnable.unPause = function(){self.unPause();};
	returnable.fs = function(){self.fsStart();};

	returnable.test = function(){return self;};
	return returnable;
})();

var escapeIWLoader = escapeIWLoader || {};

escapeIWLoader.fullScreen = (function(){
  var self = {};
  self.mainGameClass = null;
  self.screenPromptSetup = function(callback){
  	//button. replace with "Slide up to continue." or something like that.
    var fsButton = new createjs.Shape(),
    fsButtonText = new createjs.Text("Press for Full Screen","15px Arial","#ffffff");
    self.fsCont = new createjs.Container();

    fsButton.graphics.f("#ff00ff").dr(0,0,150,40).ef();
    fsButtonText.x = fsButton.x = 100;
    fsButtonText.y = fsButton.y = 100;

    self.fsCont.addChild(fsButton, fsButtonText);
    self.fsCont.name = "fsCont";
    gameStage.addChild(self.fsCont);

    //this is a single use action, sets up the repeatable one. can't use this muliple times as the callback is the preload function (or at least it was for me).
    self.fsCont.on("click", function(){
      self.fsCont.alpha = 0;
      self.fullScreen();
      callback();
      setTimeout(self.repeatableButton,1000);
    },null,true);

    self.fullScreenChangeDetect();
    createjs.Touch.enable(gameStage); //enable touch as this is before the preloader which sets this.
    gameStage.update(); //ticker may not exist yet so cause an update
  };
  self.repeatableButton = function(){
    self.fsCont.on("click", function(){
      self.fsCont.alpha = 0;
      self.fullScreen();
    },null,false);
  };
  self.fullScreen = function(){
    var elem = document.getElementById(gameStage.canvas.id);
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.msRequestFullscreen) {
      elem.msRequestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }
  };
  self.fullScreenChangeDetect = function(){
  	//add fullscreen listeners.
    var elem = document.getElementById(gameStage.canvas.id);

    elem.addEventListener("fullscreenchange", function (){
      if(!document.fullscreen){
        self.pauseCallback();
        self.fsCont.alpha = 1;
      } else {
        self.fsCont.alpha = 0;
        self.unPauseCallBack();
      }
    }, false);

    elem.addEventListener("mozfullscreenchange", function (){
      if(!document.mozFullScreen){
        self.pauseCallback();
        self.fsCont.alpha = 1;
      } else {
        self.fsCont.alpha = 0;
        self.unPauseCallBack();
      }
    }, false);

    elem.addEventListener("webkitfullscreenchange", function() {
      if(!document.webkitIsFullScreen){
        self.pauseCallback();
        self.fsCont.alpha = 1;
      } else {
        self.fsCont.alpha = 0;
        self.unPauseCallBack();
      }
    }, false);

    elem.addEventListener("msfullscreenchange", function (){
      if(!document.msFullscreenElement){
        self.pauseCallback();
        self.fsCont.alpha = 1;
      } else {
        self.fsCont.alpha = 0;
        self.unPauseCallBack();
      }
    }, false);
  };

  var returnable = {};
  returnable.startFullScreen = function(callback,pauseCallback,unPauseCallBack){
  	self.screenPromptSetup(callback);
  	self.pauseCallback = pauseCallback;
  	self.unPauseCallBack = unPauseCallBack;
  };
  returnable.fullScreenHandler = function(){};

  return returnable;
})()
