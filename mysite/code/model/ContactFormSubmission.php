<?php

class ContactFormSubmission extends DataObject {
    private static $db = array(
        'Name'      => 'Text',
        'Email'     => 'Text',
        'Message'   => 'Text'
    );

    public function getCMSFields(){

        $fields = parent::getCMSFields();
        $fields->fieldByName('Name')->setTitle('Name:');
        $fields->fieldByName('Email')->setTitle('E-mail:');
        $fields->fieldByName('Message')->setTitle('Message:');
        return $fields;

    }

}
