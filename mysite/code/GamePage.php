<?php

class GamePage extends Page {


}

class GamePage_Controller extends Page_Controller {

    private static $allowed_actions = array(
        'ContactForm'
    );

    public function ContactForm() {
        
        $fields = new FieldList( 
            new TextField('Name'),
            new EmailField('Email'),
            new TextareaField('Message')
        );
        
        $fields[0]->setAttribute('placeholder', 'Name');
        $fields[1]->setAttribute('placeholder', 'Email');
        $fields[2]->setAttribute('placeholder', 'Message');
    
        $actions = new FieldList( 
            new FormAction('doContactForm', 'Submit')
        );
        
        $actions[0]->setAttribute('class', 'btn btn-info');
        
        $validation = new RequiredFields('Name', 'Email', 'Message');
        return new Form($this, 'ContactForm', $fields, $actions, $validation);
    }
    
    public function doContactForm($data, $form) {

        // save form data into dataObject
        $submission = new ContactFormSubmission();
        $form->saveInto($submission);
        $submission->write();

        $email = new Email();
        $email->setTo('jack@sideplay.com, carlo@sideplay.com, lee@sideplay.com');
        $email->setFrom('no-reply@sideplay.com');
        $email->setSubject("Contact Message from {$data["Name"]}");
        $email->replyTo($data['Email']);
        $messageBody = "
            <p><strong>Name:</strong> {$data['Name']}</p>
            <p><strong>Email:</strong> {$data['Email']}</p>
            <p><strong>Message:</strong> {$data['Message']}</p>
        ";
        $email->setBody($messageBody);
        $email->send();

		Session::set("LatestSuccess", "Your question has been submitted. We will be in touch shortly");

        $location = Controller::join_links($this->Link(), '?success=1', '#contacts');

        $this->redirect($location);

        return;
    }
    
    public function Success() {

		return isset($_REQUEST['success']) && $_REQUEST['success'] == "1";

	}
    
    public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}
    
}