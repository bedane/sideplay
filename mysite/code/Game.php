<?php
class Game extends DataObject {
    private static $db = array(
        'Name' 		=> 'Varchar(60)',
        'Content' 	=> 'HTMLText',
        'Type'		=> "Enum('Licensed Content, Board & Dice, Big Money, Just For Fun, Themed, HTML5, Sideplay Flash, Sideplay HTML5')",   		// change this to dropdown
        // fact 1
        'fact1Title'    => 'Varchar(60)',
        'fact1Content'  => 'Text',

        // fact 2
        'fact2Title'    => 'Varchar(60)',
        'fact2Content'  => 'Text',

        // fact 3
        'fact3Title'    => 'Varchar(60)',
        'fact3Content'  => 'Text',

        // Path
        'Flash'         => 'Text',
        'FlashBase'     => 'Text',
        'Html5'         => 'Text'
    );
    private static $has_one = array(
    	'Image'		=> 'Image',
        'DemoImage1'=> 'Image',
        'DemoImage2'=> 'Image',
        'DemoImage3'=> 'Image',
        'Portfolio' => 'PortfolioPage'
    );

}
