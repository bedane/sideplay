<?php
class PortfolioPage extends Page {

	private static $db = array(

	);

	private static $has_one = array(
	);

	private static $has_many = array(

		'Games' => 'Game' 

	);
    
    public function getLicensedGames() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "Licensed Content"));

    }
    
    public function getBoardGames() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "Board & Dice"));

    }
    
    public function getBigGames() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "Big Money"));

    }
    
    public function getFunGames() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "Just For Fun"));

    }
    
    public function getThemedGames() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "Themed"));

    }

    public function getHTML5Games() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "HTML5"));

    }

    public function getSideplayFlashGames() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "Sideplay Flash"));

    }

    public function getSideplayHTML5Games() {

        $games = $this->getComponents("Games");
        return $games->filter(array("Type" => "Sideplay HTML5"));

    }

	public function getCMSFields() {
        $fields = parent::getCMSFields();
        
        $config = new GridFieldConfig_RelationEditor();
        $gamesField = new GridField(
            'Games', // Field name
            'Game', // Field title
            $this->Games(), // List of all related students
            $config
         );        
         // Create a tab named "Students" and add our field to it
        $fields->addFieldToTab('Root.Games', $gamesField);
        return $fields;
    }

}
class PortfolioPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
        'ContactForm',
        'ShowGames'
    );
    
    private static $url_handlers = array(
        'game/$ID/$Name' => 'ShowGames'
    );

    public function ContactForm() {
        
        $fields = new FieldList( 
            new TextField('Name'),
            new EmailField('Email'),
            new TextareaField('Message')
        );
        
        $fields[0]->setAttribute('placeholder', 'Name');
        $fields[1]->setAttribute('placeholder', 'Email');
        $fields[2]->setAttribute('placeholder', 'Message');
    
        $actions = new FieldList( 
            new FormAction('doContactForm', 'Submit')
        );
        
        $actions[0]->setAttribute('class', 'btn btn-info');
        
        $validation = new RequiredFields('Name', 'Email', 'Message');
        return new Form($this, 'ContactForm', $fields, $actions, $validation);
    }
    
    public function doContactForm($data, $form) {

        // save form data into dataObject
        $submission = new ContactFormSubmission();
        $form->saveInto($submission);
        $submission->write();

        $email = new Email();
        $email->setTo('jack@sideplay.com, carlo@sideplay.com, lee@sideplay.com');
        $email->setFrom('no-reply@sideplay.com');
        $email->setSubject("Contact Message from {$data["Name"]}");
        $email->replyTo($data['Email']);
        $messageBody = "
            <p><strong>Name:</strong> {$data['Name']}</p>
            <p><strong>Email:</strong> {$data['Email']}</p>
            <p><strong>Message:</strong> {$data['Message']}</p>
        ";
        $email->setBody($messageBody);
        $email->send();

		Session::set("LatestSuccess", "Your question has been submitted. We will be in touch shortly");

        $location = Controller::join_links($this->Link(), '?success=1', '#contacts');

        $this->redirect($location);

        return;
    }
    
    public function Success() {

		return isset($_REQUEST['success']) && $_REQUEST['success'] == "1";

	}
    
    private function getGame($ID) {
        
		return $this->Games("\"ID\" = '$ID'")->first();
	}
    
    public function ShowGames()	{
        
		$ID = (int)$this->getRequest()->param('ID');
		$game = $this->getGame($ID);
		return $this->customise(array("Game" => $game))->renderWith(array('GamePage', 'Page'));
	}

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

}
