(function ($) {

    var i = 0,
        a = [
            [ 100, 243, 764, 825, 639, 917, 253, 182, 651, 537, 226, 931, 062, 160, 100, 243, 764, 825, 639, 917, 253, 182, 651, 537, 226, 931, 160 ],
            [ 01, 17, 82, 16, 35, 84, 52, 31, 08, 55, 23, 17, 51, 99, 54, 72, 35, 01, 17, 82, 16, 35, 84, 52, 31, 08, 55, 23, 17, 51, 99, 54, 72, 50 ],
            [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 12]
        ];

    // get total stats
    $('.stats article h3 span').each(function(){

        var set     = false,
            self    = $(this),
            ar      = a[i];
        i++;


        $(window).bind('scroll', function() {

            var scrollTop       = $(window).scrollTop(),
                elementOffset   = self.offset().top,
                distance        = (elementOffset - scrollTop);

            if ( distance < window.innerHeight ) {

                // check if it has been fired
                if ( set === false ){
                    set = true;

                    // start ticker
                    var number = self[0].innerHTML;

                    var interval = setInterval(function(){

                        if ( ar.length <= 1 ) {
                            clearInterval(interval);
                        }

                        var newNumber = ar[0];
                        ar.shift();
                        self[0].innerHTML = newNumber;

                    }, 50);
                }
            }
        });
    });
}(jQuery))
