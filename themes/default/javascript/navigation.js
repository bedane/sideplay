(function($) {
    // nav fix
    $(window).bind('scroll', function() {

        (function(){

            var scrollTop       = $(window).scrollTop(),
                elementOffset   = $('#navigation').offset().top,
                distance        = (elementOffset - scrollTop);
             if (distance < 1) {
                 $('#navigationFixed').show().addClass('active');
             }
             else {
                 $('#navigationFixed').hide().removeClass('active');
             }
        })();

        (function(){
            $('.services img').each(function(){

                var scrollTop       = $(window).scrollTop(),
                    elementOffset   = $(this).offset().top,
                    distance        = (elementOffset - scrollTop);

                if (distance < window.innerHeight - 150) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        })();

        (function(){
            $('.caption').each(function(){

                var scrollTop       = $(window).scrollTop(),
                    elementOffset   = $(this).offset().top,
                    distance        = (elementOffset - scrollTop);

                if (distance < window.innerHeight - 150) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        })();

    });

    var waypoint = new Waypoint({
        element: document.getElementById('about'),
        offset: 70,
        handler: function(direction) {
            changeNav('about');
        }
    });
    var waypoint = new Waypoint({
        element: document.getElementById('services'),
        offset: 70,
        handler: function(direction) {
            changeNav('services');
        }
    });
    var waypoint = new Waypoint({
        element: document.getElementById('work'),
        offset: 70,
        handler: function(direction) {
            changeNav('work');
        }
    });
    var waypoint = new Waypoint({
        element: document.getElementById('contacts'),
        offset: 70,
        handler: function(direction) {
            changeNav('contacts');
        }
    });

    function changeNav(id) {

        console.log('id');

        window.history.pushState("object or string", id, "/" + id );
        $('nav li').each(function(){
            $(this).removeClass('active');
        });
        $('nav li #'+id+'link').parent().addClass('active');
    }


    // caption
    var captionArray = [],
        parallaxArray = [];
    $('.caption').each(function(){
        captionArray.push(this);
    });
    $('.parallax-mirror').each(function(){
        parallaxArray.push(this);
    });

    // reverse parallax-window
    parallaxArray.reverse();

    for ( var i = 0; i < parallaxArray.length; i++){

        var caption = captionArray[i],
            index   = $('.caption').data('index');

        if ( index === i ) {
            // place caption in that parallaxArray index
            $('.caption').appendTo( $('.parallax-mirror:nth-of-type(' + (4 - i) + ')') );
        }


    }


    function goToByScroll(id) {

        var id = id.replace('link', ''),
            current = document.location.host;

        $('html, body').animate({
            scrollTop: $('#'+id).offset().top - 50
        }, 'slow');

        window.history.pushState("object or string", id, "/" + id );

    }

    // if you click on any of the navigation items
    $('nav li a').click(function(e) {

        e.preventDefault();
        
        $('nav li').each(function(){
            $(this).removeClass('active');
        });
        $(this).parent().addClass('active');
        goToByScroll($(this).attr('id'));

    });

    $('.scrollTo').click(function(e) {

        e.preventDefault();
        goToByScroll($(this).data('section'));

    });

}(jQuery));
